package vue;


import java.io.IOException;
import java.net.InetAddress;

import application.Main;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.TextAlignment;
import modele.Account;
import modele.Client;
import modele.GameSession;
import modele.Host;
import modele.Lobby;
import modele.NetworkMessage;
import modele.Resizable;

public class OnlineLobby extends BorderPane implements Resizable
{
	private Lobby lobby;
	private Thread handlingMessageThread;
	private Account account;
	
	private Button backToMainMenu, playGame, player2Ready, player3Ready, player4Ready;
	private VBox difficulties, boxPlayer1, boxPlayer2, boxPlayer3, boxPlayer4;
	private Label labelPlayer1, labelPlayer2, labelPlayer3, labelPlayer4, difficultyInfo;
	private RadioButton hard,medium,easy;
	private ImageView p1Image,p2Image,p3Image,p4Image;
	private Image backgroundImage;
	private CheckBox overrideLevenshteinBox;
	private TextField overrideLevenshteinField;
	public boolean overrideLevenshteinSetting;
	private MediaPlayer mp;
	private String musicPath = "Soundtrack/lobbytheme.mp3";

	public OnlineLobby(Lobby lob, String ip) throws IOException 
	{
		lobby = lob;

		getStylesheets().addAll(getClass().getResource("transback.css").toExternalForm());
		account = lobby.getPlayer(0);
		backgroundImage = new Image(getClass().getResourceAsStream("Background/Images/FondTexture.png"));
		HBox boxNbPlayer = new HBox();
		boxPlayer1 = new VBox();
		boxPlayer2 = new VBox();
		boxPlayer3 = new VBox();
		boxPlayer4 = new VBox();
		HBox boxLaunchGame = new HBox();
		VBox box = new VBox();
		HBox centerBox = new HBox();
		labelPlayer2 = new Label("Player 2");
		labelPlayer3 = new Label("Player 3");
		labelPlayer4 = new Label("Player 4");
		box.setAlignment(Pos.TOP_LEFT);


		boxNbPlayer.setSpacing(Main.getStageWidth()/20);
		boxNbPlayer.setAlignment(Pos.TOP_RIGHT);

		//Add to the boxPlayers each informations of the player 2 (nickname) and a ready button
		boxPlayer2.setDisable(true);
		boxPlayer2.getChildren().add(labelPlayer2);
		boxPlayer2.getChildren().add(getPlayer2Ready());
		boxPlayer2.setAlignment(Pos.TOP_CENTER);
		boxPlayer2.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
		boxPlayer2.setPrefHeight(Main.getStageHeight()/1.5);
		boxPlayer2.setPrefWidth(Main.getStageWidth()/4.1);

		//Add to the boxPlayers each informations of the player 3 (nickname) and a ready button
		boxPlayer3.setDisable(true);
		boxPlayer3.getChildren().add(labelPlayer3);
		boxPlayer3.getChildren().add(getPlayer3Ready());
		boxPlayer3.setAlignment(Pos.TOP_CENTER);
		boxPlayer3.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
		boxPlayer3.setPrefHeight(Main.getStageHeight()/1.5);
		boxPlayer3.setPrefWidth(Main.getStageWidth()/4.1);

		//Add to the boxPlayers each informations of the player 4 (nickname) and a ready button
		boxPlayer4.setDisable(true);
		boxPlayer4.getChildren().add(labelPlayer4);
		boxPlayer4.getChildren().add(getPlayer4Ready());
		boxPlayer4.setAlignment(Pos.TOP_CENTER);
		boxPlayer4.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
		boxPlayer4.setPrefHeight(Main.getStageHeight()/1.5);
		boxPlayer4.setPrefWidth(Main.getStageWidth()/4.1);
		
		//Add to the boxPlayers each informations of the player (nickname) and a ready button
		labelPlayer1 = getPlayerLabel(account);
		boxPlayer1.getChildren().add(labelPlayer1);
		boxPlayer1.setAlignment(Pos.TOP_CENTER);
		boxPlayer1.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
		boxPlayer1.setStyle("-fx-background-color: #f7f1dc");
		boxPlayer1.setPrefHeight(Main.getStageHeight()/1.5);
		boxPlayer1.setPrefWidth(Main.getStageWidth()/4.1);
		
		
		if(ip == null)
		{
			p1Image = account.getSession().getImageView();
			boxPlayer1.getChildren().add(p1Image);
			account.getSession().setNetworkSession(new Host());
			// Give the ip address of the host
			ip = InetAddress.getLocalHost().getHostAddress();
			labelPlayer1.setText(labelPlayer1.getText() + "\n" + ip);
			account.getSession().initConnection(null);
		}
		else
		{
			account.getSession().setNetworkSession(new Client());
			getOverrideLevenshteinBox().setDisable(true);
			try
			{
				account.getSession().initConnection(ip);
				
				//We use -1 as index since we send it to the server, the server will broadcast the correct order later
				account.getSession().sendMessage(NetworkMessage.encodePlayerConnectMessage(account.getNickname(), account.getColor(),-1));
			}
			catch (IOException e) 
			{
				backToMainMenu(true,"The connection with the specified IP has not been possible");
			}
		}
		
		startHandlingMessages();
		
		//Add to the boxLaunchGame buttons to launch the game, to add a player and to return to the main menu
		boxLaunchGame.getChildren().add(getPlayGame());
		boxLaunchGame.getChildren().add(getBackToMainMenu());
		boxLaunchGame.setAlignment(Pos.BOTTOM_CENTER);
		boxLaunchGame.setSpacing(Main.getStageWidth()/20);

		//add each boxes + difficulties
		centerBox.getChildren().add(boxPlayer1);
		centerBox.getChildren().add(boxPlayer2);
		centerBox.getChildren().add(boxPlayer3);
		centerBox.getChildren().add(boxPlayer4);
		box.getChildren().add(boxNbPlayer);
		box.getChildren().add(centerBox);
		box.getChildren().add(getDifficulties());
		box.getChildren().add(boxLaunchGame);
		VBox.setMargin(boxPlayer1,new Insets(15,Main.getStageWidth()/3,15,Main.getStageWidth()/3));
		setTop(box);
		box.setSpacing(10);
		
		String path = getClass().getResource(musicPath).toExternalForm();
		Media media = new Media(path);
		mp = new MediaPlayer(media);
		mp.setCycleCount(MediaPlayer.INDEFINITE);
		mp.play();
		
		resize();
	}
	

	public CheckBox getOverrideLevenshteinBox()
	{
		if(overrideLevenshteinBox == null)
		{
			overrideLevenshteinBox = new CheckBox("Override Levenshtein distance setting in percent");
			overrideLevenshteinBox.getStyleClass().add("transback");
			overrideLevenshteinBox.setTextFill(Color.WHITE);
			
			overrideLevenshteinBox.setOnAction(e ->
			{
				if(overrideLevenshteinBox.isSelected())
				{
					getOverrideLevenshteinField().setDisable(false);
				}
				else
				{
					getOverrideLevenshteinField().setDisable(true);
				}
				
				overrideLevenshteinSetting = overrideLevenshteinBox.isSelected();
				
				account.getSession().sendMessage(NetworkMessage.encodeOverrideLevenshteinMessage(overrideLevenshteinBox.isSelected()));
			});
		}
		
		return overrideLevenshteinBox;
	}
	
	public TextField getOverrideLevenshteinField()
	{
		if(overrideLevenshteinField == null)
		{
			overrideLevenshteinField = new TextField();
			overrideLevenshteinField.setDisable(true);
			
			overrideLevenshteinField.setOnAction(e ->
			{
				account.getSession().sendMessage(NetworkMessage.encodeOverrideLevenshteinSettingMessage(overrideLevenshteinField.getText()));
			});
		}
		
		return overrideLevenshteinField;
	}

	public void handlePlayerLeft(String playerName)
	{
		Account tmpAccount = new Account(playerName, "", "", null);
		int index = lobby.indexOf(tmpAccount);
		
		Platform.runLater(()->
		{
			if(index == 1)
			{
				if(!labelPlayer3.getText().equals("Player 3"))
				{
					labelPlayer2.setText(labelPlayer3.getText());
					boxPlayer2.getChildren().remove(lobby.getPlayer(1).getSession().getImageView());
					boxPlayer2.getChildren().add(lobby.getPlayer(2).getSession().getImageView());
					
					labelPlayer3.setText("Player 3");
					boxPlayer3.setStyle(null);
					player3Ready.setVisible(false);
					boxPlayer3.setDisable(true);
					boxPlayer3.getChildren().remove(lobby.getPlayer(2).getSession().getImageView());
					
					if(!labelPlayer4.getText().equals("Player 4"))
					{
						labelPlayer3.setText(labelPlayer4.getText());
						boxPlayer3.getChildren().remove(lobby.getPlayer(2).getSession().getImageView());
						boxPlayer3.getChildren().add(lobby.getPlayer(3).getSession().getImageView());
						
						labelPlayer4.setText("Player 4");
						boxPlayer4.setStyle(null);
						player4Ready.setVisible(false);
						boxPlayer4.setDisable(true);
						boxPlayer4.getChildren().remove(lobby.getPlayer(3).getSession().getImageView());
					}
				}
				else
				{					
					labelPlayer2.setText("Player 2");
					player2Ready.setVisible(false);
					boxPlayer2.setDisable(true);
					boxPlayer2.setStyle(null);
					boxPlayer2.getChildren().remove(lobby.getPlayer(1).getSession().getImageView());
				}
			}
			else if(index == 2)
			{
				if(!labelPlayer4.getText().equals("Player 4"))
				{
					labelPlayer3.setText(labelPlayer4.getText());
					boxPlayer3.getChildren().remove(lobby.getPlayer(2).getSession().getImageView());
					boxPlayer3.getChildren().add(lobby.getPlayer(3).getSession().getImageView());
					
					labelPlayer4.setText("Player 4");
					boxPlayer4.setStyle(null);
					player4Ready.setVisible(false);
					boxPlayer4.setDisable(true);
					boxPlayer4.getChildren().remove(lobby.getPlayer(3).getSession().getImageView());
				}
				else
				{
					labelPlayer3.setText("Player 3");
					boxPlayer3.setStyle(null);
					player3Ready.setVisible(false);
					boxPlayer3.setDisable(true);
					boxPlayer3.getChildren().remove(lobby.getPlayer(2).getSession().getImageView());
				}
			}
			else 
			{
				labelPlayer4.setText("Player 4");
				boxPlayer4.setStyle(null);
				player4Ready.setVisible(false);
				boxPlayer4.setDisable(true);
				boxPlayer4.getChildren().remove(lobby.getPlayer(3).getSession().getImageView());
			}
			lobby.removePlayer(index);
		});
	}
	
	private void startHandlingMessages() {
		handlingMessageThread = new Thread(() -> 
       {
            while (!handlingMessageThread.isInterrupted()) 
            {
	             try 
	             {
	            	int size = account.getSession().getMessages().size();
		             if (size > 0) 
		             {
		            	String message = account.getSession().getMessages().removeFirst();
						String type = message.split(NetworkMessage.SEPARATOR)[0];
												
						switch (type) {
						
						case "OverrideLevenshteinSetting":
							String[] LevenshteinSettingData = NetworkMessage.decodePlayerReadyMessage(message);
							int val = Integer.parseInt(LevenshteinSettingData[1]);
						
							getOverrideLevenshteinField().setText(""+val);
						break;
						
						case "OverrideLevenshtein":
							String[] LevenshteinData = NetworkMessage.decodePlayerReadyMessage(message);
							boolean bool = Boolean.parseBoolean(LevenshteinData[1]);
							
							overrideLevenshteinSetting = bool;
						break;
							
						case "LaunchGame":
							launchGame();
							break;
						
						case "PlayerReady":
							String[] playerReadyData = NetworkMessage.decodePlayerReadyMessage(message);
							Account tmpAccount = new Account(playerReadyData[1], "", "", null);
							int index;
							
							index = lobby.indexOf(tmpAccount);
							
							if(Boolean.valueOf(playerReadyData[2]) == true)
							{
								if(index == 1)
								{
									Platform.runLater(()->
									{
										getPlayer2Ready().setText("Ready");
										getPlayer2Ready().setStyle("-fx-background-color: 	#34C924;");
									});
								}
								else if(index == 2)
								{
									Platform.runLater(()->
									{
										getPlayer3Ready().setText("Ready");
										getPlayer3Ready().setStyle("-fx-background-color: 	#34C924;");
									});
								}
								else 
								{
									Platform.runLater(()->
									{
										getPlayer4Ready().setText("Ready");
										getPlayer4Ready().setStyle("-fx-background-color: 	#34C924;");
									});
								}
							}
							else 
							{
								if(index == 1)
								{
									Platform.runLater(()->
									{
										getPlayer2Ready().setText("Not Ready");
										getPlayer2Ready().setStyle("-fx-background-color: 	#DE2916;");
									});
								}
								else if(index == 2)
								{
									Platform.runLater(()->
									{
										getPlayer3Ready().setText("Not Ready");
										getPlayer3Ready().setStyle("-fx-background-color: 	#DE2916;");
									});
								}
								else 
								{
									Platform.runLater(()->
									{
										getPlayer4Ready().setText("Not Ready");
										getPlayer4Ready().setStyle("-fx-background-color: 	#DE2916;");
									});
								}
							}
							break;
							
						case "NicknameAlreadyPresent":
                    		String[] doubleNicknameData = NetworkMessage.decodeNicknameAlreadyPresentMessage(message);
							
                    		if(doubleNicknameData[1].equals(account.getNickname()) && lobby.length() == 1)
                    		{
                    			Platform.runLater(()->backToMainMenu(true,"A player with this nickname is already in this lobby"));
                    		}
							break;
							
						case "PlayerConnect":
                    		String[] playerConnectData = NetworkMessage.decodePlayerConnectMessage(message);
                    		Color color = Color.web(playerConnectData[2]);
                    		GameSession playerSession = new GameSession(color, playerConnectData[1]);
                    		
                    		for(Account acc : lobby.getPlayers())
                    		{
                    			if(acc != null && acc.getNickname().equalsIgnoreCase(playerConnectData[1]))
								{
                    				//Player with the same name already present										
                    				account.getSession().sendMessage(NetworkMessage.encodeNicknameAlreadyPresentMessage(acc.getNickname()));
								}
                    		}
							
							addPlayer(playerSession,Integer.parseInt(playerConnectData[3]));
							
							//The host send all the informations everytime, to make sure new players are aware of the others
							if(account.getSession().isHost())
							{							
								for (int i = 0; i < lobby.length(); i++) 
								{
									Account acc = lobby.getPlayer(i);
									
									if(acc != null)									
										account.getSession().sendMessage(NetworkMessage.encodePlayerConnectMessage(acc.getNickname(), acc.getColor(),i));
								}
							}
							else
							{
								//If the host have been placed correctly into the lobby
								if(lobby.getPlayer(0) != null && lobby.getPlayer(0) != account)
								{
									Platform.runLater(()->labelPlayer1.setText("Host\n"+lobby.getPlayer(0).getNickname()));
								}
							}
							break;
							
						case "PlayerDisconnect":
							String[] playerDisconnectData = NetworkMessage.decodePlayerDisconnectMessage(message);
														
							//The host left, disconnecting
							if(Boolean.valueOf(playerDisconnectData[2]) == true)
							{
								Platform.runLater(()->backToMainMenu(true,"Connection with the host lost"));
							}
							//A player left, removing him
							else {
								handlePlayerLeft(playerDisconnectData[1]);
							}
							
							break;
							
						case "ModifyDifficulty":
							String diff = NetworkMessage.decodeModifyDifficulty(message);
							HBox toggleBox = (HBox) getDifficulties().getChildren().get(1);
							if(diff.equals("easy"))
							{
								((RadioButton)toggleBox.getChildren().get(0)).setSelected(true);
								DisplayQuestion.setHintEnabled(true);
								DisplayQuestion.setTimeToAnswer(60);
								DisplayQuestion.setRewards(1, 3);
								DisplayQuestion.setAcceptableErrorPct(0.8);
							}
							else if(diff.equals("medium"))
							{
								((RadioButton)toggleBox.getChildren().get(1)).setSelected(true);
								DisplayQuestion.setHintEnabled(true);
								DisplayQuestion.setTimeToAnswer(45);
								DisplayQuestion.setRewards(2, 5);
								DisplayQuestion.setAcceptableErrorPct(0.9);
							}
							else 
							{
								((RadioButton)toggleBox.getChildren().get(2)).setSelected(true);
								DisplayQuestion.setHintEnabled(false);
								DisplayQuestion.setTimeToAnswer(30);
								DisplayQuestion.setRewards(3, 7);
								DisplayQuestion.setAcceptableErrorPct(1);
							}
						break;
						}
					}
					Thread.sleep(100);
				} catch (InterruptedException e) 
	             {
                    Thread.currentThread().interrupt();

                    break;
				}
            }
        });
		
		//Make sure the thread is stopped when the program exit
		handlingMessageThread.setDaemon(true);
		handlingMessageThread.start();
	} 

	public void addPlayer(GameSession playerSession, int index)
	{
		Account tmpAccount = playerSession.getNickname().equals(account.getNickname()) ? account : new Account(playerSession.getNickname(), "", "", playerSession.getColor());
		
		if(lobby.contains(tmpAccount))
		{		
			return;
		}
		
		//pos sent by the host
		if(index != -1)
			lobby.addPlayer(index, tmpAccount);
		//Host side, so he only add it
		else
		{
			index = lobby.length();
			lobby.addPlayer(index,tmpAccount);
		}		

		double imageSize = Main.getStageHeight()/20+Main.getStageWidth()/40;
		
		if(index == 0)
		{
			Platform.runLater(()->
			{
				p1Image = tmpAccount.getSession().getImageView();
				
				p1Image.setFitHeight(imageSize);
				p1Image.setFitWidth(imageSize);
				
				labelPlayer1.setText(playerSession.getNickname());
				boxPlayer1.setStyle("-fx-background-color: #f7f1dc");
				boxPlayer1.getChildren().add(p1Image);
				boxPlayer2.setDisable(false);
			});
		}
		else if(index == 1)
		{
			Platform.runLater(()->
			{
				p2Image = tmpAccount.getSession().getImageView();
				
				p2Image.setFitHeight(imageSize);
				p2Image.setFitWidth(imageSize);
				
				labelPlayer2.setText(playerSession.getNickname());
				boxPlayer2.setStyle("-fx-background-color: #f7f1dc");
				boxPlayer2.getChildren().add(p2Image);
				player2Ready.setVisible(true);
				boxPlayer2.setDisable(false);
			});
		}
		else if(index == 2)
		{
			Platform.runLater(()->
			{
				p3Image = tmpAccount.getSession().getImageView();
				
				p3Image.setFitHeight(imageSize);
				p3Image.setFitWidth(imageSize);
				
				labelPlayer3.setText(playerSession.getNickname());
				boxPlayer3.setStyle("-fx-background-color: #f7f1dc");
				boxPlayer3.getChildren().add(p3Image);
				player3Ready.setVisible(true);
				boxPlayer3.setDisable(false);
			});
		}
		else 
		{
			Platform.runLater(()->
			{
				p4Image = tmpAccount.getSession().getImageView();
				
				p4Image.setFitHeight(imageSize);
				p4Image.setFitWidth(imageSize);
				
				labelPlayer4.setText(playerSession.getNickname());
				boxPlayer4.setStyle("-fx-background-color: #f7f1dc");
				boxPlayer4.getChildren().add(p4Image);
				player4Ready.setVisible(true);
				boxPlayer4.setDisable(false);
			});
		}
	}

	public VBox getDifficulties()
	{
		if(difficulties == null)
		{
			ToggleGroup difficult = new ToggleGroup();

			//Add "easy", "medium", and "hard" mode and choose between them
			easy = new RadioButton("Easy");
			easy.getStyleClass().add("outline");
			easy.setToggleGroup(difficult);
			easy.setSelected(true);
			easy.setTextFill(Color.WHITE);
			Tooltip easyTooltip = new Tooltip();
			easyTooltip.setText("60 seconds to answer the questions\nHint enabled\nYour answer need to be at least 80% correct to be validated");
			easy.setTooltip(easyTooltip);

			medium = new RadioButton("Medium");
			medium.getStyleClass().add("outline");
			medium.setToggleGroup(difficult);
			medium.setTextFill(Color.WHITE);
			Tooltip mediumTooltip = new Tooltip();
			mediumTooltip.setText("45 seconds to answer the questions\nHint enabled\nYour answer need to be at least 90% correct to be validated");
			medium.setTooltip(mediumTooltip);

			hard = new RadioButton("Hard");
			hard.getStyleClass().add("outline");
			hard.setToggleGroup(difficult);
			hard.setTextFill(Color.WHITE);
			Tooltip hardTooltip = new Tooltip();
			hardTooltip.setText("30 seconds to answer the questions\nHint disabled\nYour answer need to be entirely correct to be validated");
			hard.setTooltip(hardTooltip);
			
			if(!account.getSession().isHost())
			{
				easy.setDisable(true);
				medium.setDisable(true);
				hard.setDisable(true);
			}

			difficult.selectedToggleProperty().addListener(    
					(ObservableValue<? extends Toggle> ov, Toggle old_toggle, 
							Toggle new_toggle) -> 
					{
						//Choose a difficulty
						if (difficult.getSelectedToggle() != null) 
						{
							if(((RadioButton)difficult.getSelectedToggle()).equals(easy))
							{
								account.getSession().sendMessage(NetworkMessage.encodeModifyDifficulty("easy"));
								DisplayQuestion.setHintEnabled(true);
								DisplayQuestion.setTimeToAnswer(60);
								DisplayQuestion.setRewards(1, 3);
								DisplayQuestion.setAcceptableErrorPct(0.8);
							}
							else if(((RadioButton)difficult.getSelectedToggle()).equals(medium))
							{
								account.getSession().sendMessage(NetworkMessage.encodeModifyDifficulty("medium"));
								DisplayQuestion.setHintEnabled(true);
								DisplayQuestion.setTimeToAnswer(45);
								DisplayQuestion.setRewards(2, 5);
								DisplayQuestion.setAcceptableErrorPct(0.9);
							}
							else 
							{
								account.getSession().sendMessage(NetworkMessage.encodeModifyDifficulty("hard"));
								DisplayQuestion.setHintEnabled(false);
								DisplayQuestion.setTimeToAnswer(30);
								DisplayQuestion.setRewards(3, 7);
								DisplayQuestion.setAcceptableErrorPct(1);
							}
						}
					});

			difficulties = new VBox();
			HBox box = new HBox();
			box.getChildren().add(getOverrideLevenshteinBox());
			box.getChildren().add(getOverrideLevenshteinField());
			box.setAlignment(Pos.CENTER);
			box.setSpacing(25);
			HBox boxx = new HBox();
			boxx.getChildren().add(easy);
			boxx.getChildren().add(medium);
			boxx.getChildren().add(hard);
			boxx.setAlignment(Pos.CENTER);
			boxx.setSpacing(25);

			difficultyInfo = new Label("Hover the wanted difficulty to show the tooltip and get more information.");
			difficultyInfo.setFont(new Font(Main.getStageWidth()/50));
			difficultyInfo.getStyleClass().add("transback");
			difficultyInfo.setTextFill(Color.WHITE);

			difficulties.getChildren().add(difficultyInfo);
			difficulties.getChildren().add(box);
			difficulties.getChildren().add(boxx);
			difficulties.setAlignment(Pos.CENTER);
		}

		return difficulties;
	}

	public Label getPlayerLabel(Account acc) 
	{
		Label playerNickname = new Label();
		playerNickname.setWrapText(true);
		playerNickname.setTextAlignment(TextAlignment.CENTER);

		playerNickname.setText(acc.getNickname());
		//Return the player's nickname
		return playerNickname; 
	}

	public Button getPlayer2Ready() {
		if(player2Ready == null)
		{
			player2Ready = new Button("Not Ready");
			player2Ready.setStyle("-fx-background-color: 	#DE2916;");
			player2Ready.setVisible(false);
			player2Ready.setPrefWidth(200);
			player2Ready.setPrefHeight(50);
		}
		
		return player2Ready; 
	}

	public Button getPlayer3Ready() {
		if(player3Ready == null)
		{
			player3Ready = new Button("Not Ready");
			player3Ready.setStyle("-fx-background-color: 	#DE2916;");
			player3Ready.setVisible(false);
			player3Ready.setPrefWidth(200);
			player3Ready.setPrefHeight(50);
		}

		return player3Ready; 
	}

	public Button getPlayer4Ready() {
		if(player4Ready == null)
		{
			player4Ready = new Button("Not Ready");
			player4Ready.setStyle("-fx-background-color: 	#DE2916;");
			player4Ready.setVisible(false);
			player4Ready.setPrefWidth(200);
			player4Ready.setPrefHeight(50);
		}

		return player4Ready; 
	}

	public Button getPlayGame() {
		if(playGame == null)
		{
			playGame = new Button("Launch Game");
			playGame.setStyle("-fx-background-color: 	#FFD700;");
			playGame.setPrefWidth(400);
			playGame.setPrefHeight(50);
			
			if(!account.getSession().isHost())
			{
				playGame.setText("Ready");
			}

			playGame.setOnAction(new EventHandler<ActionEvent>() 
			{
				@Override
				public void handle(ActionEvent event) 
				{
					if(account.getSession().isHost())
					{

						boolean isEveryoneReady = false;

						if(lobby.length() == 2)
						{
							isEveryoneReady = getPlayer2Ready().getText().equals("Ready");
						}
						else if(lobby.length() == 3)
						{
							isEveryoneReady = getPlayer2Ready().getText().equals("Ready")
									&& getPlayer3Ready().getText().equals("Ready");
						}
						else 
						{
							isEveryoneReady = getPlayer2Ready().getText().equals("Ready")
									&& getPlayer3Ready().getText().equals("Ready") && getPlayer4Ready().getText().equals("Ready");
						}
							
						
						if(isEveryoneReady)
						{
							account.getSession().sendMessage(NetworkMessage.encodeLaunchGameMessage());
							launchGame();
						}
						else
						{
							if(lobby.length() == 1)
							{
								Alert alert = new Alert(AlertType.ERROR);
								alert.setTitle("Solo");
								alert.setHeaderText("You cannot launch an online game alone, please play in local for that.");
								alert.show();							
							}
							else
							{
								Alert alert = new Alert(AlertType.ERROR);
								alert.setTitle("Player ready");
								alert.setHeaderText("All players must be ready before launching the game.");
								alert.show();
							}
						}
					}
					else
					{
						if(lobby.indexOf(account) == 1)
						{
							if(player2Ready.getText().equals("Not Ready"))
							{
								//If the button is clicked, set the text to "Ready"
								player2Ready.setText("Ready");
								playGame.setText("Cancel");
								player2Ready.setStyle("-fx-background-color: 	#34C924;");
								account.getSession().sendMessage(NetworkMessage.encodePlayerReadyMessage(account.getNickname(),true));
							}
							else
							{
								//If the button is clicked, set the text to "Not Ready"
								player2Ready.setText("Not Ready");
								playGame.setText("Ready");
								player2Ready.setStyle("-fx-background-color: 	#DE2916;");
								account.getSession().sendMessage(NetworkMessage.encodePlayerReadyMessage(account.getNickname(),false));
							}
						}
						else if(lobby.indexOf(account) == 2)
						{
							if(player3Ready.getText().equals("Not Ready"))
							{
								//If the button is clicked, set the text to "Ready"
								player3Ready.setText("Ready");
								playGame.setText("Cancel");
								player3Ready.setStyle("-fx-background-color: 	#34C924;");
								account.getSession().sendMessage(NetworkMessage.encodePlayerReadyMessage(account.getNickname(),true));
							}
							else
							{
								//If the button is clicked, set the text to "Not Ready"
								player3Ready.setText("Not Ready");
								playGame.setText("Ready");
								player3Ready.setStyle("-fx-background-color: 	#DE2916;");
								account.getSession().sendMessage(NetworkMessage.encodePlayerReadyMessage(account.getNickname(),false));
							}
						}
						else if(lobby.indexOf(account) == 3)
						{
							if(player4Ready.getText().equals("Not Ready"))
							{
								//If the button is clicked, set the text to "Ready"
								player4Ready.setText("Ready");
								playGame.setText("Cancel");
								player4Ready.setStyle("-fx-background-color: 	#34C924;");
								account.getSession().sendMessage(NetworkMessage.encodePlayerReadyMessage(account.getNickname(),true));
							}
							else
							{
								//If the button is clicked, set the text to "Not Ready"
								player4Ready.setText("Not Ready");
								playGame.setText("Ready");
								player4Ready.setStyle("-fx-background-color: 	#DE2916;");
								account.getSession().sendMessage(NetworkMessage.encodePlayerReadyMessage(account.getNickname(),false));
							}
						}
					}
				}				
			});
		}
		return playGame; 
	}
	
	public void launchGame()
	{
		Platform.runLater(() ->
		{
			lobby.resetSessions();
			handlingMessageThread.interrupt();
			
			if(overrideLevenshteinSetting)
			{
				DisplayQuestion.setAcceptableErrorPct(Double.parseDouble(getOverrideLevenshteinField().getText()));
			}

			OnlineGameBoard board = new OnlineGameBoard(lobby,account);
			Scene scene = new Scene(board,Main.getStageWidth(),Main.getStageHeight());
			scene.setOnKeyPressed(board);
			mp.pause();
			Main.getStage().setScene(scene);
		}); 
	}

	public Button getBackToMainMenu() {
		if(backToMainMenu == null)
		{
			backToMainMenu = new Button("Back");
			backToMainMenu.setStyle("-fx-background-color: 	#DDAAFF;");
			backToMainMenu.setPrefWidth(400);
			backToMainMenu.setPrefHeight(50);

			backToMainMenu.setOnAction(new EventHandler<ActionEvent>() 
			{
				@Override
				public void handle(ActionEvent event) 
				{
					backToMainMenu(false,"");
				}				
			});
		}
		return backToMainMenu; 
	}
	
	public void backToMainMenu(boolean forced, String message)
	{
		//Reset the lobby, leaving only the current player inside
		lobby.clear(account);
		if(!forced)
		{
			account.getSession().sendMessage(NetworkMessage.encodePlayerDisconnectMessage(account.getNickname(), account.getSession().isHost()));
		}
		account.getSession().closeConnection();
		if(handlingMessageThread != null)
			handlingMessageThread.interrupt();
		//Go to the main menu. Use platform.runlater not because of threads, but because the transition is too quick
		//And may cause issue when the player is still in the lobby
		Platform.runLater(() ->
		{
			mp.pause();
			Main.getStage().setScene(Main.getMainMenu());
		});
		if(forced)
		{
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText(message);
			alert.show();
		}
	}
	
	@Override
	public void resize() 
	{
		final double normalButtonsWidth = Main.getStageWidth()/7;
		final double normalButtonsHeight = Main.getStageHeight()/13;
		
		getBackToMainMenu().setPrefHeight(normalButtonsHeight);
		getBackToMainMenu().setPrefWidth(normalButtonsWidth);
		
		getPlayGame().setPrefHeight(normalButtonsHeight);
		getPlayGame().setPrefWidth(normalButtonsWidth);	

		double fontSize = Main.getStageWidth()/125+Main.getStageHeight()/75;
		
		labelPlayer1.setFont(new Font(fontSize));
		labelPlayer2.setFont(new Font(fontSize));
		labelPlayer3.setFont(new Font(fontSize));
		labelPlayer4.setFont(new Font(fontSize));	
		difficultyInfo.setFont(new Font(fontSize));
		easy.setFont(new Font(fontSize));
		medium.setFont(new Font(fontSize));
		hard.setFont(new Font(fontSize));
		getOverrideLevenshteinBox().setFont(new Font(fontSize));

		VBox.setMargin(boxPlayer1,new Insets(15,Main.getStageWidth()/3,15,Main.getStageWidth()/3));
		VBox.setMargin(boxPlayer2,new Insets(15,Main.getStageWidth()/3,15,Main.getStageWidth()/3));
		VBox.setMargin(boxPlayer3,new Insets(15,Main.getStageWidth()/3,15,Main.getStageWidth()/3));
		VBox.setMargin(boxPlayer4,new Insets(15,Main.getStageWidth()/3,15,Main.getStageWidth()/3));
		
		double imageSize = Main.getStageHeight()/20+Main.getStageWidth()/40;

		if(p1Image != null)
		{
			p1Image.setFitHeight(imageSize);
			p1Image.setFitWidth(imageSize);
		}
		
		if(p2Image != null)
		{
			p2Image.setFitHeight(imageSize);
			p2Image.setFitWidth(imageSize);
		}
		
		if(p3Image != null)
		{			
			p3Image.setFitHeight(imageSize);
			p3Image.setFitWidth(imageSize);
		}
		
		if(p4Image != null)
		{			
			p4Image.setFitHeight(imageSize);
			p4Image.setFitWidth(imageSize);	
		}	
		
		boxPlayer1.setPrefHeight(Main.getStageHeight()/2);
		boxPlayer1.setPrefWidth(Main.getStageWidth()/4.1);
		
		boxPlayer2.setPrefHeight(Main.getStageHeight()/2);
		boxPlayer2.setPrefWidth(Main.getStageWidth()/4.1);
		
		boxPlayer3.setPrefHeight(Main.getStageHeight()/2);
		boxPlayer3.setPrefWidth(Main.getStageWidth()/4.1);
		
		boxPlayer4.setPrefHeight(Main.getStageHeight()/2);
		boxPlayer4.setPrefWidth(Main.getStageWidth()/4.1);
		
		BackgroundSize size = new BackgroundSize(Main.getStageWidth(), Main.getStageHeight(), false, false, false, false);
		BackgroundImage image = new BackgroundImage(backgroundImage, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, size);
		this.setBackground(new Background(image));
	}
}

