package vue;

import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.css.PseudoClass;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import modele.Account;
import utilitaire.AccountsManager;

public class DisplayLoginOptions {

	private final static PseudoClass errorClass = PseudoClass.getPseudoClass("error");
	private static Optional<Account> result;

	//Show the connection dialog allowing to connect by providing the right nickname/password pair.
	public static Optional<Account> launchDisplayConnection()
	{				
		Dialog<Account> dialog = new Dialog<>();
		dialog.setTitle("Log in");

		ButtonType applyButtonType = new ButtonType("Apply", ButtonData.APPLY);
		ButtonType cancelButtonType = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(applyButtonType, cancelButtonType);
		final Button btOk = (Button) dialog.getDialogPane().lookupButton(applyButtonType);

		GridPane grid = new GridPane();
		grid.getStylesheets().add(DisplayLoginOptions.class.getResource("MainMenu.css").toExternalForm());
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField nickname = new TextField();
		nickname.setPromptText("nickname");
		Tooltip nicknameTt = new Tooltip();
		PasswordField password = new PasswordField();
		password.setPromptText("password");
		Tooltip passwordTt = new Tooltip();

		nickname.focusedProperty().addListener(new ChangeListener<Boolean>()
		{
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
			{
				//The textfield lost the focus, so we check if the value is correct
				if (!newPropertyValue)
				{
					if(AccountsManager.getPlayer(nickname.getText()) == null)
					{
						nickname.setTooltip(nicknameTt);
						nicknameTt.setText("There is no account with this nickname in our database");
						nickname.pseudoClassStateChanged(errorClass, true);
					}
					else
					{
						nickname.setTooltip(null);
						nickname.pseudoClassStateChanged(errorClass, false);
					}
				}
			}
		});

		password.focusedProperty().addListener(new ChangeListener<Boolean>()
		{
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
			{
				//The textfield lost the focus, so we check if the value is correct
				if (!newPropertyValue)
				{
					Account account = AccountsManager.getPlayer(nickname.getText());
					if(account == null || !password.getText().equals(account.getPassword()))
					{
						password.setTooltip(passwordTt);
						passwordTt.setText("This password does not match the current nickname");
						password.pseudoClassStateChanged(errorClass, true);
					}
					else
					{
						password.setTooltip(null);
						password.pseudoClassStateChanged(errorClass, false);
					}
				}
			}
		});

		btOk.addEventFilter(
				ActionEvent.ACTION, 
				event -> {
					// Check whether some conditions are fulfilled
					if(nickname.getPseudoClassStates().contains(errorClass) || password.getPseudoClassStates().contains(errorClass))
					{
						//Consume the event to prevent the closing of the dialog
						event.consume();
					}
				}
				);

		grid.add(new Label("Nickname:"), 0, 0);
		grid.add(nickname, 1, 0);
		grid.add(new Label("Password:"), 0, 1);
		grid.add(password, 1, 1);

		dialog.getDialogPane().setContent(grid);
		Platform.runLater(() -> nickname.requestFocus());

		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == applyButtonType) {
				return AccountsManager.getPlayer(nickname.getText());
			}
			return null;
		});

		result = dialog.showAndWait();
		return result; 
	}

	public static Optional<Account> launchDisplayInscription()
	{
		//Create inscription's dialog
		Dialog<Account> dialog = new Dialog<>();
		dialog.setTitle("Create a new account");	
		Map<String,Color> colorsFromString = new TreeMap<String,Color>();
		colorsFromString.put("Blue", Color.BLUE);
		colorsFromString.put("Orange", Color.ORANGE);
		colorsFromString.put("Green", Color.GREEN);
		colorsFromString.put("Yellow", Color.YELLOW);
		colorsFromString.put("Pink", Color.PINK);
		colorsFromString.put("Brown", Color.BROWN);


		ButtonType applyButtonType = new ButtonType("Apply", ButtonData.APPLY);
		ButtonType cancelButtonType = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(applyButtonType, cancelButtonType);
		final Button btOk = (Button) dialog.getDialogPane().lookupButton(applyButtonType);

		GridPane grid = new GridPane();
		grid.getStylesheets().add(DisplayLoginOptions.class.getResource("MainMenu.css").toExternalForm());
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField nickname = new TextField();
		Tooltip nicknameTt = new Tooltip();
		nickname.setPromptText("nickname");
		TextField email = new TextField();
		Tooltip emailTt = new Tooltip();
		email.setPromptText("mail");
		PasswordField password = new PasswordField();
		Tooltip passwdTt = new Tooltip();
		password.setPromptText("password");
		PasswordField password2 = new PasswordField();
		Tooltip passwd2Tt = new Tooltip();
		password2.setPromptText("re-enter password");
		ComboBox<String> colorPicker = new ComboBox<String>();
		colorPicker.getItems().addAll("Blue","Orange","Green","Yellow","Pink","Brown");
		colorPicker.setValue("Blue");

		nickname.focusedProperty().addListener(new ChangeListener<Boolean>()
		{
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
			{
				//The textfield lost the focus, so we check if the value is correct
				if (!newPropertyValue)
				{
					if(nickname.getText().equals("") || AccountsManager.getPlayer(nickname.getText()) != null)
					{
						nickname.setTooltip(nicknameTt);
						nickname.pseudoClassStateChanged(errorClass, true);
						if(nickname.getText().equals(""))
						{
							nicknameTt.setText("Please enter a nickname.");
						}
						else
						{
							nicknameTt.setText("An account with this nickname already exist.");
						}
					}
					else 
					{
						nickname.setTooltip(null);
						nickname.pseudoClassStateChanged(errorClass, false);
					}
				}
			}
		});

		email.focusedProperty().addListener(new ChangeListener<Boolean>()
		{
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
			{
				//Set a regex to verify if the mail address is correct
				final String mailRegex = "(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
				if (!newPropertyValue)
				{
					if(!email.getText().equals("") && !email.getText().matches(mailRegex))
					{
						email.setTooltip(emailTt);
						emailTt.setText("This email does not match the standart email format.");
						email.pseudoClassStateChanged(errorClass, true);
					}
					else
					{
						email.setTooltip(null);
						email.pseudoClassStateChanged(errorClass, false);
					}
				}
			}
		});

		password.focusedProperty().addListener(new ChangeListener<Boolean>()
		{
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
			{
				//The textfield lost the focus, so we check if the value is correct
				if (!newPropertyValue)
				{
					if(password.getText().equals(""))
					{
						password.setTooltip(passwdTt);
						passwd2Tt.setText("You must enter a password");
						password.pseudoClassStateChanged(errorClass, true);
					}
					else
					{
						password.setTooltip(null);
						password.pseudoClassStateChanged(errorClass, false);
					}
				}
			}
		});

		password2.focusedProperty().addListener(new ChangeListener<Boolean>()
		{
			@Override
			public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue)
			{
				//The textfield lost the focus, so we check if the value is correct
				if (!newPropertyValue)
				{
					if(!password.getText().equals(password2.getText()))
					{
						password2.setTooltip(passwd2Tt);
						passwd2Tt.setText("The two passwords does not match");
						password2.pseudoClassStateChanged(errorClass, true);
					}
					else
					{
						password2.setTooltip(null);
						password2.pseudoClassStateChanged(errorClass, false);
					}
				}
			}
		});
		btOk.addEventFilter(
				ActionEvent.ACTION, 
				event -> {
					// Check whether some conditions are fulfilled
					if(nickname.getPseudoClassStates().contains(errorClass) || email.getPseudoClassStates().contains(errorClass) || password2.getPseudoClassStates().contains(errorClass))
					{
						//Consume the event to prevent the closing of the dialog
						event.consume();
					}
				}
				);

		grid.add(new Label("Nickname:"), 0, 1);
		grid.add(nickname, 1, 1);
		grid.add(new Label("E-mail:"), 0, 2);
		grid.add(email, 1, 2);
		grid.add(new Label("Password:"), 0, 3);
		grid.add(password, 1, 3);
		grid.add(new Label("Re-enter password:"), 0, 4);
		grid.add(password2, 1, 4);
		grid.add(new Label("Pawn color:"), 0, 5);
		grid.add(colorPicker, 1, 5);

		dialog.getDialogPane().setContent(grid);
		Platform.runLater(() -> nickname.requestFocus());

		dialog.setResultConverter(dialogButton -> {
			if (dialogButton == applyButtonType) {
				return new Account(nickname.getText(), password.getText(), email.getText(), colorsFromString.get(colorPicker.getValue()));
			}
			return null;
		});
		result = dialog.showAndWait();
		return result;
	}

	public static Optional<Account> launchDisplayGuest()
	{
		Dialog<Account> dialog = new Dialog<>();
		dialog.setTitle("Connect as a Guest");	
		Map<String,Color> colorsFromString = new TreeMap<String,Color>();
		colorsFromString.put("Blue", Color.BLUE);
		colorsFromString.put("Orange", Color.ORANGE);
		colorsFromString.put("Green", Color.GREEN);
		colorsFromString.put("Yellow", Color.YELLOW);
		colorsFromString.put("Pink", Color.PINK);
		colorsFromString.put("Brown", Color.BROWN);

		ButtonType applyButtonType = new ButtonType("Apply", ButtonData.APPLY);
		ButtonType cancelButtonType = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
		dialog.getDialogPane().getButtonTypes().addAll(applyButtonType, cancelButtonType);
		final Button btOk = (Button) dialog.getDialogPane().lookupButton(applyButtonType);

		GridPane grid = new GridPane();
		grid.getStylesheets().add(DisplayLoginOptions.class.getResource("MainMenu.css").toExternalForm());
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(20, 150, 10, 10));

		TextField nickname = new TextField();
		nickname.setPromptText("nickname");
		ComboBox<String> colorPicker = new ComboBox<String>(); 
		colorPicker.getItems().addAll("Blue","Orange","Green","Yellow","Pink","Brown");
		colorPicker.setValue("Blue");

		grid.add(new Label("Nickname:"), 0, 1);
		grid.add(nickname, 1, 1);
		grid.add(new Label("Pawn color:"), 0, 2);
		grid.add(colorPicker, 1, 2);

		dialog.getDialogPane().setContent(grid);
		Platform.runLater(() -> nickname.requestFocus());

		dialog.setResultConverter(dialogButton  -> {
			if (dialogButton == applyButtonType) {
				return new Account(nickname.getText(),null,null, colorsFromString.get(colorPicker.getValue()));
			}
			return null;
		});
		result = dialog.showAndWait();
		return result;
	}

}
