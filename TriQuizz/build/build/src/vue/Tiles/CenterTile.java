package vue.Tiles;

import java.util.ArrayList;
import java.util.List;

import application.Main;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import modele.GameSession;
import utilitaire.AccountsManager;
import vue.DisplayQuestion;
import vue.LocalGameBoard;

public class CenterTile extends Tile
{
	private List<Tile> nextTiles;
	
	public CenterTile() 
	{
		super(null);
		//Ignore all the mouse event
		setMouseTransparent(true);
		nextTiles = new ArrayList<>();
		//Get the image for the center tile
		image = new Image(getClass().getResourceAsStream("TilesImages/centreq.png"),centerTileWidth,centerTileHeight,true,true);
		this.setImage(image);
	}

	public void addNextTile(Tile t)
	{
		nextTiles.add(t);
	}
	
	public List<Tile> getNextTiles()
	{
		return nextTiles;
	}
}
