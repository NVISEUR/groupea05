package vue.Tiles;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import modele.Category;
import modele.GameSession;
import utilitaire.AccountsManager;
import vue.DisplayQuestion;
import vue.LocalGameBoard;

public class CheeseTile extends Tile
{
	Tile rightTile, leftTile;
	
	public Tile getRightTile() {
		return rightTile;
	}

	public void setRightTile(Tile rightTile) {
		this.rightTile = rightTile;
	}

	public Tile getLeftTile() {
		return leftTile;
	}

	public void setLeftTile(Tile leftTile) {
		this.leftTile = leftTile;
	}

	public CheeseTile(Category cat) 
	{
		super(cat);
		//Ignore all the mouse event
		setMouseTransparent(true);
		
		//Get the image for the cheese tile
		image = new Image(getClass().getResourceAsStream(getImageFromCategory(true)),tileSize,tileSize,true,true);
		this.setImage(image);
	}

}
