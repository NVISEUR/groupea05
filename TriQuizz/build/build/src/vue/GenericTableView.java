package vue;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Optional;

import com.sun.javafx.scene.control.skin.TableColumnHeader;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;

/**
 * Represents a generic TableView.
 * @author Nicolas Viseur
 *
 * @param <T> The type of the objects we are having in the table.
 */
public class GenericTableView<T> extends TableView<T>
{
	/**
	 * Create a new generic TableView.
	 * @param data The list of the objects we are putting into the TableView.
	 * @param type The class of the objects we are putting into the TableView.
	 */
	@SuppressWarnings("unchecked")
	public GenericTableView(ObservableList<Object> data, Class<T> type) 
	{
		//Send the list to the superclass.
		super((ObservableList<T>) data);
		
		//For each non-static non-transient field in the class,
		//We are adding a new column to the TableView
		for (Field f : type.getDeclaredFields()) 
		{	  
			Method getter=null;
			try 
			{							
				for(Method m : type.getDeclaredMethods())
				{								
					if(m.getName().equalsIgnoreCase("get"+f.getName()))
					{
						getter = m;
					}
				}
			}
			catch(Exception ex)
			{
				//Catch general exception( either that or 50 try/catch )
				ex.printStackTrace();
			}
			final Method m = getter;
			f.setAccessible(true);      
			if(!Modifier.isStatic(f.getModifiers()) &&
					!Modifier.isTransient(f.getModifiers()))
			{
				if(f.getType().equals(Color.class))
				{
					TableColumn<T, String> tc = new TableColumn<>(f.getName());
					tc.setCellValueFactory(cellData -> {
						Color color = null;
						try {
							color = (Color) m.invoke(cellData.getValue());
						} catch (IllegalArgumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						String colAsString;
						if(color.equals(Color.BLUE))
						{
							colAsString = "Blue";
						}
						else if(color.equals(Color.ORANGE))
						{
							colAsString = "Orange";
						}
						else if(color.equals(Color.GREEN))
						{
							colAsString = "Green";
						}
						else if(color.equals(Color.YELLOW))
						{
							colAsString = "Yellow";
						}
						else if(color.equals(Color.PINK))
						{
							colAsString = "Pink";
						}
						else if(color.equals(Color.BROWN))
						{
							colAsString = "Brown";
						}
						else
						{
							colAsString = "Special";
						}

						return new ReadOnlyStringWrapper(colAsString);
					});
					getColumns().add(tc);
				}
				else if(f.getType().equals(boolean.class))
				{
					TableColumn<T, String> tc = new TableColumn<>(f.getName());
					tc.setCellValueFactory(cellData -> {
						boolean admin = false;
						try {
							admin = (boolean) m.invoke(cellData.getValue());
						} catch (IllegalArgumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (InvocationTargetException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						String adminAsString;
						if(admin == true)
						{
							adminAsString = "Yes";
						}
						else
						{
							adminAsString = "No";
						}

						return new ReadOnlyStringWrapper(adminAsString);
					});
					tc.setMaxWidth(500);
					getColumns().add(tc);
				}
				else
				{
					TableColumn<T, ?> tc = new TableColumn<>(f.getName());
					tc.setCellValueFactory(new PropertyValueFactory<>(f.getName()));
					getColumns().add(tc);
				}
			}
		}

		//When we double click on a row, show a dialog allowing us to either remove this row or edit it.
		setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent click) {
				if(click.getClickCount() == 2 && !(click.getTarget() instanceof TableColumnHeader))
				{
					Dialog<ButtonType> dialogP = new Dialog<>();
					dialogP.setTitle("Edit prompt");
					dialogP.setHeaderText("What do you want to do whit this row ?");
					ButtonType editButtonType = new ButtonType("Edit it", ButtonData.OK_DONE);
					ButtonType deleteButtonType = new ButtonType("Delete it", ButtonData.OK_DONE);
					ButtonType cancelButtonType = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
					dialogP.getDialogPane().getButtonTypes().addAll(editButtonType,deleteButtonType, cancelButtonType);
					Optional<ButtonType> result = dialogP.showAndWait();
					
					//When we get the result of the dialog, update the current row with the new values
					if (result.get() == editButtonType)
					{
						T item = getSelectionModel().getSelectedItem();
						GenericObjectEditor<T> editor = new GenericObjectEditor<T>(type, item);
						Optional<T> res = editor.showAndWait();

						if(res.isPresent())
						{
							T newItem = res.get();

							//First, for each non-static non-transient fields, gets the Getters and Setters
							for(Field f : item.getClass().getDeclaredFields())
							{
								if(!Modifier.isStatic(f.getModifiers()) &&
										!Modifier.isTransient(f.getModifiers()))
								{
									try 
									{
										Method setter = null;
										Method getter = null;

										for(Method m : item.getClass().getDeclaredMethods())
										{
											if(m.getName().equalsIgnoreCase("set"+f.getName()))
											{
												setter = m;
											}

											if(m.getName().equalsIgnoreCase("get"+f.getName()))
											{
												getter = m;
											}
										}

										//Then use them to update the row.
										setter.invoke(item, getter.invoke(newItem));
									}
									catch(Exception ex)
									{
										//Catch general exception( either that or 50 try/catch )
										ex.printStackTrace();
									}
								}
							}

							refresh();
						}
					}
					else if(result.get() == deleteButtonType)
					{
						Dialog<ButtonType> dialog = new Dialog<>();
						dialog.setTitle("Delete ?");
						dialog.setHeaderText("Do you really wish to delete this row ?");
						ButtonType applyButtonType = new ButtonType("Apply", ButtonData.OK_DONE);
						dialog.getDialogPane().getButtonTypes().addAll(applyButtonType, cancelButtonType);
						Optional<ButtonType> resultt = dialog.showAndWait();
						if (resultt.get() == applyButtonType)
						{
							T row = getSelectionModel().getSelectedItem();
							data.remove(row);
						}	
					}
				}				
			}
		});
	}
}
