package vue;


import java.util.Optional;
import java.util.OptionalInt;
import java.util.stream.IntStream;

import application.Main;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import modele.Account;
import modele.Lobby;
import modele.Resizable;

public class EndGame extends BorderPane implements Resizable
{

	private Button backToMainMenu;
	private Lobby lobby;
	private VBox boxPlayer1, boxPlayer2, boxPlayer3, boxPlayer4, mainBox;
	private Label labelPlayer1, labelPlayer2, labelPlayer3, labelPlayer4;
	private Label scorePlayer1, scorePlayer2, scorePlayer3, scorePlayer4;
	private HBox boxNbPlayer, boxQuitGame;
	private Image backgroundImage;
	private ImageView p1Image,p2Image,p3Image,p4Image;

	public EndGame(Lobby lob) 
	{
		backgroundImage = new Image(getClass().getResourceAsStream("Background/Images/FondTexture.png"));

		getStylesheets().addAll(getClass().getResource("transback.css").toExternalForm());
		lobby = lob;
		boxNbPlayer = new HBox();
		boxPlayer1 = new VBox();
		boxPlayer2 = new VBox();
		boxPlayer3 = new VBox();
		boxPlayer4 = new VBox();
		HBox boxAddPlayer = new HBox();
		boxQuitGame = new HBox();
		mainBox = new VBox();
		HBox centerBox = new HBox();

		labelPlayer2 = new Label("");
		labelPlayer3 = new Label("");
		labelPlayer4 = new Label("");
		scorePlayer2 = new Label("");
		scorePlayer3 = new Label("");
		scorePlayer4 = new Label("");

		centerBox.getChildren().add(boxPlayer1);

		//Add to the boxPlayers each informations of the player1
		labelPlayer1 = getPlayerLabel(lob.getPlayer(0));
		scorePlayer1 = new Label(String.valueOf(lob.getPlayer(0).getSession().getCurrency()) + " pts");
		boxPlayer1.getChildren().add(labelPlayer1);
		boxPlayer1.getChildren().add(scorePlayer1);
		boxPlayer1.setStyle("-fx-background-color: #f7f1dc");
		p1Image = lob.getPlayer(0).getSession().getImageView();
		boxPlayer1.getChildren().add(p1Image);
		boxPlayer1.setAlignment(Pos.TOP_CENTER);
		boxPlayer1.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));


		if(lob.getPlayer(1) != null)
		{
			//Add to the boxPlayers each informations of the player2
			labelPlayer2 = getPlayerLabel(lob.getPlayer(1));
			scorePlayer2 = new Label(String.valueOf(lob.getPlayer(1).getSession().getCurrency()) + " pts");
			boxPlayer2.getChildren().add(labelPlayer2);
			boxPlayer2.getChildren().add(scorePlayer2);
			boxPlayer2.setStyle("-fx-background-color: #f7f1dc");
			p2Image = lob.getPlayer(1).getSession().getImageView();
			boxPlayer2.getChildren().add(p2Image);
			boxPlayer2.setAlignment(Pos.TOP_CENTER);
			boxPlayer2.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
			centerBox.getChildren().add(boxPlayer2);

			if(lob.getPlayer(2) != null)
			{
				//Add to the boxPlayers each informations of the player3
				labelPlayer3 = getPlayerLabel(lob.getPlayer(2));
				scorePlayer3 = new Label(String.valueOf(lob.getPlayer(2).getSession().getCurrency()) + " pts");
				boxPlayer3.getChildren().add(labelPlayer3);
				boxPlayer3.getChildren().add(scorePlayer3);
				boxPlayer3.setStyle("-fx-background-color: #f7f1dc");
				p3Image = lob.getPlayer(2).getSession().getImageView();
				boxPlayer3.getChildren().add(p3Image);
				boxPlayer3.setAlignment(Pos.TOP_CENTER);
				boxPlayer3.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
				centerBox.getChildren().add(boxPlayer3);

				if(lob.getPlayer(3) != null)
				{
					//Add to the boxPlayers each informations of the player4
					labelPlayer4 = getPlayerLabel(lob.getPlayer(3));
					scorePlayer4 = new Label(String.valueOf(lob.getPlayer(3).getSession().getCurrency()) + " pts");
					boxPlayer4.getChildren().add(labelPlayer4);
					boxPlayer4.getChildren().add(scorePlayer4);
					boxPlayer4.setStyle("-fx-background-color: #f7f1dc");
					p4Image = lob.getPlayer(3).getSession().getImageView();
					boxPlayer4.getChildren().add(p4Image);
					boxPlayer4.setAlignment(Pos.TOP_CENTER);
					boxPlayer4.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));
					centerBox.getChildren().add(boxPlayer4);
				}
			}
		}
		int[] tab = {0};
		
		for(int i = 0;i < lob.length() ; i++)
		{
			tab[i] = lob.getPlayer(i).getSession().getCurrency();
		}
		//Get the max score of the game
		OptionalInt max = IntStream.of(tab).max();
		if(lob.getPlayer(0) != null && lob.getPlayer(0).getSession().getCurrency() == max.getAsInt())
		{
			boxPlayer1.setStyle("-fx-background-color: #EFD807");
		}
		if(lob.getPlayer(1) != null && lob.getPlayer(1).getSession().getCurrency() == max.getAsInt())
		{
			boxPlayer2.setStyle("-fx-background-color: #EFD807");
		}

		if(lob.getPlayer(2) != null && lob.getPlayer(2).getSession().getCurrency() == max.getAsInt())
		{
			boxPlayer3.setStyle("-fx-background-color: #EFD807");
		}

		if(lob.getPlayer(3) != null && lob.getPlayer(3).getSession().getCurrency() == max.getAsInt())
		{
			boxPlayer4.setStyle("-fx-background-color: #EFD807");
		}



		mainBox.setAlignment(Pos.TOP_LEFT);

		boxNbPlayer.setAlignment(Pos.TOP_RIGHT);

		//Add to the boxQuitGame buttons to go back to the main menu
		boxQuitGame.getChildren().add(getBackToMainMenu());
		boxQuitGame.setAlignment(Pos.BOTTOM_CENTER);

		//add each boxes
		mainBox.getChildren().add(boxNbPlayer);
		mainBox.getChildren().add(centerBox);
		mainBox.getChildren().add(boxAddPlayer);
		mainBox.getChildren().add(boxQuitGame);
		setTop(mainBox);

		resize();
	}
	public Label getPlayerLabel(Account acc) 
	{
		Label playerNickname = new Label();
		playerNickname.setWrapText(true);
		playerNickname.setTextAlignment(TextAlignment.CENTER);

		playerNickname.setText(acc.getNickname());
		//Return the player's nickname
		return playerNickname; 
	}

	public Button getBackToMainMenu() {
		if(backToMainMenu == null)
		{
			backToMainMenu = new Button("Back to main menu");
			backToMainMenu.setStyle("-fx-background-color: 	#DDAAFF;");

			backToMainMenu.setOnAction(e -> Main.getStage().setScene(Main.getMainMenu()));
		}
		return backToMainMenu; 
	}

	@Override
	public void resize() 
	{
		final double normalButtonsWidth = Main.getStageWidth()/7;
		final double normalButtonsHeight = Main.getStageHeight()/13;

		getBackToMainMenu().setPrefHeight(normalButtonsHeight);
		getBackToMainMenu().setPrefWidth(normalButtonsWidth);		

		double fontSize = Main.getStageWidth()/125+Main.getStageHeight()/75;
		
		labelPlayer1.setFont(new Font(fontSize));
		labelPlayer2.setFont(new Font(fontSize));
		labelPlayer3.setFont(new Font(fontSize));
		labelPlayer4.setFont(new Font(fontSize));
		
		scorePlayer1.setFont(new Font(fontSize));
		scorePlayer2.setFont(new Font(fontSize));
		scorePlayer3.setFont(new Font(fontSize));
		scorePlayer4.setFont(new Font(fontSize));

		VBox.setMargin(boxPlayer1,new Insets(15,Main.getStageWidth()/3,15,Main.getStageWidth()/3));
		VBox.setMargin(boxPlayer2,new Insets(15,Main.getStageWidth()/3,15,Main.getStageWidth()/3));
		VBox.setMargin(boxPlayer3,new Insets(15,Main.getStageWidth()/3,15,Main.getStageWidth()/3));
		VBox.setMargin(boxPlayer4,new Insets(15,Main.getStageWidth()/3,15,Main.getStageWidth()/3));


		p1Image.setFitHeight(Main.getStageHeight()/20+Main.getStageWidth()/40);
		p1Image.setFitWidth(Main.getStageHeight()/20+Main.getStageWidth()/40);

		if(p2Image != null)
		{
			p2Image.setFitHeight(Main.getStageHeight()/4);
			p2Image.setFitWidth(Main.getStageHeight()/4);
		}

		if(p3Image != null)
		{			
			p3Image.setFitHeight(Main.getStageHeight()/4);
			p3Image.setFitWidth(Main.getStageHeight()/4);
		}

		if(p4Image != null)
		{			
			p4Image.setFitHeight(Main.getStageHeight()/4);
			p4Image.setFitWidth(Main.getStageHeight()/4);	
		}	

		boxPlayer1.setPrefHeight(Main.getStageHeight()/2);
		boxPlayer1.setPrefWidth(Main.getStageWidth()/4.1);

		boxPlayer2.setPrefHeight(Main.getStageHeight()/2);
		boxPlayer2.setPrefWidth(Main.getStageWidth()/4.1);

		boxPlayer3.setPrefHeight(Main.getStageHeight()/2);
		boxPlayer3.setPrefWidth(Main.getStageWidth()/4.1);

		boxPlayer4.setPrefHeight(Main.getStageHeight()/2);
		boxPlayer4.setPrefWidth(Main.getStageWidth()/4.1);

		boxNbPlayer.setSpacing(Main.getStageWidth()/20);

		boxQuitGame.setSpacing(Main.getStageWidth()/20);

		mainBox.setSpacing(Main.getStageHeight()/30);

		BackgroundSize size = new BackgroundSize(Main.getStageWidth(), Main.getStageHeight(), false, false, false, false);
		BackgroundImage image = new BackgroundImage(backgroundImage, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, size);
		this.setBackground(new Background(image));
	}
}

