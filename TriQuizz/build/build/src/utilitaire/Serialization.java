package utilitaire;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.HashSet;
import java.util.TreeSet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;
import com.sun.glass.ui.CommonDialogs.Type;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import modele.Deck;
import modele.Account;

public abstract class Serialization 
{
	/**
	 * Write the object into a json file
	 * @param obj the object to write
	 * @param fileName the file name to be written in
	 * @param type the type of the object
	 */
	public static void writeToJson(Object obj, String fileName, java.lang.reflect.Type type)
	{		
		try (Writer writer = new FileWriter(fileName)) {
			Gson gson = new GsonBuilder().setPrettyPrinting().create();		
			//Write the object in a JSON file
			writer.write(gson.toJson(obj,type));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	/**
	 * Read an object from a Json file
	 * @param fileName the file containing the json
	 * @param type the type of object to be read
	 * @return the object read
	 * @throws FileNotFoundException
	 */
	public static Object readFromJson(String fileName,java.lang.reflect.Type type) throws FileNotFoundException
	{
		try(JsonReader reader = new JsonReader(new FileReader(fileName))) {
			Gson gson = new Gson();
			//Get JSON's values
			return gson.fromJson(reader, type);
		} catch (FileNotFoundException e) {
			throw new FileNotFoundException();
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		return null;
	}
}
