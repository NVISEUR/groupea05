package modele;

public class Question {

	private String author;	
	private Category category;
	private String statement;
	private String answer;
	private transient boolean isUsed;

	public Question(String author,Category category, String statement,String answer) {

		if(statement.contains("?")) {
			this.statement = statement;
		}else {
			this.statement = statement += " ?";
		}
		this.answer = answer;

		this.author = author;

		this.category = category;
	}

	public Question(Question question) {
		this.statement = question.statement;
		this.answer = question.answer;		
		this.author = question.author;		
		this.category = question.category;
	}
	
	/**
	 * Copy the data from another question
	 * @param question to copy
	 */
	public void copyDataFromQuestion(Question question)
	{
		//Set question with copy of values from another question
		this.statement = question.statement;
		this.answer = question.answer;		
		this.author = question.author;		
		this.category = question.category;
	}
	
	/**
	 * Return true if the question have been used
	 * @return
	 */
	public boolean haveBeenUsed()
	{
		return isUsed;
	}
	
	/**
	 * Use a question
	 */
	public void use()
	{
		this.isUsed = true;
	}

	/**
	 * Return the statement of the question
	 * @return the statement
	 */
	public String getStatement() {
		return statement;
	}

	/**
	 * Return the answer of the question 
	 * @return the answer
	 */
	public String getAnswer() {
		return answer;
	}
	/**
	 *  Return the author of the question
	 * @return the author
	 */
	public String getAuthor() {
		return author;
	}
	/**
	 * Return the category of the question
	 * @return the category
	 */
	public Category getCategory() {
		return category;
	}
	

	/**
	 * Set the author of a question
	 * @param author the author
	 */
	public void setAuthor(String author) {
		this.author = author;
	}
	
	/**
	 * Set the category of a question
	 * @param category the category
	 */
	public void setCategory(Category category) {
		this.category = category;
	}
	
	/**
	 * Set the statement of a question
	 * @param statement the statement
	 */
	public void setStatement(String statement) {
		this.statement = statement;
	}
	
	/**
	 * Set the answer of a question
	 * @param answer the answer
	 */
	public void setAnswer(String answer) {
		this.answer = answer;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Question other = (Question) obj;
		if (statement == null) {
			if (other.statement != null)
				return false;
		} else if (!statement.equals(other.statement))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return category+" : "+statement+" "+answer+" by "+author;
	}
	

	public Question clone()
	{
		return new Question(this);
	}
}
