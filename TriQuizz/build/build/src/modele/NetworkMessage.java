package modele;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;


/*
 * Every message have a similar shape
 * It start by the type of the message
 * Then the data
 * Everything is separated by the chosen separator
 */
public abstract class NetworkMessage 

{
	public static final String SEPARATOR = "_";
	
	/**
	 * Launch game message
	 * Special message who do not need to be decoded
	 * @return Launch game log 
	 */
	public static String encodeLaunchGameMessage()
	{
		return "LaunchGame"+SEPARATOR;
	}
	
	/**
	 * Ping message 
	 * @return Ping log 
	 */
	public static String encodePingMessage()
	{
		return "Ping";
	}
	
	/**
	 * Difficulty modification message encoded
	 * 
	 * @param diff difficulty parameter 
	 * @return difficulty modified log
	 */
	public static String encodeModifyDifficulty(String diff)
	{
		return "ModifyDifficulty"+SEPARATOR+diff;
	}
	
	/**
	 * Difficulty modification message decoded
	 * @param message to decode
	 * @return data
	 */
	public static String decodeModifyDifficulty(String message)
	{
		String[] data = message.split(SEPARATOR);
		
		return data[1];
	}
	
	/**
	 * Returns Player ready message (encoded)
	 * @param nickname the nickname of the player
	 * @param ready the status of the player
	 * @return the player ready log
	 */
	public static String encodePlayerReadyMessage(String nickname, boolean ready)
	{
		return "PlayerReady"+SEPARATOR+nickname+SEPARATOR+ready;
	}
	
	/**
	 * Returns the player ready message (decoded)
	 * @param message the message to decode
	 * @return the message decoded
	 */
	public static String[] decodePlayerReadyMessage(String message)
	{
		/*
		 * data[0] is the type
		 * data[1] is the nickname
		 * data[2] is the boolean value
		 */

		String[] data = message.split(SEPARATOR);
		
		return data;
	}
	
	/**
	 * Return player turn message ( encoded)
	 * @param nickname the Player nickname
	 * @return Player turn log encoded
	 */
	public static String encodePlayerTurnMessage(String nickname)
	{
		return "PlayerTurn"+SEPARATOR+nickname;		
	}
	
	/**
	 * Return player turn message decoded
	 * @param message the message to decode
	 * @return the message decoded
	 */
	public static String[] decodePlayerTurnMessage(String message)
	{
		return message.split(SEPARATOR);
	}
	/**
	 * Return player connection message encoded
	 * @param nickname the player name
	 * @param color the player color
	 * @param position the player position
	 * @return the player connection message encoded
	 */
	public static String encodePlayerConnectMessage(String nickname, Color color, int position)
	{
		//Convert the color into web color code
		String colorString = String.format( "#%02X%02X%02X",
	            (int)( color.getRed() * 255 ),
	            (int)( color.getGreen() * 255 ),
	            (int)( color.getBlue() * 255 ) );
		
		return "PlayerConnect"+SEPARATOR+nickname+SEPARATOR+colorString+SEPARATOR+position;
	}
	
	/**
	 * Return the player message decoded
	 * @param message the message to decoded
	 * @return the message decoded
	 */
	public static String[] decodePlayerConnectMessage(String message)
	{
		/*
		 * data[0] is the type
		 * data[1] is the name
		 * data[2] is the color
		 * data[3] is the position in the lobby list
		 */

		String[] data = message.split(SEPARATOR);
		
		return data;
	}	
	
	/**
	 * Return the player disconnection message encoded
	 * @param nickname the player nickname
	 * @param isHost true if the player is host
	 * @return the player disconnection message encoded
	 */
	public static String encodePlayerDisconnectMessage(String nickname, boolean isHost)
	{		
		return "PlayerDisconnect"+SEPARATOR+nickname+SEPARATOR+isHost;
	}
	
	/**
	 * Return the player disconnection message decoded
	 * @param message the message encoded
	 * @return the message decoded
	 */
	public static String[] decodePlayerDisconnectMessage(String message)
	{
		/*
		 * data[0] is the type
		 * data[1] is the name
		 * data[2] is the boolean host/not
		 */

		String[] data = message.split(SEPARATOR);
		
		return data;
	}
	
	/**
	 * Return dice result message encoded
	 * @param result the result
	 * @return the encoded message
	 */
	public static String encodeDiceResultMessage(int result)
	{
		return "DiceResult"+SEPARATOR+result;
	}
	
	/**
	 * Return the dice result message decoded
	 * @param message the message to decode
	 * @return the message decoded
	 */
	public static String[] decodeDiceResultMessage(String message)
	{
		return message.split(SEPARATOR);
	}
	
	/**
	 * Return player move message encoded
	 * @param xPos the axis x position 
	 * @param yPos the axis y position
	 * @param rotation the rotation axis
	 * @return the position encoded
	 */
	public static String encodePlayerMoveMessage(double xPos, double yPos, double rotation)
	{
		return "PlayerMove"+SEPARATOR+xPos+SEPARATOR+yPos+SEPARATOR+rotation;
	}
	
	/**
	 * Return player move message decoded
	 * @param message the message to decode
	 * @return the position decoded
	 */
	public static String[] decodePlayerMoveMessage(String message)
	{
		return message.split(SEPARATOR);
	}
	
	/**
	 * Return encoded player question reward message
	 * @param reward the reward to encode
	 * @return the reward message encoded 
	 */
	public static String encodePlayerQuestionRewardMessage(int reward)
	{
		return "PlayerQuestionReward"+SEPARATOR+reward;
	}
	
	/**
	 * Return Card data message encoded
	 * @param data the data to encode
	 * @return the data encoded
	 */
	public static String encodeCardsDataMessage(String data, int index)
	{
		return "CardsData"+SEPARATOR+index+SEPARATOR+data;
	}
	
	/**
	 * Return the card data message decoded
	 * @param message the message to decode
	 * @return the Card data decoded
	 */
	public static String[] decodeCardsDataMessage(String message)
	{
		//Need to be limited since there is some _ in the json
		return message.split(SEPARATOR, 3);
	}
	
	/**
	 * Return the nickname already present message encoded
	 * @param data the data to encode
	 * @return the message encoded
	 */
	public static String encodeNicknameAlreadyPresentMessage(String data)
	{
		return "NicknameAlreadyPresent"+SEPARATOR+data;
	}
	
	/**
	 * Return the nickname already present message decoded
	 * @param message the message to decode
	 * @return the message decoded
	 */
	public static String[] decodeNicknameAlreadyPresentMessage(String message)
	{
		return message.split(SEPARATOR);
	}
	
	/**
	 * Return duel player score message encoded
	 * @param nickname the nickname of the player
	 * @param reward the reward
	 * @param timeUsed the time spent to respond
	 * @return the duel player score encoded
	 */
	public static String encodeDuelPlayerScoreMessage(String nickname, int reward, int timeUsed)
	{
		return "DuelPlayerScore"+SEPARATOR+nickname+SEPARATOR+reward+SEPARATOR+timeUsed;
	}
	
	/**
	 * Return the duel player score message decoded
	 * @param message the message to decode
	 * @return the message decoded
	 */
	public static String[] decodeDuelPlayerScoreMessage(String message)
	{
		return message.split(SEPARATOR);
	}
	
	/**
	 * Return the duel start message encoded
	 * @param nickname the nickname of the player 
	 * @return the duel start message encoded
	 */
	public static String encodeDuelStartMessage(String nickname)
	{
		return "DuelStart"+SEPARATOR+nickname;
	}
	
	/**
	 * Return the duel start message decoded
	 * @param message the message to decode
	 * @return the message decoded
	 */
	public static String[] decodeDuelStartMessage(String message)
	{
		return message.split(SEPARATOR);
	}
	
	/**
	 * Return the duel win message encoded
	 * @param nickname the nickname of the winner
	 * @param reward the reward of the winner
	 * @return the message encoded
	 */
	public static String encodeDuelWinnerMessage(String nickname, int reward)
	{
		return "DuelWinner"+SEPARATOR+nickname+SEPARATOR+reward;
	}
	
	/**
	 * Return the duel winner message decoded
	 * @param message the message to decode
	 * @return the message decoded
	 */
	public static String[] decodeDuelWinnerMessage(String message)
	{
		return message.split(SEPARATOR);
	}
	
	/**
	 * Return the Override Levenshtein Setting message encoded 
	 * @param val the levenshtein tolerance setting
	 * @return the message encoded
	 */
	public static String encodeOverrideLevenshteinSettingMessage(String val)
	{
		return "OverrideLevenshteinSetting"+SEPARATOR+val;
	}
	
	/**
	 * Return the override Levenshtein setting message decoded
	 * @param message the message decoded
	 * @return the message decoded
	 */
	public static String[] decodeOverrideLevenshteinSettingMessage(String message)
	{
		return message.split(SEPARATOR);
	}
	
	/**
	 * Return the Levenshtein Override message encoded
	 * @param val the levenshtein tolerance setting 
	 * @return the message encoded
	 */
	public static String encodeOverrideLevenshteinMessage(boolean val)
	{
		return "OverrideLevenshtein"+SEPARATOR+val;
	}
	
	/**
	 * Return the Override Levenshtein message decoded
	 * @param message to decode
	 * @return the message decoded
	 */
	public static String[] decodeOverrideLevenshteinMessage(String message)
	{
		return message.split(SEPARATOR);
	}
	
	public static String encodeCardNumberMessage(int nbCard)
	{
		return "CardNumber"+SEPARATOR+nbCard;
	}
	
	public static String[] decodeCardNumberMessage(String message)
	{
		return message.split(SEPARATOR);
	}
}
