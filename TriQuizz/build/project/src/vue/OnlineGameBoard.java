package vue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import application.Main;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.geometry.Insets;
import javafx.geometry.Point2D;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.stage.StageStyle;
import modele.Account;
import modele.Category;
import modele.Deck;
import modele.DuelScore;
import modele.GameSession;
import modele.Lobby;
import modele.Card;
import modele.NetworkMessage;
import modele.Resizable;
import vue.Tiles.CenterTile;
import vue.Tiles.CheeseTile;
import vue.Tiles.NormalTile;
import vue.Tiles.Tile;


public class OnlineGameBoard extends Pane implements EventHandler<KeyEvent>, Resizable
{
	private Map<String, GameSession> playersSession;
	private Map<String, HBox> playersPoints;
	private String currentPlayerNickname = "";
	private MediaPlayer mp;
	private Dice dice;
	private Thread handlingMessageThread;
	private Account account;
	private int playerReady = 1;
	private Lobby lobby;
	private Label currentPlayerTurn = new Label("playerTurn");
	private Map<String, DuelScore> duelScore;	
	private int nbDuelPlayer = 0;
	private boolean haveSentCard = false;
	private int nbCard;

	/**
	 * Setup the Gameboard
	 * @param lobby
	 */
	public OnlineGameBoard(Lobby lobby, Account player)
	{		
		Main.getStage().setResizable(false);
		this.lobby = lobby;
		this.account = player;
		duelScore = new LinkedHashMap<>();
		playersPoints = new LinkedHashMap<>();
		playersSession = new LinkedHashMap<>();
		//The deck need to be the same for everyone
		//So only the host load the load the deck, and then he send the cards to the others
		if(account.getSession().isHost())
			Deck.getInstance().loadQuestions("quizz.json");

		setOnMouseClicked(new EventHandler<MouseEvent>() 
		{
			@Override
			public void handle(MouseEvent event) 
			{
				onClick(event.getX(), event.getY());				
			}
		});

		buildBoard();		

		getChildren().add(getDice().getImageView());

		CenterTile centerTile = null;

		for (Node children : getChildren()) 
		{
			if(children instanceof CenterTile)
			{
				centerTile = (CenterTile) children;
			}
		}

		for (Account account: lobby.getPlayers())
		{
			if(account != null)
			{
				//Set the player on the centre tile
				account.getSession().setCurrentTile(centerTile);
				//Add the player to the map
				playersSession.put(account.getNickname(), account.getSession());
				//Set the player position on the centre
				account.getSession().movePlayer((Main.getStageWidth()/2) - (account.getSession().getImageSize()/2), (Main.getStageHeight()/2) - (account.getSession().getImageSize()/2), 0);

				//Add the player image to the board
				this.getChildren().add(account.getSession().getImageView());
				account.getSession().getImageView().setFitHeight(Main.getStageHeight()/15);
				account.getSession().getImageView().setFitWidth(Main.getStageHeight()/15);
				this.getChildren().add(account.getSession().getNameBox());

				//Initialise the player stat box
				HBox box = new HBox();
				box.setPrefHeight(Main.getStageHeight()/6);
				box.setPrefWidth(Main.getStageWidth()/5);
				box.setStyle("-fx-background-color: #ffffff;" +
						"-fx-padding: 15px;" +
						"-fx-spacing: 10px;" +
						"-fx-font-size: " + Main.getStageHeight()/30 + ";" +
						"-fx-border-style: solid inside;" +
						"-fx-border-width: 5;" +
						"-fx-border-color: #" + account.getColor().toString().substring(2,8) + ";"+
						"-fx-background-radius: 15.0;"+
						"-fx-border-radius: 15.0");
				HBox.setMargin(box, new Insets(30,30,30,30));
				box.setLayoutX(10);
				box.setLayoutY(15+lobby.indexOf(account)*(box.getPrefHeight()+15));

				String cutName = account.getNickname().length() > 7 ?  account.getNickname().substring(0,7)+"... : " : account.getNickname()+" : ";

				box.getChildren().add(new Label(cutName));
				box.getChildren().add(new Label("0 pts"));

				playersPoints.put(account.getNickname(), box);
				this.getChildren().add(box);
			}
		}


		currentPlayerTurn.setLayoutX(Main.getStageWidth()/2);
		currentPlayerTurn.setStyle("-fx-background-color: #ffffff;" +
				"-fx-font-size: " + Main.getStageHeight()/35 + ";" +
				"-fx-padding: 3px;" +
				"-fx-spacing: 10px;" +
				"-fx-border-style: solid inside;" +
				"-fx-border-width: 2;");

		getChildren().add(currentPlayerTurn);

		startHandlingMessages();

		String path = getClass().getResource("Soundtrack/gameTheme.mp3").toExternalForm();
		Media media = new Media(path);
		mp = new MediaPlayer(media);
		mp.setCycleCount(MediaPlayer.INDEFINITE);
		mp.play();

		if(!account.getSession().isHost())
		{
			//This time parameters doesn't matter, it is just to tell the host that we loaded everything
			account.getSession().sendMessage(NetworkMessage.encodePlayerReadyMessage("", true));
		}
	}

	public void startHandlingMessages()
	{
		handlingMessageThread = new Thread(() -> 
		{
			while (!handlingMessageThread.isInterrupted()) 
			{
				try 
				{
					int size = account.getSession().getMessages().size();

					if (size > 0)
					{
						String message = account.getSession().getMessages().removeFirst();
						String type = message.split(NetworkMessage.SEPARATOR)[0];

						switch (type) 
						{
						case "CardNumber":
							String[] numberData = NetworkMessage.decodeCardNumberMessage(message);
							
							Platform.runLater(() ->
							{
								Alert alert = new Alert(AlertType.INFORMATION);
								alert.setTitle("Receiving cards");
								alert.setHeaderText("Please wait while we are receiving the cards from the host");
								alert.setContentText("When it's done, the dice will be throw.");
								alert.showAndWait();
							});
							
							nbCard = Integer.parseInt(numberData[1]);
						break;
						case "PlayerReady":
							if(account.getSession().isHost())
							{
								playerReady++;
								if(playerReady == playersSession.size() && !haveSentCard)
								{
									Platform.runLater(() ->
									{
										Alert alert = new Alert(AlertType.INFORMATION);
										alert.setTitle("Sending cards");
										alert.setHeaderText("Please wait while we are sending the cards to the other players");
										alert.setContentText("When it's done, the dice will be throw.");
										alert.showAndWait();
									});
									
									account.getSession().sendMessage(NetworkMessage.encodeCardNumberMessage(Deck.getInstance().getCards().size()));
									//Send the cards to synchronise
									Gson gson = new GsonBuilder().create();
									for(Card card : Deck.getInstance().getCards())
									{
										String cardsInfo = gson.toJson(card,Card.class);
										account.getSession().sendMessage(NetworkMessage.encodeCardsDataMessage(cardsInfo,Deck.getInstance().getCards().indexOf(card)));
									}
									
									haveSentCard = true;
									
									playerReady=1;
								}
								else if(playerReady == playersSession.size() && haveSentCard)
								{
									//The host is always the first to play
									account.getSession().sendMessage(NetworkMessage.encodePlayerTurnMessage(account.getNickname()));
									//Start the turn of the host

									currentPlayerNickname = account.getNickname();

									Platform.runLater(() ->
									{
										currentPlayerTurn.setText(currentPlayerNickname+"'s turn");
									});

									nextTurn();
								}
							}
							break;

						case "CardsData":
							String[] cardsData = NetworkMessage.decodeCardsDataMessage(message);
							Gson gson = new Gson();

							Deck.getInstance().getCards().add(Integer.parseInt(cardsData[1]),gson.fromJson(cardsData[2], Card.class));
							
							if(Integer.parseInt(cardsData[1]) == nbCard-1)
							{
								account.getSession().sendMessage(NetworkMessage.encodePlayerReadyMessage("", true));
							}
							break;

						case "PlayerTurn":
							String[] turnData = NetworkMessage.decodePlayerTurnMessage(message);

							currentPlayerNickname = turnData[1];

							getChildren().forEach(children -> {
								if(children instanceof Tile)
								{
									((Tile) children).disable();
								}
							});

							Platform.runLater(() ->
							{
								currentPlayerTurn.setText(currentPlayerNickname+"'s turn");
							});

							if(currentPlayerNickname.equals(account.getNickname()))
							{
								//Turn of this player, so start.
								nextTurn();
							}
							break;

						case "DiceResult":
							String[] diceData = NetworkMessage.decodeDiceResultMessage(message);

							showAllowedTiles(Integer.parseInt(diceData[1]));
							break;

						case "PlayerMove":
							String[] moveData = NetworkMessage.decodePlayerMoveMessage(message);

							double x = Double.parseDouble(moveData[1])*Main.getStageWidth();
							double y = Double.parseDouble(moveData[2])*Main.getStageHeight();

							Platform.runLater(()->
							{
								moveCurrentPlayer(x,y,Double.parseDouble(moveData[3]));
							});
							break;

						case "PlayerQuestionReward":
							String[] questionData = NetworkMessage.decodePlayerMoveMessage(message);

							Platform.runLater(()->
							{
								DisplayQuestion.stop();

								rewardPlayer(currentPlayerNickname,Integer.parseInt(questionData[1]));
							});
							break;

						case "PlayerDisconnect":
							String[] playerDisconnectData = NetworkMessage.decodePlayerDisconnectMessage(message);

							//The host left, disconnecting
							if(Boolean.valueOf(playerDisconnectData[2]) == true)
							{
								Platform.runLater(()->
								{
									backToMainMenu(true, "Connection with the host lost");
								});
							}
							//A player left, removing him
							else {
								Platform.runLater(()->
								{
									handlePlayerLeft(playerDisconnectData[1]);
								});
							}

							break;

						case "DuelStart":
							if(account.getSession().isHost())
							{
								String[] duelStartData = NetworkMessage.decodeDuelStartMessage(message);

								duelScore.put(duelStartData[1], null);
							}
							break;

						case "DuelPlayerScore":
							if(account.getSession().isHost())
							{
								String duelPLayerData[] = NetworkMessage.decodeDuelPlayerScoreMessage(message);

								Platform.runLater(()->
								{
									rewardDuel(duelPLayerData[1], Integer.parseInt(duelPLayerData[2]), Integer.parseInt(duelPLayerData[3]));
								});
							}
							break;

						case "DuelWinner":
							String duelWinnerData[] = NetworkMessage.decodeDuelPlayerScoreMessage(message);

							Platform.runLater(()->
							{
								rewardDuelWinner(duelWinnerData[1],Integer.parseInt(duelWinnerData[2]));
							});

							break;
						}
					}

					Thread.sleep(100);
				} catch (InterruptedException e) 
				{
					Thread.currentThread().interrupt();

					break;
				}
			}
		});

		//Make sure the thread is stopped when the program exit
		handlingMessageThread.setDaemon(true);
		handlingMessageThread.start();
	}

	public void rewardDuel(String nickname, int reward, int timeUsed)
	{
		duelScore.put(nickname, new DuelScore(reward, timeUsed));

		nbDuelPlayer++;

		System.out.println("reward "+nickname);

		if(nbDuelPlayer == duelScore.size())
		{
			String winner = "";
			int highestReward = 0;
			int lowestTimeUsed = 61;

			for (String name : duelScore.keySet()) 
			{
				if(duelScore.get(name).getReward() > highestReward)
				{
					winner = name;
					highestReward = duelScore.get(name).getReward();
				}
				else if(duelScore.get(name).getReward() == highestReward)
				{
					if(duelScore.get(name).getTimeUsed() < lowestTimeUsed)
					{
						winner = name;
						highestReward = duelScore.get(name).getReward();
						lowestTimeUsed = duelScore.get(name).getTimeUsed();
					}
				}
			}

			account.getSession().sendMessage(NetworkMessage.encodeDuelWinnerMessage(winner, highestReward*2));

			rewardDuelWinner(winner, highestReward*2);
		}
	}

	public void rewardDuelWinner(String nickname, int reward)
	{
		if(reward > 0)
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Duel result");
			alert.setHeaderText(nickname+" won the duel");
			alert.showAndWait();

			rewardPlayer(nickname,reward);
		}
		else
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Duel result");
			alert.setHeaderText("Nobody won the duel");
			alert.showAndWait();
		}
	}

	public void handlePlayerLeft(String nickname)
	{
		playersPoints.get(nickname).setDisable(true);

		for (Node children : getChildren()) 
		{
			if(children instanceof ImageView && ((ImageView)children).equals(playersSession.get(nickname).getImageView()))
			{
				children.setVisible(false);
			}

			if(children instanceof ImageView && playersSession.get(nickname).getCamemberts().contains(((ImageView)children)))
			{
				children.setVisible(false);
			}		

			if(children instanceof HBox && ((HBox)children).equals(playersSession.get(nickname).getNameBox()))
			{
				children.setVisible(false);
			}
		}

		playerReady--;

		//Not enough player to continue
		if(playerReady == 1)
		{
			backToMainMenu(true,"Not enough player to continue");
		}

		//if it is the turn of the dc player, go to next turn
		if(currentPlayerNickname.equals(nickname) && account.getSession().isHost())
		{
			updateCurrentPlayer(false);
			account.getSession().sendMessage(NetworkMessage.encodePlayerTurnMessage(currentPlayerNickname));
			nextTurn();
		}
	}

	public void showAllowedTiles(int moveValue)
	{
		List<Tile> tiles = getAllowedTiles(moveValue);

		getDice().setValue(moveValue-1);

		getDice().playAnimation(tiles);
	}

	/**
	 * Proceed to the next turn
	 * @param moveAgain If the current player can play another turn or not
	 */
	public void nextTurn()
	{
		//update the current player information box
		//updateCurrentPlayer(moveAgain);
		getChildren().forEach(children -> {
			if(children instanceof Tile)
			{
				((Tile) children).disable();
			}
		});

		//Throw the dice, show is animation an then enable the available tiles
		throwDice();

		//We then wait for the current player to click on an available tile (onClick()), and to answer the question
		//And finally we launch the next turn
	}

	public void updateCurrentPlayer(boolean moveAgain)
	{
		//If the player have given the right answer, he can move again
		if(!moveAgain)
		{
			/*
			 * Iterate through the playerMap keyset, and set the currentPlayerNickname to the next one
			 * Make sure to avoid player who aren't in the game anymore
			 */
			do
			{
				String tmpNickname = currentPlayerNickname;
				String lastString = "";
				boolean first = true;
				String firstName = "";

				for (String string : playersSession.keySet()) 
				{
					if(first)
					{
						firstName = string;
						first = false;
					}

					if(lastString.equals(currentPlayerNickname))
					{
						currentPlayerNickname = string;
						break;
					}

					lastString = string;
				}

				//The nickname did not change, so we are at the end of the list
				if(currentPlayerNickname.equals(tmpNickname))
				{
					currentPlayerNickname = firstName;
				}
			}
			while(playersPoints.get(currentPlayerNickname).isDisable());

			currentPlayerTurn.setText(currentPlayerNickname+"'s turn");

		}
	}

	public List<Tile> getAllowedTiles(int moveValue)
	{

		//Get the current tile of the player
		Tile currentTile = playersSession.get(currentPlayerNickname).getCurrentTile();

		List<Tile> availableTiles = new ArrayList<>();

		//Get, based on the movement value and the current tile, the possible movement that the player can do
		if(currentTile instanceof CenterTile)
		{
			for (Tile nextTiles : ((CenterTile)currentTile).getNextTiles())
			{
				Tile tile = nextTiles;
				for(int i = 1; i < moveValue; i++)
				{
					tile = tile.getNextTile();
				}
				availableTiles.add(tile);
			}
		}
		else if(currentTile instanceof NormalTile)
		{
			boolean isTop = ((NormalTile)currentTile).isTop();
			Tile tile = null;
			Tile secondaryTile= null;
			if(isTop)
			{
				tile= currentTile.getNextTile();
				secondaryTile = currentTile.getLastTile();
			}
			else
			{
				tile= currentTile.getNextTile();
			}
			for(int i = 1; i < moveValue; i++)
			{
				if(isTop)
				{
					if(secondaryTile instanceof CheeseTile)
					{
						secondaryTile = ((CheeseTile)secondaryTile).getLeftTile();
					}
					else
					{
						secondaryTile = secondaryTile.getLastTile();
					}

					if(tile instanceof CheeseTile)
					{
						tile = ((CheeseTile)tile).getRightTile();
					}
					else
					{
						tile = tile.getNextTile();
					}
				}
				else
				{
					if(playersSession.get(currentPlayerNickname).haveAllCamembert())
					{
						tile = tile.getLastTile();

						if(tile instanceof CenterTile)
						{
							break;
						}
					}
					else
					{

						if(tile instanceof CheeseTile)
						{
							secondaryTile = ((CheeseTile)tile).getLeftTile();
							for(int j = i+1; j < moveValue; j++)
							{
								secondaryTile = secondaryTile.getLastTile();
							}

							tile = ((CheeseTile)tile).getRightTile();
						}
						else
						{
							tile = tile.getNextTile();
						}
					}
				}
			}
			availableTiles.add(tile);
			if(secondaryTile != null)
			{
				availableTiles.add(secondaryTile);
			}
		}
		else 
		{
			Tile tile = null;
			Tile secondaryTile= null;
			if(playersSession.get(currentPlayerNickname).haveAllCamembert())
			{
				tile = ((CheeseTile)currentTile).getLastTile();
				for(int i = 1; i < moveValue; i++)
				{
					tile = tile.getLastTile();

					if(tile instanceof CenterTile)
					{
						break;
					}
				}
			}
			else
			{
				tile = ((CheeseTile)currentTile).getRightTile();
				secondaryTile= ((CheeseTile)currentTile).getLeftTile();
				for(int i = 1; i < moveValue; i++)
				{
					tile = tile.getNextTile();
				}
				for(int i = 1; i < moveValue; i++)
				{
					secondaryTile = secondaryTile.getLastTile();
				}
			}
			availableTiles.add(tile);
			if(secondaryTile != null)
			{
				availableTiles.add(secondaryTile);
			}
		}

		return availableTiles;
	}

	public void throwDice()
	{		
		//Get the movement value from the dice throw
		int moveValue = getDice().throwDice();

		//Send the result to the server, who'll broadcast it to everyone (including us).
		//The message handling thread will then show the possible case accordingly
		account.getSession().sendMessage(NetworkMessage.encodeDiceResultMessage(moveValue));

		if(account.getSession().isHost())
			showAllowedTiles(moveValue);

		//We now wait for the player to click where he want to go
	}

	public void onClick(double x, double y)
	{
		//Block clicks if we are not playing
		if(!currentPlayerNickname.equals(account.getNickname()))
		{
			return;
		}

		Tile tile = getTileAtPos(x,y);

		if(tile != null)
		{			
			//Since all the screen do not have the same size, we cannot just send the x and y, so we transform them in %
			double pctXValue = x/Main.getStageWidth();
			double pctYValue = y/Main.getStageHeight();
			//We can send the new position to everyone
			account.getSession().sendMessage(NetworkMessage.encodePlayerMoveMessage(pctXValue,pctYValue,tile.getRotate()));

			if(account.getSession().isHost())
				moveCurrentPlayer(x,y,tile.getRotate());
		}
	}

	private void moveCurrentPlayer(double x, double y, double rotation)
	{
		GameSession session = playersSession.get(currentPlayerNickname);
		Tile newTile = getTileAtPos(x, y);
		boolean duel = false;
		session.movePlayer(newTile.getLayoutX(), newTile.getLayoutY(), newTile.getRotate());
		session.setCurrentTile(newTile);

		getChildren().forEach(children -> {
			if(children instanceof Tile)
			{
				((Tile) children).disable();
			}
		});

		//If true, the current player win
		if(newTile instanceof CenterTile)
		{
			Main.getStage().setResizable(true);
			EndGame board = new EndGame(lobby);
			Scene scene = new Scene(board,Main.getStageWidth(),Main.getStageHeight());
			Main.getStage().setScene(scene);
			return;
		}

		//Check if we are on the same tile than another player(for the current one)/the current player
		//If so, launch a 'duel'
		/*
		 * Both can answer
		 * When both have answered, the host find the winner : the more precise, and if they are equals the fastest
		 */

		if(account.getNickname().equals(currentPlayerNickname))
		{
			for (String acc : playersSession.keySet()) 
			{
				if(playersSession.get(acc).getCurrentTile().equals(account.getSession().getCurrentTile()) && !account.getNickname().equals(acc))
				{
					duel = true;
				}
			}
		}
		else
		{
			if(session.getCurrentTile().equals(account.getSession().getCurrentTile()))
			{
				duel = true;
			}
		}

		if(!duel)
		{
			if(currentPlayerNickname.equals(account.getNickname()))
			{
				int reward = DisplayQuestion.launchDisplayQuestion(newTile.getCategory(),true);

				account.getSession().sendMessage(NetworkMessage.encodePlayerQuestionRewardMessage(reward));

				if(account.getSession().isHost())
					rewardPlayer(currentPlayerNickname,reward);
			}
			else
			{
				DisplayQuestion.launchDisplayQuestion(newTile.getCategory(),false);
			}
		}
		else
		{			
			int reward = 0;
			int timeUsed = 61;

			//Tell the host that this is a duel	
			account.getSession().sendMessage(NetworkMessage.encodeDuelStartMessage(account.getNickname()));	

			if(account.getSession().isHost())
				duelScore.put(account.getNickname(), null);

			reward = DisplayQuestion.launchDisplayQuestion(newTile.getCategory(),true);

			timeUsed = DisplayQuestion.getTimeUsed();

			account.getSession().sendMessage(NetworkMessage.encodeDuelPlayerScoreMessage(account.getNickname(), reward, timeUsed));		

			if(account.getSession().isHost())
				rewardDuel(account.getNickname(),reward,timeUsed);
		}
	}

	private void rewardPlayer(String nickname,int reward)
	{
		GameSession session = playersSession.get(nickname);
		Category cat = session.getCurrentTile().getCategory();

		if(reward > 0)
		{
			//Add the cheese
			if(session.getCurrentTile() instanceof CheeseTile)
			{
				session.addCamembert(cat);
			}

			session.addCurency(reward);	

			//Update the player label
			playersPoints.get(nickname).getChildren().forEach(children ->
			{
				String cutName = nickname.length() > 7 ?  nickname.substring(0,7)+"... : " : nickname+" : ";

				if(children instanceof Label && !((Label)children).getText().equals(cutName))
				{
					((Label)children).setText(playersSession.get(nickname).getCurrency() + " pts");
				}
			});
			//If the player got a camembert during the last question, show it on the screen
			for (ImageView camembertImage : playersSession.get(nickname).getCamemberts()) 
			{
				if(!getChildren().contains(camembertImage))
				{
					getChildren().add(camembertImage);
				}
			}
		}	

		if(account.getSession().isHost())
		{
			//Get the next player, and launch the next turn
			updateCurrentPlayer(reward > 0);
			account.getSession().sendMessage(NetworkMessage.encodePlayerTurnMessage(currentPlayerNickname));
			nextTurn();
		}
	}

	private Tile getTileAtPos(double x, double y)
	{
		for (Node children : getChildren()) 
		{
			if(children instanceof Tile)
			{
				Tile tileTmp = (Tile)children;
				Bounds bounds = tileTmp.getBoundsInParent();

				//The user clicked on the tile
				if(bounds.contains(new Point2D(x, y)))
				{
					if(!tileTmp.isDisabled())
					{
						return tileTmp;						
					}
				}
			}
		}

		return null;
	}

	public void backToMainMenu(boolean forced, String errorMessage)
	{
		//Reset the lobby, leaving only the current player inside
		lobby.clear(account);
		if(!forced)
		{
			account.getSession().sendMessage(NetworkMessage.encodePlayerDisconnectMessage(account.getNickname(), account.getSession().isHost()));
		}
		account.getSession().closeConnection();
		if(handlingMessageThread != null)
			handlingMessageThread.interrupt();
		//Go to the main menu. Use platform.runlater not because of threads, but because the transition is too quick
		//And may cause issue when the player is still in the lobby
		Platform.runLater(() ->
		{
			mp.pause();
			Main.getStage().setResizable(true);
			Main.getStage().setScene(Main.getMainMenu());
		});
		if(forced)
		{
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Disconnected");
			alert.setHeaderText(errorMessage);
			alert.show();
		}
	}

	/**
	 * Call all the function to build the entire board
	 */
	private void buildBoard()
	{
		BackgroundImage image = new BackgroundImage(new Image(getClass().getResourceAsStream("Background/Images/FondTexture.png"),Main.getStageWidth(),Main.getStageHeight(),false,true),
				BackgroundRepeat.NO_REPEAT,BackgroundRepeat.NO_REPEAT,BackgroundPosition.DEFAULT,BackgroundSize.DEFAULT);
		this.setBackground(new Background(image));

		if(Main.getStageHeight() < (Tile.tileSize*12+Tile.centerTileHeight))
		{
			double mult = (Main.getStageHeight()/16)/Tile.tileSize;

			Tile.setTileMult(mult);
		}

		buildCenter();

		buildLine(1);
		//BottomRight
		buildDiagonal(1,1);
		//BottomLeft
		buildDiagonal(-1,1);
		buildLine(-1);
		//TopLeft
		buildDiagonal(-1,-1);
		//TopRight
		buildDiagonal(1,-1);
		buildBorder();
	}

	/**
	 * Build the center of the board
	 */
	private void buildCenter()
	{
		CenterTile tile = new CenterTile();

		//Center of the board is the screen width/height divided by 2. Since the anchor point of object in javafx
		//Is at the top left, removing half the height/width of those make sure it is centered on that point
		tile.setLayoutX(Main.getStageWidth()/2-Tile.centerTileWidth/2);
		tile.setLayoutY(Main.getStageHeight()/2-Tile.centerTileHeight/2);
		this.getChildren().add(tile);
	}

	/**
	 * Build the border around the board
	 */
	private void buildBorder()
	{
		//the numeric values have been found by trials.
		double initialAngle = 0.155;
		double centreX = Main.getStageWidth()/2-Tile.tileSize/2;
		double centreY = Main.getStageHeight()/2-Tile.topTileWidth/2;
		double radius = Tile.centerTileWidth/2+Tile.tileSize*5.43;
		//The first loop is for each segments of the border circle
		for(int i = 0; i<6;i++)
		{
			Tile lastTile = null;

			//The second is for each tiles in a segment
			for(int j = 0;j<6;j++)
			{
				//We first get the angle in radians of the position of the tile on the circle
				double anglePos = initialAngle+0.147*j;
				//Then use it to calculate the x and y coordinates
				//Using the formula :
				// x = centre.x+radius+cos(angle)
				// y = centre.y+radius+sin(angle)
				double x = centreX+radius*Math.cos(anglePos);
				double y =  centreY+radius*Math.sin(anglePos);		

				//We then create the tile, and set is x and y position
				Tile tile = new NormalTile(Category.values()[j],true,false);
				tile.setLayoutX(x);
				tile.setLayoutY(y);
				//All left to do is rotate the tile to make sure to face the centre
				//This is done by using the formula
				//Atan2(centre.y-y,centre.x-x)*(180/PI)
				double angleRot = Math.atan2(centreY-y, centreX-x)*(180/Math.PI);
				tile.setRotate(angleRot);

				//Finally we add the tile to the gameBoard, and to the tiles list
				this.getChildren().add(tile);

				//If the tile is first/last in the segment
				//We set the last/next tile to the corresponding cheese tile
				if(j == 0)
				{
					CheeseTile cheeseTile = null;

					for (Node children : getChildren()) 
					{
						if(children instanceof CheeseTile && ((CheeseTile)children).getCategory() == Category.values()[i])
						{
							cheeseTile = (CheeseTile) children;
						}
					}

					tile.setLastTile(cheeseTile);
					cheeseTile.setRightTile(tile);
				}
				else if( j == 5)
				{
					int next = i == 5 ? 0 : i+1;
					CheeseTile cheeseTile = null;

					for (Node children : getChildren()) 
					{
						if(children instanceof CheeseTile && ((CheeseTile)children).getCategory() == Category.values()[next])
						{
							cheeseTile = (CheeseTile) children;
						}
					}
					tile.setNextTile(cheeseTile);
					cheeseTile.setLeftTile(tile);
				}

				//If the last tile isn't null, we link it with the current tile
				if(lastTile != null)
				{
					tile.setLastTile(lastTile);
					lastTile.setNextTile(tile);
				}

				//then we set the last tile and go on
				lastTile = tile;
			}

			//After each segment, we increment the initial angle to jump to the start of the next one
			initialAngle+=1.047;
		}
	}

	/**
	 * Build the diagonal corresponding to the given arguments
	 * @param xSign The sign on the x axis (-1 = left && 1 = right)
	 * @param ySign The sign on the y axis (-1 = top && 1 = down)
	 */
	private void buildDiagonal(int xSign,int ySign)
	{
		//We first calculate two multipliers used to get the base tile position.
		//as well as the angle used to rotate the tiles.
		//Those are chosen based on the x and y sign
		double xMult = xSign == 1 ? 0.25 : 1.25;
		double yMult = ySign == 1 ? 0.75 : 1.75;
		double angle = xSign == ySign ? 60 : 30;
		//We then use those value to get the position of the first tile of the diagonal
		double baseX = Main.getStageWidth()/2+xSign*Tile.tileSize*xMult;
		double baseY = Main.getStageHeight()/2+ySign*Tile.tileSize*yMult;
		Tile lastTile = null;

		//Then, we loop to create all the 6 tiles in the diagonal
		for(int i = 0; i<6;i++)
		{
			//For each tiles, we first calculate the modifiers :
			//They are the x and y values to add to the base position to get the final position of our tile
			double modX = xSign*(Tile.tileSize*0.495)*i;
			double modY = ySign*(Tile.tileSize*0.865)*i;

			Tile tile= null;

			//We then create the tile, either a normal tile or a cheese tile if it is the end of the diagonal
			if(i==5)
			{
				int categoryIndex = 0;

				if(ySign == 1)
				{
					categoryIndex = xSign == 1 ? 1 : 2;
				}
				else
				{
					categoryIndex = xSign == -1 ? 4 : 5;
				}

				tile = new CheeseTile(Category.values()[categoryIndex]);
			}
			else
			{				
				tile = new NormalTile(Category.values()[i],false,false);
			}

			//Finally, the tile's position and rotation are set, and we add it to the board and list
			tile.setLayoutX(baseX+modX);
			tile.setLayoutY(baseY+modY);
			tile.setRotate(angle);
			this.getChildren().add(tile);

			//At last, we link the tile with either the last one, or the centre if it is the first in the diagonal
			//(If so, then lastTile is null)
			if(lastTile != null)
			{
				tile.setLastTile(lastTile);
				lastTile.setNextTile(tile);
			}
			else
			{
				CenterTile centerTile = null;

				for (Node children : getChildren()) 
				{
					if(children instanceof CenterTile)
					{
						centerTile = (CenterTile) children;
					}
				}
				centerTile.addNextTile(tile);
				tile.setLastTile(centerTile);
			}

			lastTile = tile;
		}
	}


	/**
	 * Build a line of the board
	 * @param sign The sign on the x axis (-1 = left && 1 = right)
	 */
	private void buildLine(int sign)
	{
		Tile lastTile = null;
		//First, we have to set some variables
		//The y position is the same for every tile
		final double yPos = Main.getStageHeight()/2-Tile.tileSize/2;
		//Since the anchor point on object in javafx is on the top left,
		//the left line have a gap already at the start
		final int gap = sign == -1 ? 1 : 0;

		//Then, we loop to create all the 6 tiles in the diagonal
		for(int i = 0+gap; i<6+gap;i++)
		{
			Tile tile = null;
			//We calculate the x position of the tile
			//We are multiplying the tile size by 0.99 to make sure that there is no gap between them
			double xPos = Main.getStageWidth()/2+(Tile.centerTileWidth/2+Tile.tileSize*i*0.99)*sign;

			//We then create the tile, either a normal tile or a cheese tile if it is the end of the line
			if(i==5+gap)
			{
				int categoryIndex = gap == 0 ? 0 : 3;

				tile = new CheeseTile(Category.values()[categoryIndex]);
			}
			else
			{				
				tile = new NormalTile(Category.values()[i-gap],false,false);
			}

			//Finally, the tile's position and rotation are set, and we add it to the board and list
			tile.setLayoutY(yPos);
			tile.setLayoutX(xPos);
			this.getChildren().add(tile);

			//At last, we link the tile with either the last one, or the centre if it is the first in the diagonal
			//(If so, then lastTile is null)
			if(lastTile != null)
			{
				tile.setLastTile(lastTile);
				lastTile.setNextTile(tile);
			}
			else
			{
				CenterTile centerTile = null;

				for (Node children : getChildren()) 
				{
					if(children instanceof CenterTile)
					{
						centerTile = (CenterTile) children;
					}
				}

				centerTile.addNextTile(tile);
				tile.setLastTile(centerTile);
			}

			lastTile = tile;
		}
	}

	public Dice getDice()
	{
		if(dice == null)
		{
			dice = new Dice();
		}

		return dice;
	}

	public String getCurrentPlayerNickname()
	{
		return currentPlayerNickname;
	}

	@Override
	public void handle(KeyEvent event) 
	{
		if(event.getEventType().equals(KeyEvent.KEY_PRESSED))
		{
			if(event.getCode().equals(KeyCode.ESCAPE))
			{
				Dialog<Boolean> menu = new Dialog<>();

				GridPane pane = new GridPane();

				Button resume = new Button("Resume");
				resume.setOnAction(e ->
				{
					menu.setResult(true);
					menu.close();
				});
				resume.setPrefWidth(200);

				Button quit = new Button("Quit to main menu");
				quit.setOnAction(e ->
				{
					mp.pause();
					backToMainMenu(false,"");
					menu.setResult(true);
					menu.close();
				});
				quit.setPrefWidth(200);

				pane.add(resume, 0, 0);
				pane.add(quit, 0, 1);

				menu.getDialogPane().setContent(pane);

				menu.initStyle(StageStyle.UNDECORATED);

				menu.showAndWait();
			}
		}
	}

	@Override
	public void resize() {
		// TODO Auto-generated method stub
		
	}
}
