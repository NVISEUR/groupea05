package vue;


import java.util.Optional;

import application.Main;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.control.ToggleGroup;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import modele.Account;
import modele.Lobby;
import modele.Resizable;

public class LocalLobby extends BorderPane implements Resizable
{

	private Button backToMainMenu, playGame, addPlayerButton, removePlayerButton;
	private Lobby lobby;
	private VBox difficulties,boxPlayer1, boxPlayer2, boxPlayer3, boxPlayer4, mainBox;
	private Label labelPlayer1, labelPlayer2, labelPlayer3, labelPlayer4;
	private HBox boxNbPlayer, boxLaunchGame;
	private Image backgroundImage;
	private Label difficultyInfo;
	private RadioButton hard,medium,easy;
	private ImageView p1Image,p2Image,p3Image,p4Image;
	private CheckBox overrideLevenshteinBox;
	private TextField overrideLevenshteinField;
	private MediaPlayer mp;
	private String musicPath = "Soundtrack/lobbytheme.mp3";

	public LocalLobby(Lobby lob) 
	{
		backgroundImage = new Image(getClass().getResourceAsStream("Background/Images/FondTexture.png"));

		getStylesheets().addAll(getClass().getResource("transback.css").toExternalForm());
		lobby = lob;
		boxNbPlayer = new HBox();
		boxPlayer1 = new VBox();
		boxPlayer2 = new VBox();
		boxPlayer3 = new VBox();
		boxPlayer4 = new VBox();
		HBox boxAddPlayer = new HBox();
		boxLaunchGame = new HBox();
		mainBox = new VBox();
		HBox centerBox = new HBox();
		labelPlayer2 = new Label("Player 2");
		labelPlayer3 = new Label("Player 3");
		labelPlayer4 = new Label("Player 4");
		mainBox.setAlignment(Pos.TOP_LEFT);

		boxNbPlayer.setAlignment(Pos.TOP_RIGHT);

		//Add to the boxPlayers each informations of the player (nickname) and a ready button
		labelPlayer1 = getPlayerLabel(lob.getPlayer(0));
		boxPlayer1.getChildren().add(labelPlayer1);
		boxPlayer1.setStyle("-fx-background-color: #f7f1dc");
		p1Image = lob.getPlayer(0).getSession().getImageView();
		boxPlayer1.getChildren().add(p1Image);
		boxPlayer1.setAlignment(Pos.TOP_CENTER);
		boxPlayer1.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

		//Add to the boxPlayers each informations of the player 2 (nickname) and a ready button
		boxPlayer2.setDisable(true);
		boxPlayer2.getChildren().add(labelPlayer2);
		boxPlayer2.setAlignment(Pos.TOP_CENTER);
		boxPlayer2.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

		//Add to the boxPlayers each informations of the player 3 (nickname) and a ready button
		boxPlayer3.setDisable(true);
		boxPlayer3.getChildren().add(labelPlayer3);
		boxPlayer3.setAlignment(Pos.TOP_CENTER);
		boxPlayer3.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

		//Add to the boxPlayers each informations of the player 4 (nickname) and a ready button
		boxPlayer4.setDisable(true);
		boxPlayer4.getChildren().add(labelPlayer4);
		boxPlayer4.setAlignment(Pos.TOP_CENTER);
		boxPlayer4.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderWidths.DEFAULT)));

		//Add to the boxAddPlayer a button to add a player to the lobby
		boxAddPlayer.getChildren().add(getAddPlayerButton());
		boxAddPlayer.getChildren().add(getRemovePlayerButton());
		boxAddPlayer.setAlignment(Pos.CENTER);

		//Add to the boxLaunchGame buttons to launch the game, to add a player and to return to the main menu
		boxLaunchGame.getChildren().add(getPlayGame());
		boxLaunchGame.getChildren().add(getBackToMainMenu());
		boxLaunchGame.setAlignment(Pos.BOTTOM_CENTER);

		//add each boxes + difficulties
		centerBox.getChildren().add(boxPlayer1);
		centerBox.getChildren().add(boxPlayer2);
		centerBox.getChildren().add(boxPlayer3);
		centerBox.getChildren().add(boxPlayer4);
		mainBox.getChildren().add(boxNbPlayer);
		mainBox.getChildren().add(centerBox);
		mainBox.getChildren().add(boxAddPlayer);
		mainBox.getChildren().add(getDifficulties());
		mainBox.getChildren().add(boxLaunchGame);
		setTop(mainBox);

		String path = getClass().getResource(musicPath).toExternalForm();
		Media media = new Media(path);
		mp = new MediaPlayer(media);
		mp.setCycleCount(MediaPlayer.INDEFINITE);
		mp.play();

		resize();
	}

	public CheckBox getOverrideLevenshteinBox()
	{
		if(overrideLevenshteinBox == null)
		{
			overrideLevenshteinBox = new CheckBox("Override Levenshtein distance setting in percent");
			overrideLevenshteinBox.getStyleClass().add("transback");
			overrideLevenshteinBox.setTextFill(Color.WHITE);

			overrideLevenshteinBox.setOnAction(e ->
			{
				if(overrideLevenshteinBox.isSelected())
				{
					getOverrideLevenshteinField().setDisable(false);
				}
				else
				{
					getOverrideLevenshteinField().setDisable(true);
				}
			});
		}

		return overrideLevenshteinBox;
	}

	public TextField getOverrideLevenshteinField()
	{
		if(overrideLevenshteinField == null)
		{
			overrideLevenshteinField = new TextField();
			overrideLevenshteinField.setDisable(true);
		}

		return overrideLevenshteinField;
	}

	public Button getAddPlayerButton()
	{
		if(addPlayerButton == null)
		{
			addPlayerButton = new Button("Add Player");
			addPlayerButton.setOnAction(e -> 
			{
				if(boxPlayer4.isDisable())
				{
					Dialog<ButtonType> choiceConnection = new Dialog<>();
					choiceConnection.setTitle("Connection");

					ButtonType inscriptionButton = new ButtonType("Create an account");
					ButtonType connectionButton = new ButtonType("Connect to an account");
					ButtonType inviteButton = new ButtonType("Connect as a Guest");
					choiceConnection.getDialogPane().getButtonTypes().addAll(inscriptionButton, connectionButton, inviteButton);

					//the red cross close the dialog
					choiceConnection.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
					Node closeButton = choiceConnection.getDialogPane().lookupButton(ButtonType.CLOSE);
					closeButton.managedProperty().bind(closeButton.visibleProperty());
					closeButton.setVisible(false);

					GridPane grid = new GridPane();

					grid.setHgap(10);
					grid.setVgap(10);
					grid.setPadding(new Insets(20, 150, 10, 10));

					Optional<ButtonType> res = choiceConnection.showAndWait();
					Optional<Account> result = null;
					String choice = res.get().getText();
					if(choice == "Connect to an account")
					{
						result = DisplayLoginOptions.launchDisplayConnection();
					}
					else if (choice == "Create an account")
					{
						result = DisplayLoginOptions.launchDisplayInscription();
					}
					else if (choice == "Connect as a Guest")
					{
						result = DisplayLoginOptions.launchDisplayGuest();
					}
					else {
						return ;
					}
					if(result.isPresent())
					{
						Account account = result.get();
						//Modify boxPlayer depends on which one is disable and add the player to the box
						if(!account.getNickname().equals(labelPlayer2.getText()) && !account.getNickname().equals(labelPlayer3.getText()))
						{
							if(boxPlayer2.isDisable())
							{
								lobby.addPlayer(1, account);
								labelPlayer2.setText(account.getNickname());
								p2Image = account.getSession().getImageView();
								boxPlayer2.getChildren().add(p2Image);
								boxPlayer2.setDisable(false);
								boxPlayer2.setStyle("-fx-background-color: #f7f1dc");
							}
							else if(boxPlayer3.isDisable())
							{
								lobby.addPlayer(2, account);
								labelPlayer3.setText(account.getNickname());
								p3Image = account.getSession().getImageView();
								boxPlayer3.getChildren().add(p3Image);
								boxPlayer3.setDisable(false);
								boxPlayer3.setStyle("-fx-background-color: #f7f1dc");
							}
							else
							{
								lobby.addPlayer(3, account);
								labelPlayer4.setText(account.getNickname());
								p4Image = account.getSession().getImageView();
								boxPlayer4.getChildren().add(p4Image);
								boxPlayer4.setDisable(false);
								boxPlayer4.setStyle("-fx-background-color: #f7f1dc");
							}
						}
					}
					else
					{
						return;
					}
				}
			});
		}		

		return addPlayerButton; 
	}

	public Button getRemovePlayerButton()
	{
		if(removePlayerButton == null)
		{
			removePlayerButton = new Button("Remove Player");
			removePlayerButton.setOnAction(e ->
			{
				if (!boxPlayer4.isDisable())
				{
					boxPlayer4.setDisable(true);
					labelPlayer4.setText("Player 4");
					boxPlayer4.getChildren().remove(lobby.getPlayer(3).getSession().getImageView());
					boxPlayer4.setStyle(null);
					lobby.removePlayer(3);

				}
				else if(!boxPlayer3.isDisable())
				{
					boxPlayer3.setDisable(true);
					labelPlayer3.setText("Player 3");
					boxPlayer3.getChildren().remove(lobby.getPlayer(2).getSession().getImageView());
					boxPlayer3.setStyle(null);
					lobby.removePlayer(2);
				}
				else if(!boxPlayer2.isDisable())
				{
					boxPlayer2.setDisable(true);
					labelPlayer2.setText("Player 2");
					boxPlayer2.getChildren().remove(lobby.getPlayer(1).getSession().getImageView());
					boxPlayer2.setStyle(null);
					lobby.removePlayer(1);
				}
			});
		}		

		return removePlayerButton; 
	}

	public VBox getDifficulties()
	{
		if(difficulties == null)
		{
			ToggleGroup difficult = new ToggleGroup();

			//Add "easy", "medium", and "hard" mode and choose between them
			easy = new RadioButton("Easy");
			easy.getStyleClass().add("transback");
			easy.setToggleGroup(difficult);
			easy.setSelected(true);
			easy.setTextFill(Color.WHITE);
			Tooltip easyTooltip = new Tooltip();
			easyTooltip.setText("60 seconds to answer the questions\nHint enabled\nYour answer need to be at least 80% correct to be validated");
			easy.setTooltip(easyTooltip);

			medium = new RadioButton("Medium");
			medium.getStyleClass().add("transback");
			medium.setToggleGroup(difficult);
			medium.setTextFill(Color.WHITE);
			Tooltip mediumTooltip = new Tooltip();
			mediumTooltip.setText("45 seconds to answer the questions\nHint enabled\nYour answer need to be at least 90% correct to be validated");
			medium.setTooltip(mediumTooltip);

			hard = new RadioButton("Hard");
			hard.getStyleClass().add("transback");
			hard.setToggleGroup(difficult);
			hard.setTextFill(Color.WHITE);
			Tooltip hardTooltip = new Tooltip();
			hardTooltip.setText("30 seconds to answer the questions\nHint disabled\nYour answer need to be entirely correct to be validated");
			hard.setTooltip(hardTooltip);

			difficult.selectedToggleProperty().addListener(    
					(ObservableValue<? extends Toggle> ov, Toggle old_toggle, 
							Toggle new_toggle) -> 
					{
						//Choose a difficulty
						if (difficult.getSelectedToggle() != null) 
						{
							if(((RadioButton)difficult.getSelectedToggle()).equals(easy))
							{
								DisplayQuestion.setHintEnabled(true);
								DisplayQuestion.setTimeToAnswer(60);
								DisplayQuestion.setRewards(1, 3);
								DisplayQuestion.setAcceptableErrorPct(0.8);
							}
							else if(((RadioButton)difficult.getSelectedToggle()).equals(medium))
							{
								DisplayQuestion.setHintEnabled(true);
								DisplayQuestion.setTimeToAnswer(45);
								DisplayQuestion.setRewards(2, 5);
								DisplayQuestion.setAcceptableErrorPct(0.9);
							}
							else 
							{
								DisplayQuestion.setHintEnabled(false);
								DisplayQuestion.setTimeToAnswer(30);
								DisplayQuestion.setRewards(3, 7);
								DisplayQuestion.setAcceptableErrorPct(1);
							}
						}
					});

			difficulties = new VBox();
			HBox box = new HBox();
			box.getChildren().add(getOverrideLevenshteinBox());
			box.getChildren().add(getOverrideLevenshteinField());
			box.setAlignment(Pos.CENTER);
			box.setSpacing(25);
			HBox boxx = new HBox();
			boxx.getChildren().add(easy);
			boxx.getChildren().add(medium);
			boxx.getChildren().add(hard);
			boxx.setAlignment(Pos.CENTER);
			boxx.setSpacing(25);

			difficultyInfo = new Label("Hover the wanted difficulty to show the tooltip and get more information.");
			difficultyInfo.setFont(new Font(Main.getStageWidth()/50));
			difficultyInfo.getStyleClass().add("transback");
			difficultyInfo.setTextFill(Color.WHITE);

			difficulties.getChildren().add(difficultyInfo);
			difficulties.getChildren().add(box);
			difficulties.getChildren().add(boxx);
			difficulties.setAlignment(Pos.CENTER);
		}

		return difficulties;
	}

	public Label getPlayerLabel(Account acc) 
	{
		Label playerNickname = new Label();
		playerNickname.setWrapText(true);
		playerNickname.setTextAlignment(TextAlignment.CENTER);

		playerNickname.setText(acc.getNickname());
		//Return the player's nickname
		return playerNickname; 
	}

	public Button getPlayGame() {
		if(playGame == null)
		{
			playGame = new Button("Launch Game");
			playGame.setStyle("-fx-background-color: 	#FFD700;");

			playGame.setOnAction(e ->
			{
				lobby.resetSessions();

				if(getOverrideLevenshteinBox().isSelected())
				{
					DisplayQuestion.setAcceptableErrorPct(Double.parseDouble(getOverrideLevenshteinField().getText()));
				}
				mp.pause();	
				LocalGameBoard board = new LocalGameBoard(lobby);
				Scene scene = new Scene(board,Main.getStageWidth(),Main.getStageHeight());
				scene.setOnKeyPressed(board);
				Main.getStage().setScene(scene);
			});
		}
		return playGame; 
	}

	public Button getBackToMainMenu() {
		if(backToMainMenu == null)
		{
			backToMainMenu = new Button("Back");
			backToMainMenu.setStyle("-fx-background-color: 	#DDAAFF;");

			backToMainMenu.setOnAction(e -> {
				Main.getStage().setScene(Main.getMainMenu());
				mp.pause();		
			});
		}
		return backToMainMenu; 
	}

	@Override
	public void resize() 
	{
		final double normalButtonsWidth = Main.getStageWidth()/7;
		final double normalButtonsHeight = Main.getStageHeight()/18;

		getBackToMainMenu().setPrefHeight(normalButtonsHeight);
		getBackToMainMenu().setPrefWidth(normalButtonsWidth);

		getPlayGame().setPrefHeight(normalButtonsHeight);
		getPlayGame().setPrefWidth(normalButtonsWidth);	

		getRemovePlayerButton().setPrefHeight(normalButtonsHeight);
		getRemovePlayerButton().setPrefWidth(normalButtonsWidth);	

		getAddPlayerButton().setPrefHeight(normalButtonsHeight);
		getAddPlayerButton().setPrefWidth(normalButtonsWidth);			

		double fontSize = Main.getStageWidth()/125+Main.getStageHeight()/75;

		labelPlayer1.setFont(new Font(fontSize));
		labelPlayer2.setFont(new Font(fontSize));
		labelPlayer3.setFont(new Font(fontSize));
		labelPlayer4.setFont(new Font(fontSize));	
		difficultyInfo.setFont(new Font(fontSize));
		easy.setFont(new Font(fontSize));
		medium.setFont(new Font(fontSize));
		hard.setFont(new Font(fontSize));
		getOverrideLevenshteinBox().setFont(new Font(fontSize));

		VBox.setMargin(boxPlayer1,new Insets(15,Main.getStageWidth()/3,15,Main.getStageWidth()/3));
		VBox.setMargin(boxPlayer2,new Insets(15,Main.getStageWidth()/3,15,Main.getStageWidth()/3));
		VBox.setMargin(boxPlayer3,new Insets(15,Main.getStageWidth()/3,15,Main.getStageWidth()/3));
		VBox.setMargin(boxPlayer4,new Insets(15,Main.getStageWidth()/3,15,Main.getStageWidth()/3));


		p1Image.setFitHeight(Main.getStageHeight()/20+Main.getStageWidth()/40);
		p1Image.setFitWidth(Main.getStageHeight()/20+Main.getStageWidth()/40);

		if(p2Image != null)
		{
			p2Image.setFitHeight(Main.getStageHeight()/4);
			p2Image.setFitWidth(Main.getStageHeight()/4);
		}

		if(p3Image != null)
		{			
			p3Image.setFitHeight(Main.getStageHeight()/4);
			p3Image.setFitWidth(Main.getStageHeight()/4);
		}

		if(p4Image != null)
		{			
			p4Image.setFitHeight(Main.getStageHeight()/4);
			p4Image.setFitWidth(Main.getStageHeight()/4);	
		}	

		boxPlayer1.setPrefHeight(Main.getStageHeight()/2);
		boxPlayer1.setPrefWidth(Main.getStageWidth()/4.1);

		boxPlayer2.setPrefHeight(Main.getStageHeight()/2);
		boxPlayer2.setPrefWidth(Main.getStageWidth()/4.1);

		boxPlayer3.setPrefHeight(Main.getStageHeight()/2);
		boxPlayer3.setPrefWidth(Main.getStageWidth()/4.1);

		boxPlayer4.setPrefHeight(Main.getStageHeight()/2);
		boxPlayer4.setPrefWidth(Main.getStageWidth()/4.1);

		boxNbPlayer.setSpacing(Main.getStageWidth()/20);

		boxLaunchGame.setSpacing(Main.getStageWidth()/20);

		mainBox.setSpacing(Main.getStageHeight()/30);

		BackgroundSize size = new BackgroundSize(Main.getStageWidth(), Main.getStageHeight(), false, false, false, false);
		BackgroundImage image = new BackgroundImage(backgroundImage, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, size);
		this.setBackground(new Background(image));
	}
}

