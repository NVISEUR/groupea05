package modele;

public interface Resizable 
{
	public abstract void resize();
}