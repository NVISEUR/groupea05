package modele;

import javafx.scene.paint.Color;

public enum Category 
{
	NETWORKS("NETWORKS"),
	OPERATING_SYSTEMS("OPERATING_SYSTEMS"),
	CYBERSECURITY("CYBERSECURITY"),
	PROGRAMMING_LANGUAGES("PROGRAMMING_LANGUAGES"),
	INTERNET("INTERNET"),
	SOCIAL_NETWORKS("SOCIAL_NETWORKS");

	private String textValue;

	Category(String text) {
		this.textValue = text;
	}

	/**
	 * Returns the category name in String format
	 * @return
	 */
	public String getText() {
		return this.textValue;
	}

	
	/**
	 * Returns randomized Categories order list
	 * @return
	 */
	public static Category random()
	{
		int rand = (int)(Math.random()*6);
		//Return a random category
		return Category.values()[rand];
	}

	/**
	 * Returns the Category Color
	 * @param cat
	 * @return
	 */
	public static Color getCategoryColor(Category cat) 
	{		
		switch (cat) 
		{
		//Return the color of the given category.
		case CYBERSECURITY:
			return Color.PURPLE;
		case INTERNET:
			return Color.BLUE;
		case NETWORKS:
			return Color.ORANGE.darker();
		case OPERATING_SYSTEMS:
			return Color.GREEN;
		case PROGRAMMING_LANGUAGES:
			return Color.RED;
		case SOCIAL_NETWORKS:
			return Color.YELLOW.darker();
		default:
			return null;
		}
	}

	
	/**
	 * Returns the category in parameter from String
	 * @param text
	 * @return
	 */
	public static Category fromString(String text) {
		for (Category c : Category.values()) {
			if (c.textValue.equalsIgnoreCase(text)) {
				return c;
			}
		}
		throw new IllegalArgumentException("No constant with text " + text + " found");
	}
}
