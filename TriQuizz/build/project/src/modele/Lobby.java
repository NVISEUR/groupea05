package modele;

public class Lobby {

	private Account[] players;

	public Lobby(Account host)
	{
		players = new Account[4];
		players[0] = host;
	}
	/**
	 * Clear lobby from players except the first player
	 * @param current the first player
	 */
	public void clear(Account current)
	{
		players = new Account[4];
		players[0] = current;
	}
	
	/**
	 * Add a player to the Lobby
	 * @param index the index of the order in players list
	 * @param p the account of the added player
	 */
	public void addPlayer(int index, Account p)
	{
		players[index] = p;
	}
	
	/**
	 * Sets the players list
	 * @param accounts
	 */
	public void setListPlayers(Account[] accounts) 
	{
		this.players = accounts;
	}
	
	/**
	 * Remove a player from the lobby
	 * @param i
	 */
	public void removePlayer(int i)
	{
		players[i] = null;
		
		for(int j = i+1; j < players.length; j++)
		{
			if(players[j] != null)
			{
				players[j-1] = players[j];
				players[j] = null;
			}
		}
	}
	
	/**
	 * Returns player by given index
	 * @param i
	 * @return
	 */
	public Account getPlayer(int i)
	{
		return players[i];
	}
	
	/**
	 * return nomber of players in the lobby
	 * @return
	 */
	public int length()
	{
		int i = 0;
		
		while(players[i] != null)
		{
			i++;
		}
		
		return i;
	}
	
	/**
	 * Return true if the lobby contains the given index player
	 * @param acc
	 * @return
	 */
	public boolean contains(Account acc)
	{
		for (Account account : players) 
		{
			if(acc.equals(account))
			{
				return true;
			}
		}
		
		return false;
	}
	
	/**
	 * Returns the player's index
	 * @param acc
	 * @return
	 */
	public int indexOf(Account acc)
	{
		for (int i = 0; i < players.length; i++) 
		{
			if(acc.equals(players[i]))
			{
				return i;
			}
		}
		
		return -1;
	}
	
	/**
	 * reset the Sessions
	 */
	public void resetSessions()
	{
		for (int i = 0; i < players.length; i++) 
		{
			if(players[i] != null)
			{
				//Reset each player's session
				players[i].getSession().reset();
			}
		}
	}
	
	/**
	 * Return a players list
	 * @return
	 */
	public Account[] getPlayers()
	{
		return players;
	}
}
