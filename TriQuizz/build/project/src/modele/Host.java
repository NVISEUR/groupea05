package modele;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;

import javafx.scene.paint.Color;


public class Host implements NetworkSession
{	
	private Map<Socket, GameSession> clients;
	//We are using a linkedList to have a FIFO object (first in first out)
	private LinkedList<String> messages;
	private ServerSocket serverSocket;
	private Thread connectionThread;
	private Thread messagingThread;
	private Thread pingThread;

	public Host() 
	{
		clients  = new HashMap<Socket, GameSession>();
		messages = new LinkedList<String>();
	}

	
	/**
	 * Returns messages 
	 */
	public LinkedList<String> getMessages()
	{		
		return messages;
	}
	
	/**
	 * Send messages to the clients instances
	 */
	public void sendMessage(String msg)
	{
		for (Socket socket : new HashSet<Socket>(clients.keySet())) 
		{
			DataOutputStream out;
			try 
			{
				out = new DataOutputStream(socket.getOutputStream());
				//Send a message
				out.writeUTF(msg);
			} 
			catch (IOException e) 
			{
				// We lost connection, tell it to the others and remove it from the map
				String message = NetworkMessage.encodePlayerDisconnectMessage(clients.get(socket).getNickname(), false);
				clients.remove(socket);
				sendMessage(message);
				//Also "send" it to ourself, so the lobby will catch the message and handle it
				messages.add(message);
			}
		}
	}

	
	/**
	 * Initialize Connection for online session as host
	 */
	public void initConnection(String hostIp) throws IOException
	{
		serverSocket = new ServerSocket(CONNECTION_PORT);
		//Since the thread only wait for connections, there is no need to set a timeout
		serverSocket.setSoTimeout(0);

		connectionThread = new Thread(() -> {
			while (!connectionThread.isInterrupted()) {
				try 
				{
					try 
					{
						try
						{
							Socket socket = serverSocket.accept();

							if(!clients.containsKey(socket))
							{
								//Intentionally set a timeout on the clients socket, so we do not wait too long before listening to the next one
								socket.setSoTimeout(CLIENT_SOCKET_TIMEOUT_IN_MS);
								socket.setTcpNoDelay(true);
								clients.put(socket, null);
							}
						}
						catch (SocketException ex) 
						{
							//ex.printStackTrace();
							//May happen when closing the server, do not cause any problem
						}
					} 
					catch (IOException ex) 
					{
						ex.printStackTrace();
					}

					Thread.sleep(MESSAGE_READ_INTERVAL_IN_MS);
				} catch (InterruptedException ex) {

					Thread.currentThread().interrupt();

					break;

				}
			}
		});
		//Make sure the thread is stopped when the program exit
		connectionThread.setDaemon(true);
		connectionThread.start();

		messagingThread = new Thread(() -> 
		{
			while (!messagingThread.isInterrupted()) 
			{
				try 
				{
					for (Socket sock : new HashSet<Socket>(clients.keySet())) 
					{
						try 
						{
							if(!sock.isClosed())
							{
								DataInputStream inputStream = new DataInputStream(sock.getInputStream());
								try
								{
									try
									{
										String message = inputStream.readUTF();

										messages.add(message);

										//Check the message to see if it is the message of connection, if so set the gamesession of the map
										if(message.split(NetworkMessage.SEPARATOR)[0].equals("PlayerConnect"))
										{
											String[] data = NetworkMessage.decodePlayerConnectMessage(message);
											Color color = Color.web(data[2]);
											GameSession session = new GameSession(color, data[1]);
											clients.put(sock, session);
										}

										if(message.split(NetworkMessage.SEPARATOR)[0].equals("PlayerDisconnect"))
										{
											clients.remove(sock);
										}

										//Pings have no reason to be broadcasted, and playerconnect will be in the lobby, to be able to give the order of the players
										if(!message.split(NetworkMessage.SEPARATOR)[0].equals("Ping") && 
												!message.split(NetworkMessage.SEPARATOR)[0].equals("PlayerConnect"))
										{
											sendMessage(message);
										}
									}
									catch (SocketTimeoutException e) 
									{
										//The socket timeout is volontary, to not block the thread and keep reading messages from the others
									}
								}
								catch (EOFException | SocketException ex) 
								{
									//There is no more data sent by the client (probably lost connection)
									//sock.close();
									//Intentionally let him in until the next sending of message to throw an exception
									//clients.remove(sock);
								}
							}
						} 
						catch (IOException ex) 
						{
							ex.printStackTrace();
						}
					}

					Thread.sleep(MESSAGE_READ_INTERVAL_IN_MS);
				} 
				catch (InterruptedException ex) 
				{
					Thread.currentThread().interrupt();

					break;
				}
			}
		});
		//Make sure the thread is stopped when the program exit
		messagingThread.setDaemon(true);
		messagingThread.start();

		pingThread = new Thread(() ->
		{
			while (!pingThread.isInterrupted()) 
			{
				try
				{
					sendMessage(NetworkMessage.encodePingMessage());
					Thread.sleep(PING_INTERVAL_IN_MS);
				}
				catch (InterruptedException e) 
				{
					Thread.currentThread().interrupt();

					break;				
				}
			}
		}
				);

		pingThread.setDaemon(true);
		pingThread.start();
	}

	/**
	 * end online hosting
	 */
	public void close() throws IOException
	{	
		//Close all threads + close the server
		connectionThread.interrupt();
		messagingThread.interrupt();
		pingThread.interrupt();

		serverSocket.close();
		for (Socket socket : clients.keySet()) 
		{
			socket.close();
		}
		clients.clear();
	}

}
