package modele;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import utilitaire.Serialization;

public class Deck {

	private List<Question> questions;

	//Using transient to avoid serialization
	private transient static Deck instance;
	private transient int currentIndex = -1;
	private transient Category currentCategory;
	private transient List<Card> cards;

	private Deck() {
		this.questions = new ArrayList<Question>();
		this.cards = new ArrayList<Card>();
	}

	
	/**
	 * Returns current instance.
	 * @return
	 */
	public static Deck getInstance()
	{
		if(instance == null)
		{
			instance = new Deck();
		}

		return instance;
	}

	
	/**
	 * Synchronize all cards for online game mode
	 * @param cards
	 */
	//Use when playing online to synchronize all the cards
	public void setCards(List<Card> cards)
	{
		this.cards = cards;
	}

	/**
	 * Returns cards list
	 * @return
	 */
	public List<Card> getCards()
	{
		return cards;
	}

	
	/**
	 * Method used to answer to a question and returns the result.
	 * @param answer
	 * @return
	 */
	public int answerQuestion(String answer)
	{
		//Get the result value based on Levensthein distance by using lower case letter only
		int result = levenshteinDistance(answer.toLowerCase(),getCurrentQuestion().getAnswer().toLowerCase());

		return result;
	}
	
	/**
	 * Returns the question selected by it's index.
	 * @param i
	 * @return
	 */
	public Question getQuestion(int i) {
		return questions.get(i);
	}

	
	/**
	 * Returns the current card
	 * @return
	 */
	public Card getCurrentCard()
	{
		return this.cards.get(currentIndex);
	}

	
	/**
	 * Returns current question
	 * @return
	 */
	public Question getCurrentQuestion()
	{
		return this.cards.get(currentIndex).getQuestionByCategory(currentCategory);
	}


	@Override
	public String toString() {
		String tostring ="";
		for(Question question : questions) {
			tostring += question.toString() + "\n";
		}
		return tostring;
	}

	
	/**
	 * Load all the questions from the json into the question list
	 * @param fileName
	 */
	public void loadQuestions(String fileName)
	{
		//Load all the questions from the json file into the questions list
		try {
			questions = ((Deck)Serialization.readFromJson(fileName,this.getClass())).questions;
		} catch (FileNotFoundException e) {
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Error");
			alert.setHeaderText("An error occured while trying to read the questions from the questions data file");
			alert.setContentText("Please make sure that the questions data file is placed near the applications");
			alert.showAndWait();
		}

		//Separate all the questions into cards.
		this.createCardDeck();
	}

	
	/**
	 * Return true if another card is available
	 * @return
	 */
	public boolean haveAnotherCard()
	{
		//Try to get a card to see if another one is available
		try
		{
			cards.get(currentIndex);

			return true;
		}
		catch(IndexOutOfBoundsException e)
		{
			return false;
		}
	}

	
	/**
	 * Returns next question
	 * @param category
	 * @return
	 */
	public Question nextQuestion(Category category)
	{		
		//Return next question of the given category
		currentCategory = category;
		if(currentIndex+1 < cards.size())
			return this.cards.get(++currentIndex).getQuestionByCategory(currentCategory);
		else
		{
			currentIndex = -1;
			return this.cards.get(++currentIndex).getQuestionByCategory(currentCategory);
		}
	}

	
	/**
	 * Reset deck
	 */
	public void resetDeck()
	{
		//Shuffle the cards list
		Collections.shuffle(cards);
		currentIndex = -1;
	}

	/**
	 * Return true if there's still one question of each category
	 * @return
	 */
	public boolean stillHaveOneQuestionOfEachCategory()
	{
		boolean cyber = false,internet = false,networks = false,os = false,pl = false,sn = false;
		//Verify if there is still one question for each category
		for(Question q : questions)
		{
			if(q.getCategory() == Category.CYBERSECURITY)
				cyber = true;
			if(q.getCategory() == Category.INTERNET)
				internet = true;
			if(q.getCategory() == Category.NETWORKS)
				networks = true;
			if(q.getCategory() == Category.OPERATING_SYSTEMS)
				os = true;
			if(q.getCategory() == Category.PROGRAMMING_LANGUAGES)
				pl = true;
			if(q.getCategory() == Category.SOCIAL_NETWORKS)
				sn = true;

			if(cyber && internet && networks && os && pl && sn)
				return true;
		}

		return false;

	}

	/**
	 * Displays the questions
	 */
	public void showQuestions()
	{
		int i = 1;

		for(Question q : questions)
		{
			System.out.println(i++ +" => "+q.toString());
		}
	}

	
	/**
	 * Remove question from the list
	 * @param i
	 */
	public void removeQuestion(int i)
	{
		questions.remove(i);
	}

	
	/**
	 * Returns the questions list
	 * @return
	 */
	public List<Question> getQuestions()
	{
		List<Question> copy = new ArrayList<>();

		for(Question question : questions)
		{
			copy.add(question.clone());
		}

		return copy;
	}

	
	/**
	 * Sets the questions list
	 * @param 
	 */
	public void setQuestions(List<Question> qs)
	{
		questions = new ArrayList<>(qs);

		saveDeck("quizz.json");

		loadQuestions("quizz.json");
	}

	
	/**
	 * Creates a Deck of questions cards
	 */
	private void createCardDeck()
	{
		int questionUsed = 0;
		int maxQuestion = questions.size();
		cards = new ArrayList<>();
		Collections.shuffle(questions);

		while(maxQuestion - questionUsed >= 6 && stillHaveOneQuestionOfEachCategory())
		{
			Card card = new Card();

			for(Question q : questions)
			{
				if(!q.haveBeenUsed() && !card.containQuestionOfCategory(q.getCategory()))
				{
					if(!card.containsQuestion(q)) //If two question have the same statement but not the same category
					{
						card.addQuestion(q);

						q.use();

						questionUsed ++;
					}
				}

				if(card.isFull())
					break;
			}
			cards.add(card);
		}

		Collections.shuffle(this.cards);
	}

	/**
	 * Add a question to the deck
	 * @param author
	 * @param statement
	 * @param answer
	 * @param category
	 */
	public void addQuestion(String author, String statement, String answer, Category category)
	{
		//Add a question to the deck
		this.questions.add(new Question(author,category,answer,statement));
	}

	/**
	 * Update the given question with the given parameters
	 * @param q
	 * @param author
	 * @param statement
	 * @param answer
	 * @param category
	 */
	public void updateQuestion(Question q,  String author, String statement, String answer, Category category)
	{
		int index = questions.indexOf(q);
		if(index != -1)
		{
			Question qu = questions.get(index);

			qu.setAnswer(answer);
			qu.setAuthor(author);
			qu.setCategory(category);
			qu.setStatement(statement);
		}
	}

	
	/**
	 * Save the current Deck into the given file
	 * @param fileName
	 */
	public void saveDeck(String fileName)
	{
		//Serialize the deck into the json file
		Serialization.writeToJson(this, fileName, this.getClass());
	}

	public int levenshteinDistance (String lhs, String rhs) {                          
		int len0 = lhs.length() + 1;                                                     
		int len1 = rhs.length() + 1;                                                     

		// the array of distances                                                       
		int[] cost = new int[len0];                                                     
		int[] newcost = new int[len0];                                                  

		// initial cost of skipping prefix in String s0                                 
		for (int i = 0; i < len0; i++) cost[i] = i;                                     

		// dynamically computing the array of distances                                  

		// transformation cost for each letter in s1                                    
		for (int j = 1; j < len1; j++) {                                                
			// initial cost of skipping prefix in String s1                             
			newcost[0] = j;                                                             

			// transformation cost for each letter in s0                                
			for(int i = 1; i < len0; i++) {                                             
				// matching current letters in both strings                             
				int match = (lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1;             

				// computing cost for each transformation                               
				int cost_replace = cost[i - 1] + match;                                 
				int cost_insert  = cost[i] + 1;                                         
				int cost_delete  = newcost[i - 1] + 1;                                  

				// keep minimum cost                                                    
				newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
			}                                                                           

			// swap cost/newcost arrays                                                 
			int[] swap = cost; cost = newcost; newcost = swap;                          
		}                                                                               

		// the distance is the cost for transforming all letters in both strings        
		return cost[len0 - 1];                                                          
	}

}
