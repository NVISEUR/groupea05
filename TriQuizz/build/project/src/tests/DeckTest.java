package tests;

import static org.junit.jupiter.api.Assertions.*;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import modele.*;

class DeckTest {

	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		Deck.getInstance().loadQuestions("quizz.json");
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
	}

	@BeforeEach
	void setUp() throws Exception {
		Deck.getInstance().nextQuestion(Category.CYBERSECURITY);
	}

	@AfterEach
	void tearDown() throws Exception {
	}

	@Test
	void testAnswerQuestion() 
	{
		Question q = Deck.getInstance().getCurrentQuestion();
		
		String answer = q.getAnswer();
		
		assertTrue(Deck.getInstance().answerQuestion(answer) == 0);
	}

	@Test
	void testHaveAnotherCard() 
	{
		boolean haveAnotherCard = Deck.getInstance().haveAnotherCard();
		
		assertTrue(haveAnotherCard);
	}

	@Test
	void testNextQuestion() 
	{
		//to initialise the current card		
		Question next = Deck.getInstance().getCards().get(Deck.getInstance().getCards().indexOf(Deck.getInstance().getCurrentCard())+1).getQuestionByCategory(Category.CYBERSECURITY);
		Question next2 = Deck.getInstance().nextQuestion(Category.CYBERSECURITY);
		
		assertTrue(next.equals(next2));
	}

	@Test
	void testStillHaveOneQuestionOfEachCategory() 
	{
		Question q1 = new Question("test1", Category.CYBERSECURITY, "test1", "test1");
		Question q2 = new Question("test2", Category.CYBERSECURITY, "test2", "test2");
		Question q3 = new Question("test3", Category.CYBERSECURITY, "test3", "test3");
		Question q4 = new Question("test4", Category.CYBERSECURITY, "test4", "test4");
		Question q5 = new Question("test5", Category.CYBERSECURITY, "test5", "test5");
		Question q6 = new Question("test6", Category.CYBERSECURITY, "test6", "test6");
		
		Deck.getInstance().getQuestions().clear();
		
		Deck.getInstance().getQuestions().add(q1);
		Deck.getInstance().getQuestions().add(q2);
		Deck.getInstance().getQuestions().add(q3);
		Deck.getInstance().getQuestions().add(q4);
		Deck.getInstance().getQuestions().add(q5);
		Deck.getInstance().getQuestions().add(q6);
		
		assertTrue(Deck.getInstance().stillHaveOneQuestionOfEachCategory());
	}

	@Test
	void testRemoveQuestion() {
		Question q = Deck.getInstance().getQuestion(0);
		
		Deck.getInstance().removeQuestion(0);
		
		assertTrue(!Deck.getInstance().getQuestions().contains(q));
	}

	@SuppressWarnings("unchecked")
	@Test
	void testGetQuestions() throws IllegalArgumentException, IllegalAccessException, NoSuchFieldException, SecurityException
	{
		List<Question> questions = Deck.getInstance().getQuestions();
		List<Question> questions2 = null;
		for (Field f : Deck.class.getDeclaredFields()) 
		{
			if(f.getName().equals("questions"))
			{
			    f.setAccessible(true);
			    questions2 = (List<Question>) f.get(Deck.getInstance());
			}
		}
		
		assertTrue(questions.equals(questions2));
	}

	@Test
	void testAddQuestion() {
		Question q = new Question("test", Category.CYBERSECURITY, "test", "test");
		
		Deck.getInstance().addQuestion("test", "test", "test", Category.CYBERSECURITY);
		
		assertTrue(Deck.getInstance().getQuestions().contains(q));
	}

	@Test
	void testUpdateQuestion() {		
		Question q = Deck.getInstance().getQuestion(0);
		
		Deck.getInstance().updateQuestion(q, "test", "test", "test", Category.CYBERSECURITY);
		
		assertTrue(q.getStatement().equals("test"));
	}

	@Test
	void testLevenshteinDistance() 
	{
		String str1 = "test";
		String str2 = "test";
		
		int res = Deck.getInstance().levenshteinDistance(str1, str2);
		
		assertTrue(res == 0);
	}

}
