package vue.Tiles;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import modele.Category;
import modele.GameSession;
import utilitaire.AccountsManager;
import vue.DisplayQuestion;
import vue.LocalGameBoard;

public class NormalTile  extends Tile
{
	private boolean isTop, isBonus;
	
	public NormalTile(Category category, boolean isTop, boolean isBonus) 
	{
		super(category);
		//Ignore all the mouse event
		setMouseTransparent(true);

		this.isBonus = isBonus;
		this.isTop = isTop;
		
		if (isBonus)
		{
			//If the tile is a bonus one, get a special image and actions
			image = new Image(getClass().getResourceAsStream(getImageFromCategory(false)),tileSize,tileSize,true,true);
			this.setImage(image);
		}
		else
		{		
			if(isTop)
			{
				//If the tile is in the contour of the circle, the image is resized to fit in the circle
				image = new Image(getClass().getResourceAsStream(getImageFromCategory(false)),tileSize,topTileWidth,false,true);
				this.setImage(image);
			}
			else 
			{
				image = new Image(getClass().getResourceAsStream(getImageFromCategory(false)),tileSize,tileSize,true,true);
				this.setImage(image);
			}
		}
	}

	public boolean isTop()
	{
		return isTop;
	}
	
	public boolean isBonus()
	{
		return isBonus;
	}
	
}
