package vue.Tiles;

import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BackgroundImage;
import modele.Category;

public class Tile extends ImageView
{
	private Category category;
	protected Image image;
	public static double tileSize = 75,topTileWidth = 67;
	public static double centerTileWidth = 150,centerTileHeight = 175;
	
	private Tile lastTile, nextTile;
	
	public void setLastTile(Tile t)
	{
		this.lastTile = t;
	}
	
	public void setNextTile(Tile t)
	{
		this.nextTile = t;
	}
	
	public void enable()
	{
		setOpacity(1.0);
		setDisable(false);
	}
	
	public void disable()
	{
		setOpacity(0.3);
		setDisable(true);
	}
	
	public Tile getLastTile() {
		return lastTile;
	}

	public Tile getNextTile() {
		return nextTile;
	}

	public static void setTileMult(double d)
	{
		// Change the dimensions of the tile
		if(d > 0)
		{
			tileSize*=d;
			centerTileWidth*=d;
			centerTileHeight*=d;
			topTileWidth*=d;
		}
	}
	
	public Tile(Category cat)
	{
		this.category = cat;
	}	
	
	protected String getImageFromCategory(boolean isCheese)
	{
		String path = "";
		
		switch (category) {
		//Choose the pictures to use for each tile in function of his category and if it contains the cheese or not
		case CYBERSECURITY:
			path = isCheese ? "TilesImages/purpleB.png" : "TilesImages/purple.png";
			break;
		case INTERNET:
			path = isCheese ? "TilesImages/blueB.png" : "TilesImages/blue.png";
			break;
		case NETWORKS:
			path = isCheese ? "TilesImages/orangeB.png" : "TilesImages/orange.png";
			break;
		case OPERATING_SYSTEMS:
			path = isCheese ? "TilesImages/greenB.png" : "TilesImages/green.png";
			break;
		case PROGRAMMING_LANGUAGES:
			path = isCheese ? "TilesImages/redB.png" : "TilesImages/red.png";
			break;
		case SOCIAL_NETWORKS:
			path = isCheese ? "TilesImages/yellowB.png" : "TilesImages/yellow.png";
			break;
		default:
			break;
		}
		
		return path;
	}
	
	public Category getCategory()
	{
		return category;
	}
}
