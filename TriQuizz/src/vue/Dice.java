package vue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.sun.glass.ui.TouchInputSupport;

import application.Main;

import static java.lang.Math.random;

import javafx.animation.KeyFrame;
import javafx.animation.KeyValue;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.CacheHint;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.effect.BlendMode;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BorderPane;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import vue.Tiles.Tile;

public class Dice 
{
	private List<Image> diceImages;
	private ImageView imageView;
	private Duration frameGap;
	private Timeline timeline;
	private int result;
	private List<Tile> tileToEnable;
	
	public int throwDice()
	{		
		/*
		 * Create a new dialog with a borderpane and an imageview
		 * Create a timeline that change the image in the imageview
		 * Calculate the result and send it back to the gameboard
		 */

		result = (int) (random()*6);
		
		return result+1;
	}
	
	public void playAnimation(List<Tile> tileToEnable)
	{		
		this.tileToEnable = tileToEnable;
		
		getImageView().setVisible(true);		
		getImageView().setFitHeight(Main.getStageHeight()/3);
		getImageView().setFitWidth(Main.getStageHeight()/3);
		getImageView().setLayoutX(Main.getStageWidth()-(Main.getStageHeight()/3));
		getImageView().setLayoutY(Main.getStageHeight()-(Main.getStageHeight()/3));

		
		timeline = getAnimation();	
		
		timeline.play();
	}
	
	public Timeline getAnimation()
	{
		Timeline timeline = new Timeline();
		Collection<KeyFrame> frames = timeline.getKeyFrames();
	    frameGap = Duration.millis(128);
	    Duration frameTime = Duration.ZERO ;
	    for(int i = 0; i<20;i++)
	    {
	        frameTime = frameTime.add(frameGap);
	        frames.add(new KeyFrame(frameTime, e -> frameEvent((int) (random()*6),false)));
	        if(i > 9)
	        	frameGap = frameGap.add(Duration.millis(16));
	    }   
	    //Change the dice to the result calculated earlier
        frameTime = frameTime.add(frameGap);
        frames.add(new KeyFrame(frameTime, e -> frameEvent(result,true)));
	    //add additional frames to let time for the player to see the result
	    for(int i = 0; i<5;i++)
	    {
	        frameTime = frameTime.add(frameGap);
	        frames.add(new KeyFrame(frameTime));
	    }   
	    timeline.setCycleCount(1);	    
	    timeline.setOnFinished(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				tileToEnable.forEach(tile -> tile.enable());
				
				getImageView().setVisible(false);
			}
		});
	    
	    return timeline;
	}
	
	//Event occuring every frame
	private void frameEvent(int i, boolean last)
	{
		Image img = getDiceImages().get(i);
		while(!last && imageView.getImage().equals(img))
		{
			img = getDiceImages().get((int) (random()*6));
		}
		imageView.setImage(img);
		
	}
	
	public ImageView getImageView()
	{
		if( imageView == null)
		{
			imageView = new ImageView(getDiceImages().get(0));
			
			//imageView.setBlendMode(BlendMode.MULTIPLY);
			imageView.setVisible(false);
			
			imageView.setCache(true);
			imageView.setCacheHint(CacheHint.SPEED);
		}
		
		return imageView;
	}
	
	public List<Image> getDiceImages()
	{
		if(diceImages == null)
		{
			diceImages = new ArrayList<>();
			for(int i = 1; i<=6;i++)
			{
				//Get every dice images
				diceImages.add(new Image(Dice.class.getResourceAsStream("DiceImages/"+i+".png")));
			}
		}		
		
		return diceImages;
	}
	
	public void setValue(int value)
	{
		result = value;
	}
}
