package vue;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.Node;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import modele.Category;

/**
 * Represents a generic object editor.
 * @author Nicolas Viseur
 *
 * @param <T> The type of the object we are editing.
 */
public class GenericObjectEditor<T> extends Dialog<T>
{
	private GridPane pane;
	/**
	 * Represents the class of the object being edited.
	 */
	private Class<T> tClass;
	private Label[] labels;
	/**
	 * Represents an Array of the parameters types of the class of the object being edited.
	 */
	private Class<?>[] paramsType;
	/**
	 * Represents an Array of the new parameters values of the object being edited.
	 */
	private Object[] paramsValue;
	/**
	 * Represents the number of fields of the class of the object being edited.
	 */
	private int numberOfFields;
	/**
	 * Represents the object being edited.
	 */
	private T object;

	/**
	 * Creates a generic object editor.
	 * @param tClass The class of the object being edited.
	 * @param t The object being edited
	 */
	@SuppressWarnings("unchecked")
	public GenericObjectEditor(Class<T> tClass, T t)
	{
		//Creates the visual part of the object editor.
		setWidth(400);
		setHeight(300);
		object = t;
		this.tClass = tClass;
		this.getDialogPane().setContent(getGridPane());
		setTitle(tClass.getSimpleName()+" Editor");

		ButtonType btOk = new ButtonType("Apply", ButtonData.OK_DONE);
		ButtonType btCc = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);

		getDialogPane().getButtonTypes().addAll(btOk,btCc);

		//Upon pressing the apply Button, creates a new instance of the Class T and returns it to update the current object
		//Or add a new one.
		setResultConverter(dialogButton -> 
		{
			if(dialogButton == btOk)
			{
				//Initialises the array used to store the parameters values of the object.
				paramsValue = new Object[numberOfFields];
				int i = 0;
				//Then populates the array with the values from the different boxes.
				for (Node child : pane.getChildren()) 
				{
					if(child instanceof TextField)
					{
						String val = ((TextField)child).getText();

						setObjectFromString(val,i);

						i++;
					}
					else if(child instanceof ComboBox)
					{
						String val = "";
						try
						{
							val = ((ComboBox<String>)child).getValue();
						}
						catch(ClassCastException ex)
						{
							val = ((ComboBox<Category>)child).getValue().toString();
						}

						setObjectFromString(val,i);

						i++;
					}
				}
				T instance = null;
				//Create the new instance of the T Class, by getting the constructor using the parameters types array,
				//then calling the constructor with the parameters values.
				try {
					Constructor<T> constructor = tClass.getConstructor(paramsType);
					try {
						instance = constructor.newInstance(paramsValue);
					} catch (InstantiationException | IllegalAccessException | IllegalArgumentException
							| InvocationTargetException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				} catch (NoSuchMethodException | SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				return instance;
			}

			return null;
		});
	}

	/**
	 * Sets the parameter value at the given index by casting the given value.
	 * @param s The string representation of the value.
	 * @param i The index of the parameter in the array.
	 */
	private void setObjectFromString(String s, int i)
	{
		//For each parameter, cast it by looking to the corresponding type in the parameters types array,
		//then add it to the parameters values array.
		if(paramsType[i] == int.class)
		{
			try
			{
				paramsValue[i] = Integer.parseInt(s);
			}
			catch (Exception e) 
			{
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Type error");
				alert.setContentText("The field "+labels[i].getText()+" have the wrong type of value\nShould be "+paramsType[i]);
				alert.showAndWait();
			}
		}
		else if(paramsType[i] == boolean.class)
		{
			try
			{
				paramsValue[i] = Boolean.parseBoolean(s);
			}
			catch (Exception e) 
			{
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Type error");
				alert.setContentText("The field "+labels[i].getText()+" have the wrong type of value\nShould be "+paramsType[i]);
				alert.showAndWait();
			}
		}						
		else if(paramsType[i] == Color.class)
		{
			try
			{
				if(s.equals("Blue"))
				{
					paramsValue[i] = Color.BLUE;
				}
				else if(s.equals("Orange"))
				{
					paramsValue[i] = Color.ORANGE;
				}
				else if(s.equals("Green"))
				{
					paramsValue[i] = Color.GREEN;
				}
				else if(s.equals("Yellow"))
				{
					paramsValue[i] = Color.YELLOW;
				}
				else if(s.equals("Pink"))
				{
					paramsValue[i] = Color.PINK;
				}
				else if(s.equals("Brown"))
				{
					paramsValue[i] = Color.BROWN;
				}
			}
			catch (Exception e) 
			{
				Alert alert = new Alert(AlertType.ERROR);
				alert.setTitle("Type error");
				alert.setContentText("The field "+labels[i].getText()+" have the wrong type of value\nShould be "+paramsType[i]);
				alert.showAndWait();
			}
		}
		else if(paramsType[i] == Category.class)
		{
			paramsValue[i] = Category.fromString(s);
		}
		else
		{
			paramsValue[i] = s;
		}
	}

	/**
	 * Gets the GridPane containing all the labels and boxes representing the parameters of the class of the object being edited.
	 * @return The GridPane containing all the labels and boxes representing the parameters of the class of the object being edited.
	 * @see GridPane
	 */
	private GridPane getGridPane()
	{
		//If the GridPane doesn't exist yet, creates it.
		if(pane == null)
		{
			pane = new GridPane();
			int i = 0;
			//Count all the non-static non-transient fields in the class of the object being edited.
			for (Field field : tClass.getDeclaredFields()) 
			{
				if(!Modifier.isStatic(field.getModifiers()) &&
						!Modifier.isTransient(field.getModifiers()))
				{
					i++;
				}
			}
			numberOfFields = i;
			paramsType = new Class[i];
			labels = new Label[i];
			i = 0;
			//For each non-static non-transient fields in the class of the object being edited,
			//add a label and a box of the needed type.
			for (Field field : tClass.getDeclaredFields()) 
			{
				if(!Modifier.isStatic(field.getModifiers()) &&
						!Modifier.isTransient(field.getModifiers()))
				{
					paramsType[i] = field.getType();
					Label label = new Label(field.getName());
					pane.add(label, 0, i);

					//If the object isn't null (which mean we are editing and not adding),
					//Gets the getter of the current field to not break the encapsulation.
					//And then fill the box of the field with the field's value of the object.
					if(object != null)
					{
						Method getter = null;
						try 
						{							
							for(Method m : object.getClass().getDeclaredMethods())
							{								
								if(m.getName().equalsIgnoreCase("get"+field.getName()))
								{
									getter = m;
								}
							}
						}
						catch(Exception ex)
						{
							//Catch general exception( either that or 50 try/catch )
							ex.printStackTrace();
						}
						try 
						{
							Object val = null;
							try {
								val = getter.invoke(object);
							} catch (InvocationTargetException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							if(field.getType().equals(String.class) || field.getType().equals(int.class))
							{
								TextField tfield = new TextField();
								tfield.setText(val.toString());
								pane.add(tfield, 1, i);
							}
							else if(field.getType().equals(boolean.class))
							{
								ComboBox<String> tfield = new ComboBox<String>();
								boolean bool = (boolean)val;
								tfield.getItems().add("true");
								tfield.getItems().add("false");
								tfield.setValue(bool == true ? "true" : "false");
								pane.add(tfield, 1, i);
							}
							else if(field.getType().equals(Color.class))
							{								
								ComboBox<String> colorPicker = new ComboBox<String>();
								Color color = (Color)val;

								colorPicker.getItems().addAll("Blue","Orange","Green","Yellow","Pink","Brown");

								if(color.equals(Color.BLUE))
								{
									colorPicker.setValue("Blue");
								}
								else if(color.equals(Color.ORANGE))
								{
									colorPicker.setValue("Orange");
								}
								else if(color.equals(Color.GREEN))
								{
									colorPicker.setValue("Green");
								}
								else if(color.equals(Color.YELLOW))
								{
									colorPicker.setValue("Yellow");
								}
								else if(color.equals(Color.PINK))
								{
									colorPicker.setValue("Pink");
								}
								else
								{
									colorPicker.setValue("Brown");
								}
								pane.add(colorPicker, 1, i);
							}
							else if(field.getType().equals(Category.class))
							{
								ComboBox<Category> tfield = new ComboBox<Category>();
								Category cat = (Category)val;
								tfield.getItems().addAll(Category.values());
								tfield.setValue(cat);
								pane.add(tfield, 1, i);
							}
						} catch (IllegalArgumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					//If we are adding a new object, just create the label and box of the field.
					else
					{
						if(field.getType().equals(String.class) || field.getType().equals(int.class))
						{
							TextField tfield = new TextField();
							pane.add(tfield, 1, i);
						}
						else if(field.getType().equals(boolean.class))
						{
							ComboBox<String> tfield = new ComboBox<String>();
							tfield.getItems().add("Yes");
							tfield.getItems().add("No");
							pane.add(tfield, 1, i);
						}
						else if(field.getType().equals(Color.class))
						{								
							ComboBox<String> colorPicker = new ComboBox<String>();

							colorPicker.getItems().addAll("Blue","Orange","Green","Yellow","Pink","Brown");

							pane.add(colorPicker, 1, i);
						}
						else if(field.getType().equals(Category.class))
						{
							ComboBox<Category> tfield = new ComboBox<Category>();
							tfield.getItems().addAll(Category.values());
							pane.add(tfield, 1, i);
						}
					}
					labels[i] = label;
					i++;
				}
			}
		}
		return pane;
	}
}
