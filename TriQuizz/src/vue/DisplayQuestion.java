package vue;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Map.Entry;

import javax.imageio.ImageIO;

import application.Main;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.embed.swing.SwingFXUtils;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Alert;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.control.Button;
import javafx.scene.control.ContextMenu;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;
import javafx.stage.FileChooser;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import modele.Category;
import modele.Deck;
import modele.Question;
import utilitaire.GoogleSearchJava;

public abstract class DisplayQuestion {

	private static Dialog<Boolean> dialog;
	private static Label timer;
	private static TextField field;
	private static BorderPane pane;
	private static Button hintButton;
	private static ImageView hintImage;
	private static ScrollPane scrollPane;
	private static Timeline timeline;
	private static int timeToAnswer = 60;
	private static boolean hintEnabled = true;
	private static int highReward = 3;
	private static int lowReward = 1;
	private static int reward;
	private static int timeUsed;
	private static Label hintError;
	private static String currentUrl;
	private static double acceptableErrorPct = 0.8;

	public static int launchDisplayQuestion(Category cat,boolean canAnswer)
	{	
		//Create a new dialog with everything requires to handle the questions
		timeUsed = 0;
		dialog = new Dialog<>();
		getHintError().setVisible(false);
		getHintImage().setVisible(false);
		
		//Move to the next question
		Deck.getInstance().nextQuestion(cat);

		pane =  new BorderPane();
		pane.setPrefSize(Main.getStageWidth()/1.25, Main.getStageHeight()/1.25);
		dialog.initStyle(StageStyle.UNDECORATED);
		dialog.getDialogPane().setContent(pane);
		pane.setStyle("-fx-font-size : 30px;");

		BackgroundImage QuestionBackground = new BackgroundImage(new Image(DisplayQuestion.class.getResourceAsStream("Background/Images/BackgroundTextureQuestion.png"),pane.getPrefWidth()*1.5,pane.getPrefHeight()*1.5,false,true),
				BackgroundRepeat.NO_REPEAT,BackgroundRepeat.NO_REPEAT,BackgroundPosition.DEFAULT,BackgroundSize.DEFAULT);
		pane.setBackground(new Background(QuestionBackground));

		VBox questionBox = new VBox();

		VBox boxx = new VBox();
		boxx.getChildren().add(getTimer());
		for(int i = 0; i< Deck.getInstance().getCurrentCard().getQuestions().length; i++)
		{
			Question question = Deck.getInstance().getCurrentCard().getQuestion(i);
			Label lb = new Label( "=> "+question.getStatement()+"\n");

			lb.setFont(Font.font(null,FontWeight.EXTRA_BOLD,16));

			lb.setTextFill(Category.getCategoryColor(question.getCategory()));
			if(question.equals(Deck.getInstance().getCurrentQuestion())) {
				lb.setStyle(
						"-fx-border-style: solid inside;" +
								"-fx-border-color: #CFCF04;" +
								"-fx-border-width: 3;" +
						"-fx-border-radius: 12,33%;");
			}

			lb.setTextFill(Category.getCategoryColor(question.getCategory()));
			lb.setMaxWidth(500);
			lb.setPadding(new Insets(10, 10, 10, 10) );
			lb.setWrapText(true);
			questionBox.getChildren().add(lb);
		}
		questionBox.setStyle("-fx-background-color:#"+Color.rgb(219, 214, 212).toString().substring(2,8)+";"+
				"-fx-background-radius: 12,70%;" + 
				"-fx-border-style: solid inside;" +
				"-fx-border-color: #CFCF04;" +
				"-fx-border-width: 3;" +
				"-fx-border-radius: 12,33%;");
		VBox.setMargin(questionBox, new Insets(10, 10, 30, 10));
		boxx.setAlignment(Pos.CENTER);
		if(hintEnabled)
		{
			VBox box = new VBox();
			box.getChildren().add(getHintButton());
			box.getChildren().add(getHintImage());
			box.setAlignment(Pos.CENTER);
			boxx.getChildren().add(box);
			if(getHintButton().isDisabled())
				hintButton.setDisable(false);
		}
		pane.setLeft(questionBox);
		pane.setRight(boxx);
		boxx.getChildren().add(getTextField());

		//This mean that we are only spectators of this question
		if(!canAnswer)
		{
			field.setDisable(true);
			hintButton.setDisable(true);	
			timer.setVisible(false);
		}
		else
		{			
			field.setDisable(false);
			getAnimation().play();	
			timer.setVisible(true);
		}

		dialog.showAndWait();

		return reward;
	}

	public static int getTimeUsed()
	{
		return timeUsed;
	}

	//Called by the onlinegameboard to close the vue of spectators
	public static void stop()
	{
		getAnimation().stop();

		dialog.setResult(true);
		dialog.close();
		hintImage.setImage(null);
	}

	public static Label getHintError()
	{
		if(hintError == null)
		{
			hintError = new Label("Network error : connection timed out");
			hintError.setVisible(false);
		}

		return hintError;
	}

	public static void setHintEnabled(boolean bool)
	{
		hintEnabled = bool;
	}

	public static void setTimeToAnswer(int time)
	{
		timeToAnswer = time;
	}

	public static void setRewards(int low, int high)
	{
		lowReward = low;
		highReward = high;
	}

	public static Timeline getAnimation()
	{
		//Animation used to change the counter
		if(timeline == null)
		{
			timeline = new Timeline();
			Collection<KeyFrame> frames = timeline.getKeyFrames();
			Duration frameGap = Duration.seconds(1);
			Duration frameTime = Duration.ZERO ;
			for(int i = 0; i<timeToAnswer;i++)
			{
				frameTime = frameTime.add(frameGap);
				frames.add(new KeyFrame(frameTime, e -> {
					int valueTimer = Integer.parseInt(timer.getText())-1;
					timeUsed++;


					if((Integer.parseInt(timer.getText())-1) < 10 )
					{
						timer.setText("0"+valueTimer);
						timer.setStyle("-fx-background-color: #C60800;" +

				    "-fx-font-size: " + Main.getStageHeight()/20 + ";" +
				    "-fx-padding: 20px;" +
				    "-fx-spacing: 10px;" +
				    "-fx-border-style: solid inside;" +
				    "-fx-border-width: 2;" +
				    "-fx-background-radius: 50.0;"+
								"-fx-border-radius: 50.0");
					}
					else
					{
						timer.setText(""+valueTimer);
					}
				}));
			}
			timeline.setCycleCount(1);	    
			timeline.setOnFinished(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent event) {		
					checkAnswer(true);
				}
			});
		}		

		return timeline;
	}

	public static ScrollPane getHintImage()
	{
		//Get a scrollpane containing the hint image
		if(scrollPane == null)
		{
			hintImage = new ImageView();

			hintImage.setOnMouseClicked(new EventHandler<MouseEvent>() 
			{
				@Override
				public void handle(MouseEvent event) 
				{
					if(event.getButton() == MouseButton.SECONDARY)
					{
						MenuItem item = new MenuItem("Save");
						item.setOnAction(new EventHandler<ActionEvent>() 
						{
							@Override
							public void handle(ActionEvent event) 
							{
								timeline.pause();
								FileChooser fileChooser1 = new FileChooser();
								FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.jpeg");
								fileChooser1.getExtensionFilters().add(extFilter);
								fileChooser1.setTitle("Save Image");
								File file = fileChooser1.showSaveDialog(Main.getStage());
								if (file != null) {
									try {
										ImageIO.write(SwingFXUtils.fromFXImage(hintImage.getImage(),
												null), "png", file);
									} catch (IOException ex) {
										System.out.println(ex.getMessage());
									}

								}
								timeline.play();
							}
						});
						ContextMenu contextMenu = new ContextMenu(item);

						contextMenu.show((Node)event.getSource(),event.getScreenX(),event.getSceneY());
					}
				}
			});

			StackPane pane = new StackPane();
			pane.getChildren().add(hintError);
			pane.getChildren().add(hintImage);
			scrollPane = new ScrollPane(pane);
			pane.setPrefWidth(scrollPane.getWidth());
			scrollPane.setFitToHeight(true);
			scrollPane.setFitToWidth(true);
		}

		return scrollPane; 
	}

	public static Button getHintButton()
	{
		//Get a button used to search the hint on google
		if(hintButton == null)
		{
			hintButton = new Button("Hint");

			hintButton.setOnAction(new EventHandler<ActionEvent>() 
			{
				@Override
				public void handle(ActionEvent event) 
				{				
					timeline.pause();
					getHintImage().setVisible(true);
					String searchQuery = Deck.getInstance().getCurrentQuestion().getAnswer().replace(' ', '+');
					try {
						currentUrl = GoogleSearchJava.Search(searchQuery);
						hintImage.setImage(new Image(currentUrl));
					} catch (IOException e) {
						getHintError().setVisible(true);
					}
					hintButton.setDisable(true);
					timeline.play();
				}				
			});
		}

		return hintButton; 
	}

	public static void setAcceptableErrorPct(double val)
	{
		acceptableErrorPct = val;
	}

	public static Label getTimer()
	{
		if(timer == null)
		{
			timer = new Label();
			timer.setTextAlignment(TextAlignment.CENTER);
		}
		timer.setText(""+timeToAnswer);

		timer.setStyle("-fx-background-color: #ffffff;" +

			    "-fx-font-size: " + Main.getStageHeight()/20 + ";" +
			    "-fx-padding: 20px;" +
			    "-fx-spacing: 10px;" +
			    "-fx-border-style: solid inside;" +
			    "-fx-border-width: 2;" +
			    "-fx-background-radius: 50.0;"+
				"-fx-border-radius: 50.0");

		return timer; 
	}

	public static TextField getTextField()
	{
		//Get the textfield used to answer. Temporarily show the result in an alert box
		if(field == null)
		{
			field = new TextField();
			field.setPrefWidth(500);
			field.setPrefHeight(100);
			field.setOnAction(new EventHandler<ActionEvent>() {

				@Override
				public void handle(ActionEvent arg0) {
					checkAnswer(false);
				}
			});
		}
		field.setText("");
		field.setPromptText("Enter your answer"); //Appear when the text field is empty and not focused

		return field; 
	}

	public static void checkAnswer(boolean ranOutOfTime)
	{
		timeline.stop();

		/*
		 * Rules of rewarding :
		 * -The Levenshtein result must be less than 70% of string length ( ex : for a 10 letter answer, max acceptable res is 3 )
		 * -The percentage used below is simply : (10-3)/10
		 * -The awarded points are : points * percentage rounded below ( ex : if the base award is 3 and we take the example above, reward would be 2)
		 *
		 */
		int res = Deck.getInstance().answerQuestion(field.getText());
		double nbOfLetter = Deck.getInstance().getCurrentQuestion().getAnswer().length();

		double perc = (nbOfLetter-res)/nbOfLetter;

		if(perc >= acceptableErrorPct)
		{
			Alert alert = new Alert(AlertType.INFORMATION);
			alert.setTitle("Validation");
			if(perc == 1)
			{
				alert.setHeaderText("Congratulation");
				alert.setContentText("You gave the right answer");
			}
			else
			{
				alert.setHeaderText("Congratulation, you nearly found it!");
				alert.setContentText("The right answer was "+Deck.getInstance().getCurrentQuestion().getAnswer());
			}

			if((hintEnabled && !hintButton.isDisabled()) || hintError.isVisible() || !hintEnabled)
			{
				reward = (int) Math.floor(highReward*perc);
			}
			else
			{
				reward = (int) Math.floor(lowReward*perc);
			}

			alert.show();
		}
		else
		{
			Alert alert = new Alert(AlertType.ERROR);
			alert.setTitle("Validation");
			if(!ranOutOfTime)
			{
				alert.setHeaderText("You got it wrong !");
				alert.setContentText("The right answer was "+Deck.getInstance().getCurrentQuestion().getAnswer());
			}
			else
			{
				alert.setHeaderText("You ran out of time !");
				alert.setContentText("The right answer was "+Deck.getInstance().getCurrentQuestion().getAnswer());
			}

			reward = 0;
			alert.show();
		}

		timer.setStyle("-fx-background-color: #ffffff;" +

			    "-fx-font-size: " + Main.getStageHeight()/20 + ";" +
			    "-fx-padding: 20px;" +
			    "-fx-spacing: 10px;" +
			    "-fx-border-style: solid inside;" +
			    "-fx-border-width: 2;" +
			    "-fx-background-radius: 50.0;"+
				"-fx-border-radius: 50.0");

		timeline.stop();
		dialog.setResult(true);
		dialog.close();
		if(hintEnabled)
			hintImage.setImage(null);	
	}
}
