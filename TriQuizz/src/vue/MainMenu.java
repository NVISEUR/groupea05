package vue;

import java.io.IOException;
import java.util.Optional;

import application.Main;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import modele.Lobby;
import modele.Resizable;
import modele.Account;
import utilitaire.AccountsManager;

public class MainMenu extends BorderPane implements Resizable
{
	private Button offlinePlayButton, onlinePlayButton, editButton, exitButton, connectButton, inscriptionButton, disconnectButton;
	private VBox buttonBox;
	private MediaPlayer mp;
	private Label currentPlayerLabel;
	private Lobby lobby;
	private Image backgroundImage;

	//Create the main menu and all his components
	public MainMenu()
	{
		backgroundImage = new Image(getClass().getResourceAsStream("Background/Images/FondTextureMainMenu.png"));
		HBox accountNameBox = new HBox();
		accountNameBox.getChildren().add(getCurrentPlayerLabel());
		HBox accountButtonBox = new HBox();
		accountButtonBox.getChildren().add(getConnectButton());
		accountButtonBox.getChildren().add(getInscriptionButton());

		StackPane accountButtonPane = new StackPane();
		accountButtonPane.getChildren().add(accountButtonBox);
		accountButtonPane.getChildren().add(getDisconnectButton());
		accountButtonPane.setAlignment(Pos.TOP_LEFT);
		getDisconnectButton().setVisible(false);
		VBox top = new VBox();
		top.getChildren().add(accountNameBox);
		top.getChildren().add(accountButtonPane);
		setTop(top);


		buttonBox = new VBox();
		buttonBox.getChildren().add(getOfflinePlayButton());
		buttonBox.getChildren().add(getOnlinePlayButton());
		buttonBox.getChildren().add(getExitButton());
		buttonBox.getChildren().add(getEditButton());	
		buttonBox.setAlignment(Pos.CENTER);
		setCenter(buttonBox);

		getEditButton().setVisible(false);

		getStylesheets().addAll(getClass().getResource("transback.css").toExternalForm());

		//Get the path to the menu's soundtrack
		String path = getClass().getResource("Soundtrack/menutheme.mp3").toExternalForm();
		//Create a Media with the path
		Media media = new Media(path);
		//Create a new MediaPlayer to play the Media
		mp = new MediaPlayer(media);
		mp.setCycleCount(MediaPlayer.INDEFINITE);
		//Play the soundtrack
		mp.play();



		if(!AccountsManager.loadAccounts())
		{
			//the account file do not exist yet, so we create it with the admin account
			new Account("Admin", "helha", "", Color.WHITE, true);
			AccountsManager.saveAccounts();
		}

		resize();
	}

	//Disconnect the player, resetting the lobby
	public Button getDisconnectButton()
	{
		if(disconnectButton == null)
		{
			disconnectButton = new Button();
			disconnectButton.setText("Disconnect");

			disconnectButton.setOnAction(e ->
			{
				lobby.clear(null);
				getDisconnectButton().setVisible(false);
				getConnectButton().setVisible(true);
				getInscriptionButton().setVisible(true);
				getCurrentPlayerLabel().setText("Current player : none");
				getEditButton().setVisible(false);
			});
		}

		return disconnectButton; 
	}

	public Label getCurrentPlayerLabel()
	{
		if(currentPlayerLabel == null)
		{
			currentPlayerLabel = new Label();
			currentPlayerLabel.setText("Current player : none");
			currentPlayerLabel.getStyleClass().add("outline");
			currentPlayerLabel.setTextFill(Color.WHITE);
		}

		return currentPlayerLabel; 
	}

	//Go to the lobbyBeforeGame and give it the lobby object
	public Button getOfflinePlayButton()
	{
		if(offlinePlayButton == null)
		{
			offlinePlayButton = new Button();
			offlinePlayButton.setText("Offline Game");

			offlinePlayButton.setOnAction(e -> 
			{
				if(lobby == null || lobby.length() == 0)
				{
					Account invite = null;
					if(AccountsManager.getPlayer("Player1") != null)
					{
						invite = AccountsManager.getPlayer("Player1");
					}
					else
					{
						invite = new Account("Player1",null,null,Color.BLUE);
					}
					lobby = new Lobby(invite);
				}

				mp.pause();
				Main.getStage().setScene( new Scene(new LocalLobby(lobby),Main.getStageWidth(),Main.getStageHeight()));
			});
		}

		return offlinePlayButton; 
	}


	public Button getOnlinePlayButton()
	{
		if(onlinePlayButton == null)
		{
			onlinePlayButton = new Button();
			onlinePlayButton.setText("Online Game");

			onlinePlayButton.setOnAction(e ->
			{
				if(lobby == null || lobby.length() == 0)
				{
					Alert alert = new Alert(AlertType.ERROR);
					alert.setTitle("Not connected");
					alert.setHeaderText("You must be connected in order to play online");
					alert.showAndWait();
					return;
				}

				//Open a dialog to select if the player join or host the game
				Dialog<ButtonType> choiceOnline = new Dialog<>();
				choiceOnline.setTitle("Connection");

				ButtonType host = new ButtonType("Create a room");
				ButtonType join = new ButtonType("Join an existing room");
				choiceOnline.getDialogPane().getButtonTypes().addAll(host, join);

				//the red cross close the dialog
				choiceOnline.getDialogPane().getButtonTypes().add(ButtonType.CLOSE);
				Node closeButton = choiceOnline.getDialogPane().lookupButton(ButtonType.CLOSE);
				closeButton.managedProperty().bind(closeButton.visibleProperty());
				closeButton.setVisible(false);

				GridPane grid = new GridPane();

				grid.setHgap(10);
				grid.setVgap(10);
				grid.setPadding(new Insets(20, 150, 10, 10));

				Optional<ButtonType> res = choiceOnline.showAndWait();
				String choice = res.get().getText();

				if(choice == "Create a room")
				{
					mp.pause();
					try {
						Main.getStage().setScene( new Scene(new OnlineLobby(lobby,null),Main.getStageWidth(),Main.getStageHeight()));
					} catch (IOException ex) {
						// TODO Auto-generated catch block
						ex.printStackTrace();
					}
				}
				else if (choice == "Join an existing room")
				{
					//Create a dialog to join the room
					Dialog<String> dialog = new Dialog<>();
					dialog.setTitle("Send ip");
					ButtonType applyButtonType = new ButtonType("Apply", ButtonData.APPLY);
					ButtonType cancelButtonType = new ButtonType("Cancel", ButtonData.CANCEL_CLOSE);
					dialog.getDialogPane().getButtonTypes().addAll(applyButtonType, cancelButtonType);

					GridPane gridIp = new GridPane();
					gridIp.setHgap(10);
					gridIp.setVgap(10);
					gridIp.setPadding(new Insets(20, 150, 10, 10));

					TextField textIp = new TextField();
					textIp.setPromptText("ip");
					textIp.setText("192.168.1.28");

					gridIp.add(new Label("IP :"), 0, 0);
					gridIp.add(textIp, 1, 0);

					dialog.getDialogPane().setContent(gridIp);

					dialog.setResultConverter(dialogButton -> {
						if (dialogButton == applyButtonType) {
							return textIp.getText();
						}
						return null;
					});

					Optional<String> result = dialog.showAndWait();

					if(result.isPresent())
					{
						mp.pause();
						try {
							Main.getStage().setScene( new Scene(new OnlineLobby(lobby,textIp.getText()),Main.getStageWidth(),Main.getStageHeight()));
						} catch (IOException ex) {
							// TODO Auto-generated catch block
							ex.printStackTrace();
						}
					}
				}
			});
		}

		return onlinePlayButton; 
	}

	//Show the connection dialog allowing to connect by providing the right nickname/password pair.
	public Button getConnectButton()
	{
		if(connectButton == null)
		{
			connectButton = new Button();
			connectButton.setText("Log in");

			connectButton.setOnAction(e ->
			{
				Optional<Account> result = DisplayLoginOptions.launchDisplayConnection();

				if(result.isPresent())
				{
					Account account = result.get();
					getCurrentPlayerLabel().setText("Current player : "+account.getNickname());
					lobby = new Lobby(account);
					getDisconnectButton().setVisible(true);
					getConnectButton().setVisible(false);
					getInscriptionButton().setVisible(false);

					if(account.getIsAdministrator())
					{
						getEditButton().setVisible(true);
					}
				}
			});
		}
		return connectButton; 
	}

	//Show the inscription dialog allowing to create a new account
	public Button getInscriptionButton()
	{
		if(inscriptionButton == null)
		{
			inscriptionButton = new Button();
			inscriptionButton.setText("Create a new account");

			inscriptionButton.setOnAction(e -> 
			{
				Optional<Account> result = DisplayLoginOptions.launchDisplayInscription();

				if(result.isPresent())
				{							
					Account player = result.get();

					AccountsManager.saveAccounts();
					getCurrentPlayerLabel().setText("Current player : "+player.getNickname());
					lobby = new Lobby(player);
					getDisconnectButton().setVisible(true);
					getConnectButton().setVisible(false);
					getInscriptionButton().setVisible(false);
				}
			});
		}

		return inscriptionButton; 
	}

	//Go to the editor
	public Button getEditButton()
	{
		if(editButton == null)
		{
			editButton = new Button();
			editButton.setText("Data editor");

			editButton.setOnAction(e -> {
				Main.getStage().setScene(new Scene(new GenericEditor(),Main.getStageWidth(),Main.getStageHeight()));
				mp.pause();
			});
		}

		return editButton; 
	}

	public Button getExitButton()
	{
		if(exitButton == null)
		{
			exitButton = new Button();
			exitButton.setText("Exit");
			//Quit the game
			exitButton.setOnAction(e -> Platform.exit());
		}

		return exitButton; 
	}

	public void resize()
	{
		final double normalButtonsWidth = Main.getStageWidth()/5;
		final double normalButtonsHeight = Main.getStageHeight()/13;
		final double smallButtonsWidth = Main.getStageWidth()/8;
		final double smallButtonsHeight = Main.getStageHeight()/20;

		//Exit Button
		getExitButton().setPrefWidth(normalButtonsWidth);
		getExitButton().setPrefHeight(normalButtonsHeight);

		//Online Play Button
		getOnlinePlayButton().setPrefWidth(normalButtonsWidth);
		getOnlinePlayButton().setPrefHeight(normalButtonsHeight);

		//Offline Play Button
		getOfflinePlayButton().setPrefWidth(normalButtonsWidth);
		getOfflinePlayButton().setPrefHeight(normalButtonsHeight);

		//Edit Button
		getEditButton().setPrefWidth(normalButtonsWidth);
		getEditButton().setPrefHeight(normalButtonsHeight);

		//Inscription Button
		getInscriptionButton().setPrefWidth(smallButtonsWidth);
		getInscriptionButton().setPrefHeight(smallButtonsHeight);

		//Connect Button
		getConnectButton().setPrefWidth(smallButtonsWidth);
		getConnectButton().setPrefHeight(smallButtonsHeight);

		//Disconnect Button
		getDisconnectButton().setPrefWidth(smallButtonsWidth);
		getDisconnectButton().setPrefHeight(smallButtonsHeight);


		double fontSize = Main.getStageWidth()/125+Main.getStageHeight()/75;

		//Label
		getCurrentPlayerLabel().setFont(new Font(fontSize));

		//Button box 
		buttonBox.setSpacing(Main.getStageHeight()/50);

		//Background
		BackgroundSize size = new BackgroundSize(Main.getStageWidth(), Main.getStageHeight(), false, false, false, false);
		BackgroundImage image = new BackgroundImage(backgroundImage, BackgroundRepeat.NO_REPEAT, BackgroundRepeat.NO_REPEAT, BackgroundPosition.DEFAULT, size);
		this.setBackground(new Background(image));
	}
}
