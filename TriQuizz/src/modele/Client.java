package modele;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.LinkedList;

public class Client implements NetworkSession
{	
	private Socket server;
	//We are using a linkedList to have a FIFO object (first in first out)
	private LinkedList<String> messages;
	private Thread messagingThread;
	private Thread pingThread;

	public Client()
	{
		messages = new LinkedList<String>();
	}

	
	/**
	 * Returns messages from the host
	 */
	public LinkedList<String> getMessages()
	{
		return messages;
	}

	
	/**
	 * Send message to the host
	 */
	public void sendMessage(String msg)
	{ 		        
		DataOutputStream out;
		try 
		{
			out = new DataOutputStream(server.getOutputStream());
			//Send message
			out.writeUTF(msg);
		} 
		catch (IOException | NullPointerException e) 
		{
			// We lost connection with the host, so we "send" ourself a message to be handled by the lobby
			messages.add(NetworkMessage.encodePlayerDisconnectMessage("host", true));
		}
	}

	
	/**
	 * Initialize the connection : connect the client ot the host, and start waiting for messages
	 */
	public void initConnection(String hostIp) throws UnknownHostException, IOException
	{
		server = new Socket();
		//Connect, with a maximum timeout of 1 second
		server.connect(new InetSocketAddress(hostIp, CONNECTION_PORT), 1000);
		server.setTcpNoDelay(true);

		messagingThread = new Thread(() -> 
		{        	
			while (!messagingThread.isInterrupted()) 
			{
				try 
				{
					try 
					{
						if(!server.isClosed())
						{
							try
							{
								DataInputStream inputStream = new DataInputStream(server.getInputStream());
								String message = inputStream.readUTF();

								messages.add(message);
							}
							catch (EOFException | SocketException ex) 
							{
								//There is no more data sent by the server (probably lost connection)
								server.close();
							}
						}
					} 
					catch (IOException ex) 
					{
						ex.printStackTrace();
					}

					Thread.sleep(MESSAGE_READ_INTERVAL_IN_MS);
				} catch (InterruptedException ex) {

					Thread.currentThread().interrupt();

					break;

				}
			}
		});
		//Make sure the thread is stopped when the program exit
		messagingThread.setDaemon(true);
		messagingThread.start();

		pingThread = new Thread(() ->
		{
			while (!pingThread.isInterrupted()) 
			{
				try
				{
					//Send a ping
					sendMessage(NetworkMessage.encodePingMessage());
					Thread.sleep(PING_INTERVAL_IN_MS);
				}
				catch (InterruptedException e) 
				{
					Thread.currentThread().interrupt();

					break;				
				}
			}
		}
				);

		pingThread.setDaemon(true);
		pingThread.start();
	}

	/**
	 * Stop connection with server
	 */
	public void close() throws IOException
	{
		//Close all threads + close the server
		if(messagingThread != null)
			messagingThread.interrupt();
		if(pingThread != null)
			pingThread.interrupt();
		if(server != null)
			server.close();
	}
}
