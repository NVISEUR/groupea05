package modele;

public class DuelScore 
{
	int reward, timeUsed;

	public DuelScore(int reward, int timeUsed)
	{
		this.reward = reward;
		this.timeUsed = timeUsed;
	}

	
	/**
	 * Returns the reward
	 * @return
	 */
	public int getReward()
	{
		return reward;
	}

	/**
	 * Returns the time spent 
	 * @return
	 */
	public int getTimeUsed()
	{
		return timeUsed;
	}
}
