package modele;

import javafx.scene.paint.Color;
import utilitaire.AccountsManager;

public class Account implements Comparable<Account>
{
	private String nickname;
	private String password;
	private String email;
	private Color color;
	private boolean isAdministrator = false;
	private int currency = 0;
	private transient GameSession session;

	public Account(String nickname,String password, String email, Color col)
	{
		this.nickname = nickname;
		this.password = password;
		this.email = email;
		this.color = col;

		//Add the account into the accounts manager
		AccountsManager.addAccount(this);
	}

	public Account(String nickname,String password, String email, Color col, boolean admin)
	{
		this.nickname = nickname;
		this.password = password;
		this.email = email;
		this.color = col;
		this.isAdministrator = admin;

		//Add the account into the accounts manager
		AccountsManager.addAccount(this);
	}

	public Account(String nickname,String password, String email, Color col, boolean admin, int currency)
	{
		this.nickname = nickname;
		this.password = password;
		this.email = email;
		this.color = col;
		this.isAdministrator = admin;
		this.currency = currency;
	}
	/**
	 * Copy and get data from the account
	 * @param acc
	 */
	public void copyDataFromAccount(Account acc)
	{
		//Set user's values with a copy of another one user
		this.nickname = acc.getNickname();
		this.password = acc.getPassword();
		this.email = acc.getEmail();
		this.color = acc.getColor();
		this.isAdministrator = acc.isAdministrator;
		this.currency = acc.getCurrency();
	}
	
	/**
	 * Returns permission status
	 * @return
	 */


	public boolean getIsAdministrator()
	{
		return this.isAdministrator;
	}
	/**
	 * Add a certain number of Currencies defined by "value" to the player
	 * @param value
	 */

	public void addCurency(int value)
	{
		this.currency += value;
	}
	/**
	 * Returns the total amount of currency obtained by the account
	 * @return
	 */

	public int getCurrency()
	{
		return this.currency;
	}
	/**
	 * Returns the account's e-mail
	 * @return
	 */

	public String getEmail()
	{
		return this.email;
	}

	/**
	 * Returns the nickname of the account
	 * @return
	 */

	public String getNickname()
	{
		return this.nickname;
	}
 
	/**
	 * Returns the password of the account
	 * @return
	 */
 

 
	public String getPassword()
	{
		return this.password;
	}
 
	/**
	 * Set a color
	 * @param col
	 */
 

 
	public void setColor(Color col)
	{
		this.color = col;
	}
 
	/**
	 * Set a nickname
	 * @param nickname
	 */
 

 
	public void setNickname(String nickname) {
		this.nickname = nickname;
	}
	/**
	 * Set a password
	 * @param password
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * Assign an e-mail contact to the account
	 * @param email
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * Set an account as Administrator
	 * @param isAdministrator
	 */
	public void setIsAdministrator(boolean isAdministrator) {
		this.isAdministrator = isAdministrator;
	}
	/**
	 * Set total value of the account's wallet
	 * @param currency
	 */
	public void setCurrency(int currency) {
		this.currency = currency;
	}
	/**
	 * Returns the color
	 * @return
	 */
	public Color getColor()
	{
		return this.color;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (nickname == null) {
			if (other.nickname != null)
				return false;
		} else if (!nickname.equalsIgnoreCase(other.nickname))
			return false;
		return true;
	}

	@Override
	public int compareTo(Account p) {
		return nickname.compareToIgnoreCase(p.getNickname());
	}
 
	
	/**
	 * Returns the current game session
	 * @return
	 */
 

 
	public GameSession getSession() 
	{
		if(session == null)
		{
			session = new GameSession(color,nickname);
		}

		return session;
	}

	public String toString()
	{
		return this.nickname;
	}
}
