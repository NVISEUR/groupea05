package modele;

import java.io.IOException;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import application.Main;
import javafx.geometry.Pos;
import javafx.scene.CacheHint;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.image.PixelReader;
import javafx.scene.image.PixelWriter;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import vue.Tiles.Tile;

public class GameSession 
{
	private int currency;
	private ImageView imageView;
	private HBox hbox;
	private Label nameLabel;
	private boolean haveGreenCamembert, haveBlueCamembert, haveYellowCamembert, haveRedCamembert, havePurpleCamembert, haveOrangeCamembert;
	private Tile currentTile;
	private List<ImageView> camembertImages;
	private int imageSize = 50;
	private NetworkSession networkSession;
	private Color color;
	private String name;

	public GameSession(Color color, String name)
	{
		this.color = color;
		this.name = name;
		nameLabel = new Label(name);
		nameLabel.setFont(new Font(20));
		camembertImages = new ArrayList<>();
		setImage(color);
	}

	
	/**
	 * Returns the color
	 * @return
	 */
	public Color getColor()
	{
		return this.color;
	}

	
	/**
	 * Returns Nickname
	 * @return
	 */
	public String getNickname()
	{
		return this.name;
	}

	/**
	 * Returns name HBox
	 * @return
	 */
	public HBox getNameBox()
	{		
		if(hbox == null)
		{
			hbox = new HBox();
			hbox.getChildren().add(nameLabel);
			nameLabel.setFont(new Font(Main.getStageHeight()/50));
			hbox.setStyle("-fx-background-color: #ffffff;"+
					"-fx-border-style: solid inside;" +
					"-fx-border-width: 3;" +
					"-fx-border-color: #000000;");
			hbox.setAlignment(Pos.CENTER);
			hbox.setPrefWidth(100);
		}
		return hbox;
	}

	
	/**
	 * Return Image size
	 * @return
	 */
	public int getImageSize()
	{
		return imageSize;
	}

	
	/**
	 * Resets the game session
	 */
	public void reset()
	{
		//Reset the Game session
		haveGreenCamembert = false;
		haveBlueCamembert = false;
		haveYellowCamembert = false;
		haveRedCamembert = false;
		havePurpleCamembert = false;
		haveOrangeCamembert = false;

		camembertImages = new ArrayList<>();
		currentTile = null;
		currency = 0;
	}

	
	/**
	 * Close connection (for online multiplayer session)
	 */
	public void closeConnection()
	{
		try {
			networkSession.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	/**
	 * Set the current tile
	 * @param tile
	 */
	public void setCurrentTile(Tile tile)
	{
		currentTile = tile;
	}

	/**
	 * Returns the current tile
	 * @return
	 */
	public Tile getCurrentTile()
	{
		return currentTile;
	}

	/**
	 * Add a currency to the player
	 * @param value
	 */
	public void addCurency(int value)
	{
		this.currency += value;
	}

	/**
	 * Remove a currency from the player
	 * @param value
	 */
	public void removeCurrency(int value)
	{
		this.currency -= value;
	}

	/**
	 * Set the images and try to adjust the color to match the account color
	 * @param color
	 */
	public void setImage(Color color)
	{
		
		Image image = new Image(getClass().getResourceAsStream("Images/Pion_Base.png"),50,50,true,true);
		image = recolorImage(image, color);
		imageView = new ImageView(image);

		//then cache it for speed
		imageView.setCache(true);
		imageView.setCacheHint(CacheHint.SPEED);
	}

	
	/**
	 * Recolor an image with the given color.
	 * @param i
	 * @param c
	 * @return
	 */
	public Image recolorImage(Image i, Color c)
	{
		int width = (int) i.getWidth();
		int height = (int) i.getHeight();

		PixelReader pixelReader = i.getPixelReader();

		WritableImage wImage = new WritableImage(width,height);
		PixelWriter pixelWriter = wImage.getPixelWriter();

		for(int readY=0;readY<height;readY++){
			for(int readX=0; readX<width;readX++){
				Color color = pixelReader.getColor(readX,readY);

				// Now write a brighter color to the PixelWriter.
				if(color.getOpacity() != 0)
				{
					Color newCol = Color.hsb(c.getHue(), c.getSaturation(), color.getBrightness(), c.getOpacity());
					//Set a new color for each pixel
					pixelWriter.setColor(readX,readY,newCol);
				}
			}
		}

		return wImage;
	}

	/**
	 * Returns the image view
	 * @return
	 */
	public ImageView getImageView()
	{
		return this.imageView;
	}

	/**
	 * Return true if all the camemberts are present.
	 * @return
	 */
	public boolean haveAllCamembert()
	{
		return haveGreenCamembert && haveBlueCamembert && haveYellowCamembert && haveRedCamembert && havePurpleCamembert && haveOrangeCamembert;
	}

	
	/**
	 * add a camembert of the given category to the session
	 * @param cat
	 */
	public void addCamembert(Category cat)
	{
		//add the camembert of the given category to the session
		Image image = null;
		switch (cat) {
		case CYBERSECURITY:
			if(havePurpleCamembert)
				return;
			image = new Image(getClass().getResourceAsStream("Images/CamembertPurple.png"),imageSize,imageSize,true,true);
			havePurpleCamembert = true;
			break;
		case INTERNET:
			if(haveBlueCamembert)
				return;
			image = new Image(getClass().getResourceAsStream("Images/CamembertBlue.png"),imageSize,imageSize,true,true);
			haveBlueCamembert = true;
			break;
		case NETWORKS:
			if(haveOrangeCamembert)
				return;
			image = new Image(getClass().getResourceAsStream("Images/CamembertOrange.png"),imageSize,imageSize,true,true);
			haveOrangeCamembert = true;
			break;
		case OPERATING_SYSTEMS:
			if(haveGreenCamembert)
				return;
			image = new Image(getClass().getResourceAsStream("Images/CamembertGreen.png"),imageSize,imageSize,true,true);
			haveGreenCamembert = true;
			break;
		case PROGRAMMING_LANGUAGES:
			if(haveRedCamembert)
				return;
			image = new Image(getClass().getResourceAsStream("Images/CamembertRed.png"),imageSize,imageSize,true,true);
			haveRedCamembert = true;
			break;
		case SOCIAL_NETWORKS:
			if(haveYellowCamembert)
				return;
			image = new Image(getClass().getResourceAsStream("Images/CamembertYellow.png"),imageSize,imageSize,true,true);
			haveYellowCamembert = true;
			break;
		}
		//And set is position according to the current player position
		ImageView imageView = new ImageView(image);
		imageView.setLayoutX(this.imageView.getLayoutX());
		imageView.setLayoutY(this.imageView.getLayoutY());
		imageView.setRotate(this.imageView.getRotate());
		imageView.setFitHeight(Main.getStageHeight()/15);
		imageView.setFitWidth(Main.getStageHeight()/15);
		camembertImages.add(imageView);
	}

	/**
	 * Returns the camemberts image list
	 * @return
	 */
	public List<ImageView> getCamemberts()
	{
		return this.camembertImages;
	}

	
	/**
	 * Change the position of the pawn image and all the camemberts images
	 * @param x
	 * @param y
	 * @param rotate
	 */
	public void movePlayer(double x, double y, double rotate)
	{
	
		this.getNameBox().setLayoutX(x+imageSize/2-getNameBox().getPrefWidth()/2);
		this.getNameBox().setLayoutY(y-imageSize/2-20);
		this.imageView.setLayoutX(x);
		this.imageView.setLayoutY(y);
		this.imageView.setRotate(rotate);

		for (ImageView imageView : camembertImages) 
		{
			imageView.setLayoutX(x);
			imageView.setLayoutY(y);
			imageView.setRotate(rotate);
		}
	}
	/**
	 * Returns a Currency
	 * @return
	 */
	public int getCurrency() {
		return currency;
	}
	/**
	 * Sets the network session
	 * @param servermode
	 */
	public void setNetworkSession(NetworkSession servermode) 
	{
		this.networkSession = servermode;
	}

	/**
	 * Send the given message to the server host
	 * @param message
	 */



	public void sendMessage(String message)
	{
		if(networkSession != null)
		{
			networkSession.sendMessage(message);
		}
	}

	/**
	 * Initialize connection to the session host
	 * @param hostIp
	 * @throws IOException
	 */
	public void initConnection(String hostIp) throws IOException
	{
		if(networkSession != null)
		{
			networkSession.initConnection(hostIp);
		}
	}

	
	/**
	 * Returns session's messages to synchronize.
	 * @return
	 */
	public LinkedList<String> getMessages()
	{
		return networkSession.getMessages();
	}

	
	/**
	 * Return true if the instance is a host instance.
	 * @return
	 */
	public boolean isHost()
	{
		return networkSession instanceof Host;
	}
}
