package modele;

public class Card {
	private Question[] questions;

	public Card()
	{
		questions = new Question[6];
	}
	/**
	 * Returns the question by the way of it's index
	 * @param i
	 * @return
	 */
	public Question getQuestion(int i) {
		return questions[i];
	}
 
	/**
	 * Returns the question's list
	 * @return
	 */
 

 
	public Question[] getQuestions() {
		return questions;
	}
 
	
	/**
	 * Returns true if the questions list contains a question from selected category.
	 * @param cat
	 * @return
	 */
 

 
	public boolean containQuestionOfCategory(Category cat)
	{
		int n=-1;

		switch (cat) {
		case CYBERSECURITY:
			n=0;
			break;
		case INTERNET:
			n=1;
			break;
		case NETWORKS:
			n=2;
			break;
		case OPERATING_SYSTEMS:
			n=3;
			break;
		case PROGRAMMING_LANGUAGES:
			n=4;
			break;
		case SOCIAL_NETWORKS:
			n=5;
			break;
		}
		//Return category
		return questions[n] != null;
	}
 
	/**
	 * add a question into the list.
	 * @param q
	 */
 

 
	public void addQuestion(Question q)
	{
		int n=-1;

		switch (q.getCategory()) {
		case CYBERSECURITY:
			n=0;
			break;
		case INTERNET:
			n=1;
			break;
		case NETWORKS:
			n=2;
			break;
		case OPERATING_SYSTEMS:
			n=3;
			break;
		case PROGRAMMING_LANGUAGES:
			n=4;
			break;
		case SOCIAL_NETWORKS:
			n=5;
			break;
		default:
			break;
		}
		//Set card question
		this.questions[n] = q;
	}
 
	
	/**
	 * Returns true if the list contains the selected question in parameter
	 * @param q
	 * @return
	 */
 

 
	public boolean containsQuestion(Question q)
	{
		int n=-1;

		switch (q.getCategory()) {
		case CYBERSECURITY:
			n=0;
			break;
		case INTERNET:
			n=1;
			break;
		case NETWORKS:
			n=2;
			break;
		case OPERATING_SYSTEMS:
			n=3;
			break;
		case PROGRAMMING_LANGUAGES:
			n=4;
			break;
		case SOCIAL_NETWORKS:
			n=5;
			break;
		default:
			break;
		}
		//Return if the question is already in the card
		return questions[n] != null && questions[n].equals(q);
	}
 
	
	/**
	 * Returns a question from the selected category
	 * @param cat
	 * @return
	 */
 

 
	public Question getQuestionByCategory(Category cat)
	{
		int n=-1;

		switch (cat) {
		case CYBERSECURITY:
			n=0;
			break;
		case INTERNET:
			n=1;
			break;
		case NETWORKS:
			n=2;
			break;
		case OPERATING_SYSTEMS:
			n=3;
			break;
		case PROGRAMMING_LANGUAGES:
			n=4;
			break;
		case SOCIAL_NETWORKS:
			n=5;
			break;
		default:
			break;
		}
		//Return the category's question
		return questions[n];
	}
 
	
	/**
	 * Returns true if the question list is full
	 * @return
	 */
	public boolean isFull()
	{
		boolean isFull = false;

		isFull = questions[0] != null &&
				questions[1] != null &&
				questions[2] != null &&
				questions[3] != null &&
				questions[4] != null &&
				questions[5] != null;

		return isFull;
	}

	public String toString()
	{
		String s = "";

		for(Question q : questions)
		{
			if(q != null)
				s+=q.toString()+"\n";
		}

		return s;
	}
}
