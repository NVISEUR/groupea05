package modele;

import java.io.IOException;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.LinkedList;

public interface NetworkSession 
{
	//Every ten second
	final int PING_INTERVAL_IN_MS = 5000;
	//Every hundred of a second
	final int MESSAGE_READ_INTERVAL_IN_MS = 10;
	//Port used by connections (perhaps we may allow to change it later)
	final int CONNECTION_PORT = 8000;
	//Time during which we wait for the client to send a message.
	final int CLIENT_SOCKET_TIMEOUT_IN_MS = 10;
	
	/**
	 * Send a message
	 * @param msg the message to send
	 */
	public void sendMessage(String msg);
	
	/**
	 * Initialize the connection to the host
	 * @param hostIp the host's ip adress to reach
	 * @throws UnknownHostException Unknown host exception
	 * @throws IOException 
	 */
	public void initConnection(String hostIp) throws UnknownHostException, IOException;
	
	/**
	 * Close the connection
	 * @throws IOException
	 */
	public void close() throws IOException;
	
	/**
	 * Return the messages from the host
	 * @return
	 */
	public LinkedList<String> getMessages();
}
