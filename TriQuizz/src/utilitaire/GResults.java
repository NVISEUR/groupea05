/*
 * Helper class to hold data returned from the google search
 */


package utilitaire;

import java.util.List;

public class GResults {

	public String link;
	public List<GResults> items;

	/**
	 * Return the link to the image
	 * @return the link
	 */
	public String getLink(){
		return link;
	}
	
	/**
	 * Return the list of links as items
	 * @return le list of items
	 */
	public List<GResults> getItems(){
		return items;
	}
	
	/**
	 * Set the link of the image to be used
	 * @param linkvthe link of the image
	 */
	public void setLink(String link){
		this.link = link;
	}
	
	/**
	 * Set the group of items as list
	 * @param items the list of items
	 */
	public void setGroups(List<GResults> items){
		this.items = items;
	}
	
	/**
	 * Return the item by index
	 * @param i the index of the item
	 */
	public void getThing (int i){
		System.out.println(items.get(i));
	}

	/**
	 * return the link  as a string
	 */
	public String toString(){
		return String.format("%s", link);
	}

}