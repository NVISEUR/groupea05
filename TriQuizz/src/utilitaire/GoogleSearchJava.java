package utilitaire;

import java.net.*;
import com.google.gson.Gson;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class GoogleSearchJava
{

	public static String Search(String query) throws IOException {
		//Provide the key id key and the search engine key generated from our google account
	    String key = "AIzaSyCcz51_6VX8r3Fl91voorFdIRYnvqLMiQU";
	    String cx  = "003939752990360010339:tivfa3vchv8";
	    //Set the searched file type and the type of search
	    String fileType = "png,jpg";
	    String searchType = "image";
	    URL url = null;
		try {
			//Create the url using the provided query
			url = new URL ("https://www.googleapis.com/customsearch/v1?key=" +key+ "&amp&cx=" +cx+ "&q=" +query+"&fileType="+fileType+"&searchType="+searchType+"&alt=json&num=5");
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
	    HttpURLConnection conn = null;
		try {
			//Try to open the connection
			conn = (HttpURLConnection) url.openConnection();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	    try {
	    	//Set the request method
			conn.setRequestMethod("GET");
		} catch (ProtocolException e) {
			e.printStackTrace();
		}
	    //Set the return type (json)
	    conn.setRequestProperty("Accept", "application/json");
	    BufferedReader br = null;
	    conn.setConnectTimeout(4000);
		br = new BufferedReader(new InputStreamReader ( ( conn.getInputStream() ) ) );
	    //Read the data from the generated json file
	    GResults results = new Gson().fromJson(br, GResults.class);
	    conn.disconnect();
	    
	    //Get a random index ( to avoid showing the same image every time )
	    int imageIndex = (int) (Math.random()*results.items.size());
	    
	    //Return the hint image
	    return results.getItems().get(imageIndex).link;
  }
}