package application;

import javafx.application.Application;
import javafx.beans.value.ChangeListener;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Screen;
import javafx.stage.Stage;
import modele.Resizable;
import vue.MainMenu;

public class Main extends Application {

	private static Scene mainMenu;
	private static Stage stage;
	private static double stageHeight, stageWidth;
	private static ChangeListener<Number> resizeListener = (observable, oldValue, newValue) ->
	{
		stageHeight = stage.getHeight();
		stageWidth = stage.getWidth();
		((Resizable)stage.getScene().getRoot()).resize();
	};
	
	/**
	 * Returns for the stage height
	 * @return
	 */
	public static double getStageHeight()
	{
		return stageHeight;
	} 
	/**
	 * Returns the stage width
	 * @return
	 */
	public static double getStageWidth()
	{
		return stageWidth;
	}
	
	/**
	 * Returns the main menu 
	 * @return
	 */
	public static Scene getMainMenu()
	{
		mainMenu = new Scene(new MainMenu(),stageWidth,stageHeight);
		
		return mainMenu; 
	}
	/**
	 * Returns the current stage
	 * @return
	 */
	public static Stage getStage()
	{
		return stage;
	}
	/**
	 * Launch the app screen
	 */
	@Override
	public void start(Stage primaryStage) throws Exception {
		try {			
			//Get the screen height and width
			stage = primaryStage;
			stage.setResizable(false);
	        Rectangle2D primaryScreenBounds = Screen.getPrimary().getVisualBounds();
	        Double mult = 0.90;

	        //set Stage boundaries to visible bounds of the main screen
	        stage.setWidth(primaryScreenBounds.getWidth()*mult);
	        stage.setHeight(primaryScreenBounds.getHeight()*mult);
			stageHeight = stage.getHeight();
			stageWidth = stage.getWidth();        
			primaryStage.setX((primaryScreenBounds.getWidth() - primaryStage.getWidth()) / 2);
	        primaryStage.setY((primaryScreenBounds.getHeight() - primaryStage.getHeight()) / 2);
	        
			//Set the initial stage state
			stage.widthProperty().addListener(resizeListener);
			stage.heightProperty().addListener(resizeListener);
			stage.setTitle("TriQuizz");
			stage.setScene(getMainMenu());
			stage.show();
			stage.getIcons().add(new Image(getClass().getResourceAsStream("Images/icon.png")));
			
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void main(String[] args) 
	{
		launch(args);
	}
}
